#!/usr/bin/php
<?php
include 'BotCore.php';
/** PurgeMath.php
* Fuehrt eine Purge bei allen Math-Seiten durch
* PROBLEM: Bisher kein weg zum Purgen gefunden (MW-Softwaresetitig)
* @Author Luke081515
* @Version 0.2
* @Status Alpha
*/
class PurgeMath extends Core
{
	protected $host;
	protected $username;
	protected $password;
	protected $cookies;
	public $bot = 1; // set to 0 if this edit should not be marked as bot edit

	public function PurgeMath ($host, $username, $password)
	{
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
		$this->main ();
	}
	public function main ($Search)
	{
		$Result = unserialize($this->search ($Search, 0));
		$Spaces = array (0, 4, 10, 14);
		$b=0;
		while (isset ($Spaces [$b]) === true)
		{
			$Result = $this->search ($Search, $Spaces [$b]);
			$a=0;
			while (isset ($Result [$a]) === true)
			{
				$this->PurgePage ($Result [$a]);
				$a++;
			}
		$b++;
		}
	}
	/** PurgePage
	* Purgt die Seite $title
	*/
	public function PurgePage($title)
	{
		$crl = curl_init();
		curl_setopt ($crl, CURLOPT_URL, "https://" . $this->host . "/w/index.php?title=" . urlencode($title) . "action=purge&mathpurge=true");
		curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt ($crl, CURLOPT_USERAGENT, "LukeBot");
		$ret = curl_exec($crl);
		curl_close($crl);
		return $ret;
	}
	/** search
	* Sucht alle vorkommen eines Strings in einem Namensraum
	* @Params $Search - Suchstring; $Namespace - Namesraeume
	* @Return serialisiertes Array mit Seiten die den String enthalten
	*/
	private function search ($Search, $Namespace)
	{
		$Offset = 0;
		$b=0;
		while ($Offset != false)
		{
			$data = "action=query&list=search&format=php&srsearch=" . urlencode($Search) . "&srnamespace=" . urlencode($Namespace) . "&srwhat=text&srinfo=totalhits&sroffset=" . urlencode($Offset) . "&srlimit=500&rawcontinue=&assert=bot";
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
			$a=9;
			$Answer = explode ("\"", $website);
			if (isset ($answer["query-continue"]['search sroffset']) === true)
				$Offset = $answer["query-continue"]['search sroffset'];
			else
				$Offset = false;
			while (isset ($answer["query"]['search'][$a]['title']) === true)
			{
				$Site [$b] = $answer["query"]['search'][$a]['title'];
				$b++;
				$a++;
			}
		}
		return serialize ($Site);
	}
}
$Bot = new PurgeMath ("de.wikipedia.org", "Luke081515Bot", "------");
?>
