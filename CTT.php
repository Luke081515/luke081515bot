#!/usr/bin/php
<?php
include './BotCore.php';

class IMPBot extends Core {
	# Status: Stable #
	protected $uuml;
	public function IMPBot ($Account, $Job, $pUseHTTPS = true) {
		$this->initcurl($Account, $Job, $pUseHTTPS = true);
		$Runs = true;
		$this->uuml = "{{subst:Benutzer:Luke081515/u}}";
		while ($Runs === true) {
			$this->addNewTranslations ();
			sleep (5);
			if ($this->readPage ("Benutzer:Luke081515Bot/Import") === "true")
				$this->checkSection ();
			else
				echo ("\nBot gesperrt");
			sleep (5);
		}
	}
    public function addNewTranslations () {
		$data = "action=query&list=recentchanges&format=php&rcdir=older&rctag=contenttranslation&rcprop=title&rclimit=5000&rcshow=!redirect&rawcontinue=&rctype=new";
		try {
			$result = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Answer = explode ("\"", $result);
		unset ($result);
		$i = 13;
		$a = 0;
		while (isset ($Answer [$i]) === true) {
			$Results [$a] = $Answer [$i];
			$Result [$a] = urldecode ($Results [$a]);
			$a++;
			$i = $i + 10;
		}
		unset ($i);
		$a=0;
		while (isset ($Result [$a]) === true)
		{
			if (strstr ($Result [$a], "Benutzer:") !== false) {
				$Username = substr ($Result [$a], 9);
				$Username = strstr ($Username, "/", true);
				$Notification = true;
			}
			else
				$Notification = false;
			$AlreadySet = $this->readPage ("Wikipedia:Importw&uuml;nsche");
			$IgnoreList =  $this->readPage ("Benutzer:Luke081515Bot/NoImport");
			if (strstr ($IgnoreList, "<!--Import:Aktiviert-->") !== false) {
				if ((strstr ($AlreadySet, $Result [$a]) === false) && (strstr ($IgnoreList, $Result [$a]) === false)) {
					if (strstr ($AlreadySet, "{{/Intro}}") !== false) {
						if ($this->readPage ("Benutzer:Luke081515Bot/Import") === "true") {
							$data = "action=query&prop=revisions&format=xml&rvprop=comment&rvlimit=1&rvdir=newer&rvtag=contenttranslation&titles=" .  urlencode($Result [$a]);
							try {
								$result = $this->httpRequest($data, $this->job, 'GET');
							} catch (Exception $e) {
								throw $e;
							}
							$website = $result;
							if (strstr ($website, "[[") !== false) {
								$Source = strstr ($website, "[[");
								$Target = $Source;
								$Source = explode (":", $Source);
								$Lang = $Source [1];
								$Target = strstr ($Target, "|");
								$Target = substr ($Target, 1);
								$Target = strstr ($Target, "]]", true);
								$data = "action=query&prop=revisions&format=xml&rvprop=comment&rvlimit=1&rvdir=newer&titles=" .  urlencode($Result [$a]);
								try {
									$result = $this->httpRequest($data, $this->job, 'GET');
								} catch (Exception $e) {
									throw $e;
								}
								$result = $website;
								if (strstr ($website, "[[") !== false) {
									$Comparision = strstr ($website, "[[");
									$Comparision = strstr ($Comparision, "|");
									$Comparision = substr ($Comparision, 1);
									$Comparision = strstr ($Comparision, "]]", true);
									if ($Comparision === $Target) {
										if ($this->checkAlreadyImported ($Result [$a]) === false) {
											if ($Notification === false)
												$new = $AlreadySet . "\n\n{{subst:Wikipedia:Importw" . $this->uuml . "nsche/Wunsch\n|Sprache=" . $Lang . "\n|Fremdlemma=" . $Target . "\n|Ziel=" . $Result [$a] . "\n|Begr" . $this->uuml . "ndung= Ein Nachimport ist wahrscheinlich notwendig, da die Seite über das Tool „Inhaltsübersetzung“ erstellt wurde. Wenn kein Nachimport erforderlich ist, trage bitte <code><nowiki>* [[:" . $Result [$a] . "]] ~~~~~</nowiki></code> in [https://de.wikipedia.org/w/index.php?title=Benutzer:Luke081515Bot/NoImport&action=edit die Blacklist] ein. Viele Grüße, ~~~~}} <!--Exists--> ";
											else {
												$new = $AlreadySet . "\n\n{{subst:Wikipedia:Importw" . $this->uuml . "nsche/Wunsch\n|Sprache=" . $Lang . "\n|Fremdlemma=" . $Target . "\n|Ziel=" . $Result [$a] . "\n|Begr" . $this->uuml . "ndung= Ein Nachimport ist wahrscheinlich notwendig, da die Seite über das Tool „Inhaltsübersetzung“ erstellt wurde. Wenn kein Nachimport erforderlich ist, trage bitte <code><nowiki>* [[:" . $Result [$a] . "]] ~~~~~</nowiki></code> in [https://de.wikipedia.org/w/index.php?title=Benutzer:Luke081515Bot/NoImport&action=edit die Blacklist] ein. Der Benutzer [[User:" . $Username . "|" . $Username . "]] wurde vom Bot auf seiner Diskussionsseite benachrichtigt. Viele Grüße, ~~~~}} <!--Exists--> ";
												$Message = "\n\n{{subst:Benutzer:Luke081515Bot/ImportMessage|1=:" . $Result [$a] . "}}";
												$this->editPage ("Benutzer Diskussion:" . $Username, $this->readPage("Benutzer Diskussion:" . $Username) . $Message, "Bot:Benachrichtigung aufgrund eines Importantrages", 0);
												sleep (5);
											}
											$new = str_replace ("&#039;", "'", $new);
											$new = str_replace ("&amp;", "'&", $new);
											$this->editPage("Wikipedia:Importw&uuml;nsche", $new, "Bot: Melde &Uuml;bersetzung durch das Tool \"Inhalts&uuml;bersetzung\": [[" . $Result [$a] . "]]");
											echo ("\n Trage " . $Result [$a] . " ein");
											/*if (strstr ($Result [$a], "Kategorie") === false) {
												$List = $this->readPage("Benutzer:Luke081515/CTT");
												$List = $List . "\n* [[:" . $Result [$a] . "]]";
												$this->editPage("Benutzer:Luke081515/CTT", $List, "Bot: Neue CTT-&Uuml;bersetzung vorhanden");
											}*/
											sleep (5);
										}
									}
								}
							}
						}
						else
							echo ("\n Bot gesperrt!");
					}
				}
			}
			$a++;
		}
	}
	public function checkSection () {
		$a=1;
		$Set = true;
		while ($Set === true) {
			if (strstr ($this->readSection ("Wikipedia:Importw&uuml;nsche", $a), "==") === false)
				$Set = false;
			$Site = $this->getDewikiName ($a);
			if (strstr ($this->readSection ("Wikipedia:Importw&uuml;nsche", $a), "<!--Exists-->") !== false)
				$this->CheckPage ($Site, $a, "IMP");
			$a++;
		}
		$a=1;
		$Set = true;
		while ($Set === true) {
			if (strstr ($this->readSection ("Wikipedia:Importw&uuml;nsche/Importupload", $a), "==") === false)
				$Set = false;
			$Site = $this->getDewikiName ($a);
			if (strstr ($this->readSection ("Wikipedia:Importw&uuml;nsche/Importupload", $a), "<!--Exists-->") !== false)
				$this->CheckPage ($Site, $a, "IU");
			$a++;
		}
	}
	public function CheckPage ($Site, $Section, $Page) {
		if ($Page === "IMP") {
			$IMPPage = "Wikipedia:Importw&uuml;nsche";
		} else if ($Page === "IU") {
			$IMPPage = "Wikipedia:Importw&uuml;nsche/Importupload";
		} else {
			throw new Exception('Fehler im Programm');
		}
		$Content = $this->readPage ($Site);
		$Low = strtolower ($Content);
		$Work = false;
		$data = "action=query&prop=revisions&format=php&rvprop=comment&rvlimit=1&rvdir=newer&rvtag=contenttranslation&titles=" .  urlencode($Site);
		try {
			$pagestatus = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		if (strstr ($pagestatus, "missing") === false) {
			if ($this->wasMoved ($Site) !== -1) {
				$NewLemma = $this->wasMoved ($Site);
				if ((strstr ($NewLemma, "User:") !== false) || (strstr ($NewLemma, "Benutzer:") !== false) || (strstr ($NewLemma, "Benutzerin:") !== false)) {
					$Username = substr ($NewLemma, 9);
					$Username = strstr ($Username, "/", true);
					$this->giveMoveNotice ($Site, $NewLemma, $Username);
				}
				$Work = true;
				$this->fixTitle ($IMPPage, $Site, $NewLemma, $Section);
			} else if (strstr ($this->readSection ($IMPPage, $Section), "LD") === false) {
				if ($this->checkLA ($Site) === true)
					$this->addDeleteNote ($Page, $Section);
				$Work = true;
			} else if (strstr ($this->readSection ($IMPPage, $Section), "nun nachimportiert werden") === false) {
				if ($this->checkLA ($Site) === false)
					$this->removeLDComment ($Page, $Section);
				$Work = true;
			}
		}
		else if (strstr ($pagestatus, "missing") !== false) {
			if ($this->wasDeleted ($Site) === true) {
				$this->PageDeleted ($Page, $Section);
				$Work = true;
			}
		}
		# Bei WLs #
		if ((strstr ($pagestatus, "missing") !== false) && (strstr ($Low, "#redirect") !== false || strstr ($Low, "#weiterleitung") || strstr ($Low, "# redirect") || strstr ($Low, "# weiterleitung"))) {
			if ($Work === true) {}
			else {
				if ($this->wasMoved ($Site) !== -1) {
					$NewLemma = $this->wasMoved ($Site);
					if ((strstr ($NewLemma, "User:") !== false) || (strstr ($NewLemma, "Benutzer:") !== false) || (strstr ($NewLemma, "Benutzerin:") !== false)) {
						$Username = substr ($NewLemma, 9);
						$Username = strstr ($Username, "/", true);
						$this->giveMoveNotice ($Site, $NewLemma, $Username);
					}
					$this->fixTitle ($IMPPage, $Site, $NewLemma, $Section);
				} else if ($this->wasDeleted ($Site) === true) {
					$this->PageDeleted ($Page, $Section);
				} else if (strstr ($this->readSection ($IMPPage, $Section), "LD") === false) {
					if ($this->checkLA ($Site) === true)
						$this->addDeleteNote ($Page, $Section);
				} else if (strstr ($this->readSection ($IMPPage, $Section), "nun nachimportiert werden") === false) {
					if ($this->checkLA ($Site) === false)
						$this->removeLDComment ($Page, $Section);
				}
			}
		}
	}
	public function wasMoved ($Site) {
		$data = "action=query&list=logevents&format=php&letype=move&ledir=older&letitle=" . urlencode ($Site) . "&lelimit=500&rawcontinue=";
		try {
			$result = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Log = explode ("\"", $result);
		if (isset ($Log [23]) === true)
			return $Log [23];
		return -1;
	}
	public function wasDeleted ($Site) {
		$data = "action=query&list=logevents&format=php&letype=delete&ledir=older&letitle=" . urlencode ($Site) . "&lelimit=500&rawcontinue=";
		try {
			$result = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Log = explode ("\"", $result);
		if (isset ($Log [29]) === true)
			return true;
		return false;
	}
	public function checkAlreadyImported ($Site) {
		$data = "action=query&list=logevents&format=php&letype=import&ledir=older&letitle=" . urlencode ($Site) . "&lelimit=500&rawcontinue=";
		try {
			$result = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Log = explode ("\"", $result);
		if (strstr ($result, "import") !== false)
			return true;
		return false;
	}
	public function addDeleteNote ($Page, $Section) {
		$Content = $this->readSection ($Page, $Section);
		$Content = str_replace ("* '''Erledigungsvermerk/Anmerkung:'''", "* '''Erledigungsvermerk/Anmerkung:''' <mark>Befindet sich in einer Löschdiskussion.</mark> --~~~~\n", $Content);
		$this->editSection ($Page, $Content, "Bot: Ein L&ouml;schantrag wurde gestellt", $Section);
	}
	public function PageDeleted ($Page, $Section) {
		$this->editSection ("Wikipedia:Importw&uuml;nsche", "", "Bot: Artikel wurde gel&ouml;scht", $Section);
	}
	public function giveMoveNotice ($OldLemma, $NewLemma, $User) {
		$Message = "{{subst:User:Luke081515Bot/MoveNotice|1=:" . $OldLemma . "|2=:" . $NewLemma . "}}";
		$Content = $this->readPage ("Benutzer Diskussion:" . $User);
		$Content = $Content . "\n\n" . $Message;
		$this->editPage ("Benutzer Diskussion:" . $User, $Content, "Bot: Benachrichtigung, dass deine &Uuml;bersetzung verschoben wurde");
	}
	public function fixTitle ($Page, $OldLemma, $NewLemma, $Section) {
		$Content = $this->readSection ($Page, $Section);
		$Content = str_replace ("* [[:" . $OldLemma, "* [[:" . $NewLemma, $Content); // Ersetze den C&P Vorschlag für die Blacklist
		$Content = str_replace ("[{{fullurl:" . $OldLemma, "[{{fullurl:" . $NewLemma, $Content); // Ersetze den Link auf die History
		$Content = str_replace ("[[" . $OldLemma . "]]", "[[" . $NewLemma . "]]", $Content); // Ersetze den Link im Titel
		$Content = str_replace ("* '''Erledigungsvermerk/Anmerkung:'''", "* '''Erledigungsvermerk/Anmerkung:'''\n<small>Ziellemma wurde nach Verschiebung angepasst, ursprüngliches Ziellemma war [[:" . $OldLemma . "]]. ~~~~</small> ", $Content);
		$this->editSection ($Page, $Content, "Bot: Aktualisiere Eintrag nach Verschiebung", $Section);
	}
	public function removeLDComment ($Page, $Section) {
		$Content = $this->readSection ($Page, $Section);
		$ContentBefore = strstr ($Content, "<mark>Befindet sich in einer Löschdiskussion.</mark>", true);
		$ContentAfter = "<!-- nach erfolgreichem Import bitte ganzen Abschnitt inkl. Überschrift löschen --> <!--Exists-->";
		$Content = $ContentBefore . $ContentAfter;
		$this->editSection ($Page, $Content, "Bot: L&ouml;schantrag wurde entfernt", $Section);
	}
	public function checkLA ($Site) {
		if (strstr ($Content, "schantragstext") !== false)
			return true;
		return false;
	}
	public function getDewikiName ($Section) {
		$Content = $this->readSection ("Wikipedia:Importw&uuml;nsche", $Section);
		$Content = strstr ($Content, "[["); // Spezial:Import
		$Content = substr ($Content, 2);
		$Content = strstr ($Content, "[[:"); // Fremdlemma
		$Content = substr ($Content, 3);
		$Content = strstr ($Content, "[["); // Zielseite
		$Content = substr ($Content, 2);
		while (substr ($Content, 0, 1) === ':') // Seite mit ":" vorher angegeben? Ingoriere den Punkt
			$Content = substr ($Content, 1);
		$Content = strstr ($Content, "]]", true);
		return $Content;
	}
}
$Bot = new IMPBot('Luke081515Bot@dewiki', 'CTT');
?>