#!/usr/bin/php
<?php
include 'BotCore.php';
/** WeblinkSearcher
* Sucht nach Weblinks in Vorlagen
* @Author Luke081515
* @Version 0.7
* @Status Alpha
*/
class WeblinkSearcher extends Core
{
	public function WeblinkSearcher ($Account, $Job, $pUseHTTPS = true) {
		$this->initcurl($Account, $Job, $pUseHTTPS = true);
		$this->main (10);
	}
	/** main
	* Trigger suche im Namensraum
	* @Param $Namespace - Namensraum
	*/
	public function main ($Namespace) {
		# Auslesen aller Seiten #
		$AllPages = unserialize ($this->getAllPages ($Namespace));
		
		# Überprüfe auf Weblinks #
		$a=0;
		$b=0;
		$c=0;
		while (isset ($AllPages [$a]) === true) {
			$Content = $this->readPage ($AllPages [$a]);
			$Result = $this->CheckPage ($Content);
			if (isset ($Result [2]) === true)
				$Result [2] = unserialize ($Result [2]);
			if ($Result [0] === false) {}
			else {
				$Number = $this->getAllEmbedings ($AllPages [$a]);
				$n=0;
				$LinkList = unserialize ($Result [1]);
				if ($Result [0] === 1) {
					while (isset ($LinkList [$n]) === true) {
						$ParameterList [$b] = array ($AllPages [$a], $LinkList [$n], $Number, $Result [2] [$n]);
						$b++;
						$n++;
					}
				} else if ($Result [0] === 2) {
					while (isset ($LinkList [$n]) === true) {
						$JustURLList [$c] = array ($AllPages [$a], $LinkList [$n], $Number, $Result [2] [$n]);
						$c++;
						$n++;						
					}						
				}
				else {}
			}
			$a++;
		}
		$WriteContent = "\n== Vorlagen mit Link ==\n=== Vorlagen mit URL, beeinflusst durch Parameter ===";
		//$WriteContent = $WriteContent . "\n{|class=\"wikitable sortable\"\n!Seite\n!Anzahl der Einbindungen\n!Link\n!Weblinksuche\n|-";
		$WriteContent = $WriteContent . "\n{|class=\"wikitable sortable\"\n!Seite\n!Anzahl der Einbindungen\n!Link\n|-";
		$a=0;
		while (isset ($ParameterList [$a] [0]) === true) {	
			//$WriteContent = $WriteContent . "\n|-\n|[[" . $ParameterList [$a] [0] . "]]\n|" . $ParameterList [$a] [2] . "\n|<code><nowiki>" . $ParameterList [$a] [1] . "</nowiki></code>\n|" . $ParameterList [$a] [3];
			$WriteContent = $WriteContent . "\n|-\n|[[" . $ParameterList [$a] [0] . "]]\n|" . $ParameterList [$a] [2] . "\n|<code><nowiki>" . $ParameterList [$a] [1] . "</nowiki></code>";
			$a++;
		}
		$WriteContent = $WriteContent . "\n|}\n\n=== Vorlagen mit URL, nicht beeinflusst ===";
		//$WriteContent = $WriteContent . "\n{|class=\"wikitable sortable\"\n!Seite\n!Anzahl der Einbindungen\n!Link\n!Weblinksuche\n|-";
		$WriteContent = $WriteContent . "\n{|class=\"wikitable sortable\"\n!Seite\n!Anzahl der Einbindungen\n!Link\n|-";
		$a=0;
		while (isset ($JustURLList [$a] [0]) === true) {	
			//$WriteContent = $WriteContent . "\n|-\n|" . $JustURLList [$a] [0] . "\n|" . $JustURLList [$a] [2] . "\n|<code><nowiki>" . $JustURLList [$a] [1] . "</nowiki></code>\n|" . $JustURLList [$a] [3];
			$WriteContent = $WriteContent . "\n|-\n|[[" . $JustURLList [$a] [0] . "]]\n|" . $JustURLList [$a] [2] . "\n|<code><nowiki>" . $JustURLList [$a] [1] . "</nowiki></code>";
			$a++;
		}
		$WriteContent = $WriteContent . "\n|}\n\n--~~~~";
		echo ($WriteContent);
		$this->savePage ("Benutzer:Mabschaaf/Vorlagen mit Weblinks", $WriteContent, "Bot: Test");
	}
	/** checkPage
	* @ToDo
	*/
	private function checkPage ($Content) {
		$a=0;
		while (strstr ($Content, "<noinclude>") === true) {
			$Remove = strstr ($Content, "<noinclude>", false);
			$Remove = strstr ($Remove, "</noinclude>", true);
			$str_replace ($Remove, "", $Content);
		}
		if (strstr ($Content, "[http://") === false && strstr ($Content, "[https://") === false)
			return array (false, "");
		$Set = 0;
		while (strstr ($Content, "[http://") !== false || strstr ($Content, "[https://") !== false) {
			if (strstr ($Content, "[http://") !== false)
				$Link = strstr ($Content, "[http://");
			else
				$Link = strstr ($Content, "[https://");
			$Content = strstr ($Link, "]", false);
			$Link = strstr ($Link, "]", true);
			$ret [$a] = $Link . "]";
			$a++;
		}
		$a=0;
		while (isset ($ret [$a]) === true) {	
			echo ("\n" . $ret [$a]);
			if (strstr ($ret [$a], "{{") !== false) { // Link von Parameter beinflusst
				while (isset ($ret [$a]) === true) {
					$LinkSearch [$a] = strstr ($ret [$a], "{{", true);
					$LinkSearch [$a] = $LinkSearch [$a] . "%";
					$LinkSearch [$a] = "{{Giftbotweblinksuche|namespace=0|url=" . $LinkSearch [$a] . "}}";
					$a++;
				}
				$LinkSearch = serialize ($LinkSearch);
				$ret = serialize ($ret);
				return array (1, $ret, $LinkSearch);
			} else {
				while (isset ($ret [$a]) === true) {
					$LinkSearch [$a] = "{{Giftbotweblinksuche|namespace=0|url=" . $ret [$a] . "}}";
					$a++;
				}
				$LinkSearch = serialize ($LinkSearch);
				$ret = serialize ($ret);
				return array (2, $ret, $LinkSearch);
			}
			$a++;
		}
	}
	/** getAllPages
	* @ToDo
	*/
	public function getAllPages ($Namespace) {
		$Continue = "";
		$data = "action=query&list=allpages&format=php&apcontinue=" . $Continue . "&apnamespace=" . $Namespace . "&aplimit=5000&apdir=ascending&rawcontinue=";
		try {
			$result = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Answer = explode ("\"", $result);
		$Sites = "";
		$q = 0;
		while (strstr ($website, "apcontinue") !== false) {
			$b=19;
			$Continue = $Answer [7];
			echo ("\n Continue: " . $Continue);
			while (isset($Answer [$b]) === true) {
				$Exception = false;
				$x = $b + 1;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$b] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$b++;
					$x = $b + 1;
				}
				if ($Exception === true)
					$Sites [$q] = $New;
				else
					$Sites [$q] = $Answer [$b];
				$b = $b + 8;
				$q++;
			}
			$data = "action=query&list=allpages&format=php&apcontinue=" . $Continue . "&apnamespace=" . $Namespace . "&aplimit=5000&apdir=ascending&rawcontinue=";
			try {
				$result = $this->httpRequest($data, $this->job, 'GET');
			} catch (Exception $e) {
				throw $e;
			}
			$Answer = explode ("\"", $result);
		}
		$b=11;
		$data = "action=query&list=allpages&format=php&apcontinue=" . $Continue . "&apnamespace=" . $Namespace . "&aplimit=5000&apdir=ascending&rawcontinue=";
		try {
			$result = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Answer = explode ("\"", $result);
		while (isset($Answer [$b]) === true) {
			$Exception = false;
			$x = $b + 1;
			while (strstr ($Answer [$x], ";}") === false) {
				if ($Exception === false)
					$New = $Answer [$b] . "\"" . $Answer [$x];
				else
					$New = $New . "\"" . $Answer [$x];
				$Exception = true;
				$b++;
				$x = $b + 1;
			}
			if ($Exception === true)
				$Sites [$q] = $New;
			else
				$Sites [$q] = $Answer [$b];
			$b = $b + 8;
			$q++;
		}
		$ret = serialize ($Sites);
		return $ret;
	}
	/** getAllEmbedings
	* @ToDo
	*/
	public function getAllEmbedings ($Templ) {
		$b=0;
		$z=0;
		if (isset ($Templ) === true) {
			$data = "action=query&list=embeddedin&format=php&eititle=" . urlencode($Templ) . "&einamespace=0&eidir=ascending&eifilterredir=nonredirects&eilimit=5000&rawcontinue=";
			try {
				$result = $this->httpRequest($data, $this->job, 'GET');
			} catch (Exception $e) {
				throw $e;
			}
			unset ($Answer);
			$Answer = explode ("\"", $result);
			while (strstr ($website, "eicontinue") !== false) {
				$a=19;
				$Continue = $Answer [7];
				while (isset ($Answer [$a]) === true) {
					$x = $a + 1;
					$Exception = false;
					while (strstr ($Answer [$x], ";}") === false) {
						if ($Exception === false)
							$New = $Answer [$a] . "\"" . $Answer [$x];
						else
							$New = $New . "\"" . $Answer [$x];
						$Exception = true;
						$a++;
						$x = $a + 1;
					}
					if ($Exception === true)
						$Result [$b] = $New;
					else
						$Result [$b] = $Answer [$a];
					$a = $a +  8;
					$b++;
				} 
				unset ($Answer);
				$data = "action=query&list=embeddedin&format=php&eititle=" . urlencode($Templ) . "&einamespace=0&eicontinue=" . urlencode($Continue) .  "&eidir=ascending&eifilterredir=nonredirects&eilimit=5000&rawcontinue=";
				try {
					$result = $this->httpRequest($data, $this->job, 'GET');
				} catch (Exception $e) {
					throw $e;
				}
				$Answer = explode ("\"", $result);
			}
			$a=11;
			while (isset ($Answer [$a]) === true) {
				$x = $a + 1;
				$Exception = false;
				while (strstr ($Answer [$x], ";}") === false) {
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$a = $a +  8;
				$b++;
			}
			$z++;
		}
		$a=0;
		while (isset ($Result [$a]) === true)
			$a++;
		$a++;
		return $a;
	}
}
$Bot = new WeblinkSearcher ("de.wikipedia.org", "Luke081515Bot", "------");
?>