#!/usr/bin/php
<?php
include '../BotCore.php';
class CoreTest {
	/** CoreTest
	* Testklasse von BotCore
	* Setze $DepenciesFailed auf true, sobald ein test alle anderen auch zum crashen bringt
	** Beispiel: Login geht nicht, dann kann auch keine Seite gespeichert werden
	*/
	public function CoreTest () {
		global $argv;
		$pwd = $argv[1];
		$Passed = 0;
		$Failed = 0;
		$Skipped = 0;
		$DepenciesFailed = false;
		$Bot = new Core ('Luke081515Bot@test-cac', 'BotCoreTest');
		// Teste Login
		if ($DepenciesFailed === false) {
			try {
				echo ("\nTeste Login....");
				$Bot->setSite ('test-cac.wmflabs.org');
				$Bot->setUsername ('JenkinsTestUser');
				$Bot->setPassword ($pwd);
				$Bot->testInitcurl ('CoreTest', true);
				echo ("\nLogge ein...");
				$Bot->login ();
				echo ("\nResult: SUCCESS");
				$Passed++;
			} catch (Exception $e) {
				echo ("\nResult: FAILED");
				$Failed++;
				$DepenciesFailed = true; // Login ist essentiell
			}
		} else {
			$Skipped++;
		}
		// Teste das Erstellen einer Seite, zudem wird der Lesezugriff mitgetestet
		if ($DepenciesFailed === false) {
			try {
				echo ("\nTeste das Speichern einer Seite....");
				$Bot->editPage ("SaveTest", "\n\nReadContentTest", "", 0);
				sleep (30);
				echo ("\nTeste das Speichern einer Seite, mit vorher eingelesenen Informationen");
				$Content = $Bot->readPage ("SaveTest");
				if (strstr ($Content, "ReadContentTest") === false)
					echo ('Result: FAILED: Lesezugriff fehlgeschlagen: readPage');
				$Bot->editPage ("SaveTest", "== Section 1 ==\nTestSectionContent\n== Section 2 ==\n\n== Section 3 ==", "");
				sleep (30);
				$Content = $Bot->readSection ("SaveTest", 1);
				if (strstr ($Content, "TestSectionContent") === false)
					echo ('Result: FAILED: Lesezugriff fehlgeschlagen: readSection');
				$Bot->editSection ("SaveTest", "TestForNextSection", "", 2);
				sleep (30);
				echo ("\nResult: SUCCESS");
				$Passed++;
			} catch (Exception $e) {
				echo ("\nResult: FAILED");
				$Failed++;
			}
		} else {
			$Skipped++;
		}
		// Gebe Ergebnisse aus
		echo ("\nTESTS FINISHED\nRESULT: " . $Passed . " Tests passed, " . $Failed . " Tests failed and " . $Skipped . " Tests skipped");
		if ($Failed < 1) {
			echo ("\nAll tests passed");
			die (0);
		} else {
			echo ("\nThere are tests with failures");
			die (1);
		}
	}
}
$CoreTest = new CoreTest();
?>