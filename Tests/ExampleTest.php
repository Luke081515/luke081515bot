#!/usr/bin/php
<?php
include '../BotCore.php';
class ExampleTest {
	/** ExampleTest
	* Beispielprogramm einer Testklasse
	* Setze $DepenciesFailed auf true, sobald ein test alle anderen auch zum crashen bringt
	** Beispiel: Login geht nicht, dann kann auch keine Seite gespeichert werden
	*/
	public function ExampleTest () {
		global $argv;
		$pwd = $argv[1];
		$Passed = 0;
		$Failed = 0;
		$Skipped = 0;
		$DepenciesFailed = false;
		$Bot = new Core ('Luke081515Bot@test-cac', 'ExampleTest'); // Logindaten sind erforderlich, werden aber nicht beachtet
		// Teste Login
		if ($DepenciesFailed === false) {
			try {
				echo ("\nTeste Login....");
				$Bot->setSite ('test-cac.wmflabs.org');
				$Bot->setUsername ('JenkinsTestUser');
				$Bot->setPassword ($pwd);
				$Bot->testInitcurl ('CoreTest', true);
				echo ("\nLogge ein...");
				$Bot->login ();
				echo ("\nResult: SUCCESS");
				$Passed++;
			} catch (Exception $e) {
				echo ("\nResult: FAILED");
				$Failed++;
				$DepenciesFailed = true; // Login ist essentiell
			}
		} else {
			$Skipped++;
		}
		// Teste das Erstellen einer Seite, zudem wird der Lesezugriff mitgetestet
		if ($DepenciesFailed === false) {
			try {
				echo ("\nTeste xy....");
				// Definiere hier einen Test, mit einem Check, ob er erfolgreich war
				echo ("\nResult: SUCCESS");
				$Passed++;
			} catch (Exception $e) {
				echo ("\nResult: FAILED");
				$Failed++;
			}
		} else {
			$Skipped++;
		}
		// Gebe Ergebnisse aus
		echo ("\nTESTS FINISHED\nRESULT: " . $Passed . " Tests passed, " . $Failed . " Tests failed and " . $Skipped . " Tests skipped");
		if ($Failed < 1) {
			echo ("\nAll tests passed");
			die (0); // Build erfolgreich
		} else {
			echo ("\nThere are tests with failures");
			die (1); // Lasse den Build fehlschlagen
		}
	}
}
$ExampleTest = new ExampleTest();
?>