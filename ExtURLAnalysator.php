#!/usr/bin/php
<?php
include 'BotCore.php';
###################
## Status: Alpha ##
###################
class ExtURLAnalysator extends Core
{
	protected $host;
	protected $username;
	protected $password;
	protected $cookies;
	public $bot = 1; // set to 0 if this edit should not be marked as bot edit

	public function ExtURLAnalysator ($host, $username, $password)
	{
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
		$this->main ();
	}
	public function main ()
	{}
	
	/** getParamContent
	* Liest den Inhalt einzelner Parameter aus
	* Parameter: $ParamName - Parametername, $Template - Vorlagenname, $Page - zu untersuchende Seite
	* Returns: false, falls Vorlage nicht vorhanden, oder Parameterinhalt als String
	*/
	private function getParamContent ($ParamName, $Template, $Page)
	{
		$Content = $this->readPage ($Page);
		$Content = str_replace ("\n", "", $Content);
		$Content = str_replace (" ", "", $Content);
		if (strstr ($Content, "{{" . $Template) === false)
			return false;
		$TempContent = strstr ($Content, "{{" . $Template);
		$TempContent = substr ($TempContent, 2);
		$TemplateContent = strstr ($TempContent, "}}", true);
		if (strstr ($TemplateContent, "{{") !== false)
		{
			$TempContent = strstr ($TempContent, "}}");
			$AddedContent = substr ($TempContent, 2);
			$AddedContent = strstr ($AddedContent, "}}", true);
			$TemplateContent = $TemplateContent . "}}" . $AddedContent;
		}
		while (strstr ($TempContent, "{{") !== false)
		{
			$TempContent = strstr ($TempContent, "}}");
			$AddedContent = substr ($TempContent, 2);
			$AddedContent = strstr ($AddedContent, "}}", true);
			$TemplateContent = $TemplateContent . "}}" . $AddedContent;
		}
		if (strstr ($TemplateContent, $ParamName) === false)
			return -1;
		$ParamContent = strstr ($TemplateContent, "|" . $ParamName);
		$ParamContent = strstr ($ParamContent, "=");
		$ParamContent = substr ($ParamContent, 1);
		$ParamContent = strstr ($ParamContent, "|", true);
		return $ParamContent;
	}
	
	/**
	* Liest angegebene URL aus
	* Parameter: $URL - URL die aufgerufen wird, mit Protokoll
	* Returns: String: Quelltext der Seite
	*/
	public function readPage($URL)
	{
		$crl = curl_init();
		curl_setopt ($crl, CURLOPT_URL, urlencode($URL));
		curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, 30); // 30 seconds
		curl_setopt ($crl, CURLOPT_USERAGENT, "LukeBot"); 
		$ret = curl_exec($crl);
		curl_close($crl);
		return $ret;    
	}
}
$Bot = new ExtURLAnalysator ("de.wikipedia.org", "Luke081515Bot", "------");
?>