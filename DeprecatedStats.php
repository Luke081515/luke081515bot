#!/usr/bin/php
<?php
include 'BotCore.php';
/** DeprecatedStats.php
* Sucht nach veralteten Daten in Vorlagen
* @Author Luke081515
* @Version 0.2
* @Status Alpha
*/
class DepreBot extends Core
{
	public function DepreBot ($Account, $Job, $pUseHTTPS = true) {
		$this->initcurl($Account, $Job, $pUseHTTPS = true);
		$Settings = ""; //ToDo
		$a=0;
		$b=0;
		$z=0;
		$q=0;
		# Auslesen der Wartungsbedürftigen Fälle #
		while (isset ($Settings ['writeSettings'] ['possible'] [$a]) === true) {
			$Possible [$b] = $Settings ['writeSettings'] ['possible'] [$a];
			$b++;
			$a++;			
		}
		$a=0;
		$b=0;
		while (isset ($Settings ['SearchSettings'] ['Templates'] [$a]) === true) {
			$Templates [$b] = $Settings ['SearchSettings'] ['Templates'] [$a];
			$b++;
			$a++;			
		}
		$a=0;
		while (isset ($Templates [$a]) === true) {
			$EmbeddingsS = $this->getAllEmbedings($Templates [$a]);
			$Embed [$a] = unserialize ($EmbeddingsS);
			$a++;			
		}
		$a=0;
		$b=0;
		while (isset ($Embed [$a] [$b]) === true) {
			$b=0;
			while (isset ($Embed [$a] [$b]) === true) {
				$Result = $this->getParamContent ($TemplateName [$a], $Templates [$a], $Embed [$a] [$b]);
				$c=0;
				$Found = false;
				while (isset ($Settings ['writeSettings'] ['possible'] [$c]) === true) {
					if (strstr ($Result, $Settings ['writeSettings'] ['possible'] [$c]) !== false) {
						$EndResult [$c] [$z] = $Embed [$a] [$b];
						$z++;
						$Found = true;
					}
					$c++;
				}
				if ($Found === false) {
					$NotFound [$q] = $Embed [$a] [$b];
					$q++;
				}
				$b++;
			}
			$a++;
		}
		# Schreiben der Ergebnisse #
		$a=0;
		$b=0;
		$c=0;
		while (isset ($Settings ['writeSettings'] ['possible'] [$a]) === true) {
			if (isset ($EndResult [$a] [$b]) === true) {
				$Sec = $Settings ['writeSettings'] ['sections'] [$Settings ['writeSettings'] ['possible'] [$c]];
				$Content = $this->readSection ($Settings ['writeSettings'] ['Page'], $Sec);
				while (isset ($EndResult [$a] [$b]) === true) {
					$Content = $Content . "\n* [[" . $EndResult [$a] [$b] . "]]";
					$b++;
				}
				$this->editSection ($Settings ['writeSettings'] ['Page'], $Content, "Bot: Aktualisiere Liste", $Sec);
			}
			$a++;
		}
		if (isset ($NotFound [0]) === true) {
			$q=0;
			$Content = $this->readSection ($Settings ['writeSettings'] ['Page'], $Settings ['writeSettings'] ['OptionUnset']);
			while (isset ($NotFound [$q]) === true) {
				$Content = $Content . "\n* [[" . $NotFound [$q] . "]]";
				$q++;
			}
			$this->editSection ($Settings ['writeSettings'] ['Page'], $Content, "Bot: Aktualisiere Liste", $Settings ['writeSettings'] ['OptionUnset']);
		}
	}
	/** getParamContent
	* Liest eine Parameter aus einer Vorlage aus
	* @Param $ParamName - Name des Parameters; $Template - Name der Vorlage; $Page - Name der Seite mit der Vorlage
	* @return String, Parameterinhalt
	*/
	private function getParamContent ($ParamName, $Template, $Page) {
		$Content = $this->readPage ($Page);
		$Content = str_replace ("\n", "", $Content);
		$Content = str_replace (" ", "", $Content);
		if (strstr ($Content, "{{" . $Template) === false)
			return false;
		$TempContent = strstr ($Content, "{{" . $Template);
		$TempContent = substr ($TempContent, 2);
		$TemplateContent = strstr ($TempContent, "}}", true);
		if (strstr ($TemplateContent, "{{") !== false) {
			$TempContent = strstr ($TempContent, "}}");
			$AddedContent = substr ($TempContent, 2);
			$AddedContent = strstr ($AddedContent, "}}", true);
			$TemplateContent = $TemplateContent . "}}" . $AddedContent;
		}
		while (strstr ($TempContent, "{{") !== false) {
			$TempContent = strstr ($TempContent, "}}");
			$AddedContent = substr ($TempContent, 2);
			$AddedContent = strstr ($AddedContent, "}}", true);
			$TemplateContent = $TemplateContent . "}}" . $AddedContent;
		}
		if (strstr ($TemplateContent, $ParamName) === false)
			return -1;
		$ParamContent = strstr ($TemplateContent, "|" . $ParamName);
		$ParamContent = strstr ($ParamContent, "=");
		$ParamContent = substr ($ParamContent, 1);
		$ParamContent = strstr ($ParamContent, "|", true);
		return $ParamContent;
	}
}
$Bot = new DepreBot ('Luke081515Bot@dewiki', 'StatsBot');	
?>