#!/usr/bin/php
<?php
include './BotCore.php';
/** SearchReplace.php
* Sucht alle Vorkommen mit einem Suchstring, und ersetzt sie
* @Author Luke081515
* @Version 0.3
* @Status Alpha
*/
class SReplace extends Core {
	public function SearchReplace($Account, $Job, $pUseHTTPS = true)
    {
		$this->initcurl($Account, $Job, $pUseHTTPS = true);
		$Search = array (
			"<!-- österreichbezogen -->",
			"<!--österreichbezogen-->",
			"<!-- österreichbezogen-->",
			"<!--österreichbezogen -->",
		);
		$Replace = "{{Österreichbezogen}}";
		$Namespace = 0;
		$a=0;
		while (isset ($Search [$a]) === true) {
			$Result = unserialize($this->search ($Search [$a], $Namespace));
			$b=0;
			while (isset ($Result [$b]))  {
				$this->replace ($Result [$b], $Search [$a], $Replace, 2, "Ersetze österreichbezogen durch Vorlage, siehe <Link>");
				$b++;
			}
			$a++;
		}						
	}
	/** replace
	* Ersetzt die Daten
	* @Params $Site - Seite auf der gearbeitet wird; $Sets - Serialisiertes Array mit zuersetzenden Strings; $Targets - Serialisiertes Array mit Strings durch die $Sets ersetzt werden sollen; $Time - zu wartende Zeit nach dem Edit; $Summary - Zusammenfassung des Edits, ohne Bot: Prefix
	*/
	private function replace ($Site, $Sets, $Targets, $Time, $Summary)
	{
		echo ("\n Schreibe");
		$Set = unserialize ($Sets);
		$Target = unserialize ($Targets);
		$Replaced = $this->readPage ($Site);
		$a=0;
		while (isset ($Set [$a]) === true && isset ($Target [$a]) === true) {
			$Set [$a] = str_replace ("{{pipe}}", "|", $Set [$a]);
			$Target [$a] = str_replace ("{{pipe}}", "|", $Target [$a]);
			$Replaced = str_replace ($Set[$a], $Target[$a] . " ", $Replaced);
			echo ("\nSite:" . $Site);
			echo ("\nSet:" . $Set[$a]);
			echo ("\nTarget:" . $Target[$a]);
			$a++;
		}
		$Result = $this->editPage($Site, $Replaced, "Bot: " . $Summary);
		echo ("\n" . $Result);
		if (strstr ($Result, "error") !== false)
			return -1;
		if (strstr ($Result, "nochange") !== false)
			return -2;
		sleep ($Time);
		return $a;
	}
	/** search
	* Sucht alle vorkommen eines Strings in einem Namensraum
	* @Params $Search - Suchstring; $Namespace - Namesraeume
	* @Return serialisiertes Array mit Seiten die den String enthalten
	*/
	private function search ($Search, $Namespace) {
		$Offset = 0;
		$b=0;
		while ($Offset != false)
		{
			$data = "action=query&list=search&format=php&srsearch=" . urlencode($Search) . "&srnamespace=" . urlencode($Namespace) . "&srwhat=text&srinfo=totalhits&sroffset=" . urlencode($Offset) . "&srlimit=500&rawcontinue=&assert=bot";
			try {
				$result = $this->httpRequest($data, $this->job, 'GET');
			} catch (Exception $e) {
				throw $e;
			}
			$a=9;
			$Answer = explode ("\"", $result);
			if (isset ($answer["query-continue"]['search sroffset']) === true)
				$Offset = $answer["query-continue"]['search sroffset'];
			else 
				$Offset = false;
			while (isset ($answer["query"]['search'][$a]['title']) === true) {
				$Site [$b] = $answer["query"]['search'][$a]['title'];	
				$b++;
				$a++;
			}
		}
		return serialize ($Site);
	}
}
$IBCT = new SsearchReplace('Luke081515Bot@dewiki', 'SearchReplace');
?>
