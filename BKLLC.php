#!/usr/bin/php
<?php
include './BotCore.php';
include './DBCore.php';
/** BKLLC.php
* Sucht nach Links auf BKLs, und kategorisiert diese, um Wartungslisten zu erstellen
* Required: DB-Anbindung
* @Author Luke081515
* @Version 0.2
* @Status Alpha
*/
class BKLLC extends Core {
	protected $DB;
	public function BKLLC ($Account, $Job, $pUseHTTPS = true) {
		$this->initcurl($Account, $Job, $pUseHTTPS = true);
		$this->connectToDatabase ();
		$this->fillBKLTable ();
	}
	/** connectToDatabase
	* Verbindet sich nach den Daten aus der Passwort-Datei mit der Datenbank
	*/
	protected function connectToDatabase () {
		$this->DB = new DBCore ('root@localhost', 'bkllc');
	}	
	/** fillBKLTable
	* In die Tabelle der BKLs werden BKLs geschrieben
	*/
	protected function fillBKLTable () {
		$sql = 'SELECT DisID FROM Disambig;';
		$result = $this->DB->query ($sql);
		$a = 0;
		while ($row = $result->fetch_assoc()){
			$AlreadySet [$a] = $row['DisID']; // Ruft alle bestehende Infos ab
			$a++;
		}
		$result->free();
		$ListIDs = unserialize ($this->getCatMembers ("Kategorie:Begriffskl&auml;rung"));
		echo ("\nstart");
		$a=0;
		while (isset ($ListIDs [$a])) {
			$sql = 'SELECT DisID FROM Disambig WHERE DisID = ' . $ListIDs [$a] . ';';
			$result = $this->DB->query ($sql);
			if ($result->num_rows == 0) {
				$sql = 'INSERT INTO Disambig SET DisID = ' . $ListIDs [$a] . ', TotalLinks = 0;';
				echo ("\n" . $sql);
				$this->DB->modify ($sql);
			}
			$a++;
		}
		
	}
	/** fillPagesTable
	* @ToDo
	*/
	protected function fillPagesTable () {
		// Part 1 - Grep Table
		// @ToDo: Write a function that gets the Disambigs
	
		// Part 2 - Search Links
		// @ToDo: Write a function that greps the links which points to the disambig
		// and make sure, that this page is not a disambig too
	
		// Part 3 - Insert Into Page Table
		// @ToDo: Write the pages into the table
	}
	/** fillCatTable
	* @ToDo
	*/
	protected function fillCatTable () {
		// @ToDo Get the cats for each page and insert them
	}
	/** assertCatToDisambig
	* @ToDo
	*/
	protected function assertCatToDisambig () {
		//@ToDo Grep the disamigs, look to which pages they link, and save the categories for them
		// so that you can say: These disambigs links for example to chemic thinks, expert needed
	}
	/** assertCatToDisambig
	* Gibt alle Seiten in einer Kategorie aus
	* @Param die Kategorien, die analysiert wird
	*/
	protected function getCatMembers ($Kat) {
		$a=0;
		$b=0;
		$c=0;
		try {
			$result = $this->httpRequest('action=query&list=categorymembers&format=php&cmtitle=' . urlencode($Kat) . '&cmprop=ids&cmtype=page&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=', $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$answer = unserialize($result); 
		$Cont = false;
		if (isset ($answer ["query-continue"]["categorymembers"]["cmcontinue"]) === true) {
			$Continue = $answer ["query-continue"]["categorymembers"]["cmcontinue"];
			$Cont = true;
		}
		while ($Cont === true) {
			$a=0;
			if (isset ($answer["query"]['categorymembers'][$a]['pageid']) === true) {
				while (isset ($answer["query"]['categorymembers'][$a]['pageid']) === true) {
					$Site [$c] = $answer["query"]['categorymembers'][$a]['pageid'];	
					$c++;
					$a++;
				}
			} else  {}
			try {
				$result = $this->httpRequest('action=query&list=categorymembers&format=php&cmcontinue=' . $Continue 
				. '&cmtitle=' . urlencode($Kat) 
				. '&cmprop=ids&cmtype=page&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=', $this->job, 'GET');
			} catch (Exception $e) {
				throw $e;
			}
			$answer = unserialize($result); 
			if (isset ($answer ["query-continue"]["categorymembers"]["cmcontinue"]) === true) {
				$Continue = $answer ["query-continue"]["categorymembers"]["cmcontinue"];
				$Cont = true;
			} else
				$Cont = false;
		}
		$a=0;
		if (isset ($answer["query"]['categorymembers'][$a]['pageid']) === true) {
			while (isset ($answer["query"]['categorymembers'][$a]['pageid']) === true) {
				$Site [$c] = $answer["query"]['categorymembers'][$a]['pageid'];	
				$c++;
				$a++;
			}
		} else {}
		$b++;
		if (isset ($Site [0]) === false)
			return false;
		else
			return (serialize ($Site));
	}
}
$Bot = new BKLLC ('Luke081515Bot@dewiki', 'bkllc');
?>