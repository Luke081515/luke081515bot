#!/usr/bin/php
<?php
include 'BotCore.php';
/** AdTVerwaltung.php
* Verwaltet AdT Eintraege
* @Author Luke081515
* @Version 0.2
* @Status Alpha
*/
class AdtVerwaltung extends Core {
	/** AdTVerwaltung
	* Trigger wesentliche Funktionen, Konstruktur
	*/
	public function AdtVerwaltung ($Account, $Job, $pUseHTTPS = true)
	{
		$this->initcurl($Account, $Job, $pUseHTTPS = true);
		//$Today = date("m.d.y");  
		$Site = "Wikipedia_Diskussion:Hauptseite/Artikel_des_Tages/Vorschl&auml;ge";
		$a=2; //Fängt beim ersten Vorschlag an
		$b=0;
		$Avaible = true;
		while ($Avaible === true) { //Prüft wie viele Abschnitte verfügbar sind
			$Result = $this->readSection ($Site, $a);
			echo (" " . $a);
			if (strstr ($Result, "==") !== false) {
				$AdTs [$b] = $Result;
				$a++;
				$b++;
			} else
				$Avaible = false;
		}
		$a=0;
		while (isset ($AdTs [$a]) === true) {
			$AdTs [$a] = explode ("\n", $AdTs [$a]);
			$a++;
		}
		$a=0;
		$b=0;
		$a=0;
		$c=0;
		while (isset ($AdTs [$a] [0]) === true) {
			$Title = $AdTs [$a] [0];
			$Date = $AdTs [$a] [0];
			if (strstr ($Title, "== Ende") !== false) {}
			else {
				$Date = strstr ($Date, ":", true);
				$Date = str_replace ("== ", "", $Date);
				if (strstr ($Date, "Alternativvorschlag") !== false) {
					$Date = str_replace ("Alternativvorschlag ", "", $Date);
					$Title = str_replace ("Alternativvorschlag ", "", $Title);
					$Title = substr ($Title, 15);
					echo ("\nDatum (A):" . $Date);
				} else if (strstr ($Date, "<s>") !== false) {
					$Date = str_replace ("<s>", "", $Date);
					$Title = str_replace ("<s>", "", $Title);
					$Title = str_replace ("== ", "", $Title);
					$Title = str_replace ("</s> (gestrichen)", "", $Title);
					$Title = str_replace ("</s>", "", $Title);
					$Title = str_replace ("(gestrichen)", "", $Title);
					$Title = substr ($Title, 14);
					echo ("\nDatum (S):" . $Date);
				} else {
					echo ("\nDatum:" . $Date);
					$Title = substr ($Title, 15);
				}
				$Title = str_replace ("[", "", $Title);
				$Title = str_replace ("]", "", $Title);
				$Title = str_replace (" ==", "", $Title);
				$Title = str_replace ("==", "", $Title);
				echo ("\nLemma:" . $Title);
				echo ("\n-----\n");
				//$Results []
			}
			$a++;
		}
		echo ("\n");
	}
	/** SucheArtikel
	* Schaut nach, auf welcher der beiden Seiten der angegebene Artikel vorkommt
	* @Param: Name des zu suchenden Artiklels
	* @Return 0 falls nicht gefunden, 1 falls bei den Lesenswerten, 2 falls bei den exzellenten
	*/
	public function SucheArtikel ($Name) {
		$Site = $this->readPage ("Wikipedia:Hauptseite/Artikel_des_Tages/Verwaltung/Lesenswerte_Artikel");
		if (strstr ($Site, $Name) !== false)
			return 1;
		$Site = $this->readPage ("Wikipedia:Hauptseite/Artikel_des_Tages/Verwaltung");
		if (strstr ($Site, $Name) !== false)
			return 2;
		else
			return 0;
	}
	/** LeseDatumAus
	* @ToDo
	*/
	public function LeseDatumAus ($Name, $Site, $Date) {
		$Page = $this->readPage ($Site);
		if (strstr ($Page, $Date) !== false) {
			$Analyse = strstr ($Page, $Name);
			$Analyse = strstr ($Page, "-", false);
			//ToDo
		}
	}
	/** addDate
	* @ToDo
	*/
	public function addDate ($Name, $Site, $Date) {
		$Content = $this->readPage ($Site);
		if (strstr ($Content, "'''[[" . $Name . "]]'''") !== false) {
			$Content = str_replace ("'''[[" . $Name . "]]'''", "[[" . $Name . "]]", $Content);
			$FirstTime = true;
		} else {
			$FirstTime = false;
		}
		$ContentBefore = strstr ($Content, "[[" . $Name . "]]", true);
		$ContentAfter = strstr ($Content, "[[" . $Name . "]]", false);
		$ContentAfter = strstr ($ContentAfter, "-", false);
		$ContentToChange = strstr ($ContentAfter, "-", true);
		if ($FirstTime === true) {
			$ContentToChange = str_replace ("[[" . $Name . "]]", "[[" . $Name . "]] <small>''" . $Date . "''</small>", $ContentToChange);
		} else {
			$ContentToChange = strstr ($ContentAfter, "</small>", true);
			$ContentToChange = $ContentToChange . " ''" . $Date . "''</small>";
		}
		$Content = $ContentBefore . $ContentToChange . $ContentAfter;
		$this->editPage ($Site, $Content, "Bot: Artikel wurde f&uuml; AdT nominiert: [[" . $Name . "]]");
	}
	/** kursivEntfernen
	* @ToDo
	*/
	public function kursivEntfernen ($Name) {}
	/** removeDate
	* @ToDo
	*/
	public function removeDate ($Name, $Site, $Date) {}
}
$Bot = new AdtVerwaltung ('Luke081515Bot@dewiki', 'AdT');
?>