#!/usr/bin/php
<?php
include './BotCore.php';
/** Queue.php
* Stellt Tasks in der Warteschlange bereit
* @Author Luke081515
* Dokumentation siehe Submodule
*/
class QueueBot extends Core {
	protected $account;
	protected $inwork;
	protected $auftraege;
	public function QueueBot ($Account, $Job, $pUseHTTPS = true) {
		$this->initcurl($Account, $Job, $pUseHTTPS = true);
		echo ("\n" . time () . "--" . "Programm gestartet");
		$Set = true;
		while ($Set === true) {
			$this->mainEngine ();
			sleep (5);
		}
		echo ("\n" . time () . "--" . "Programm beendet");
	}
	/** mainEngine
	* Ueberprueft die Warteschlange, triggert Submodule
	*/
	public function mainEngine () {
		$inwork = false;
		if ($this->readPageJs("Benutzer:Luke081515/Bot.js") === "true") {
			$AllTasks = $this->readPage("Benutzer:Luke081515Bot/Warteschlange/Auftraege");
			$Border = "}}";
			$Tasks = explode ($Border, $AllTasks);
			$i = 0;
			while (isset($Tasks[$i]) === true) {
				$i++;
			}
			$Border = "|";
			$i--;
			for ($j = 0; $j < $i; $j++) {
				$Tasks[$j] = explode ($Border, $Tasks[$j]);
				$Task[$j] = substr ($Tasks[$j][1], 5);
				$User[$j] = substr ($Tasks[$j][2], 5);
				$StartLemma[$j] = substr ($Tasks[$j][3], 11);
				$TargetLemma[$j] = substr ($Tasks[$j][4], 12);
				$Summary[$j] = substr ($Tasks[$j][5], 8);
				$Status[$j] = substr ($Tasks[$j][6], 7);
				if (isset ($Tasks[$j][7]) === true)
					$Zeitstempel[$j] = substr ($Tasks[$j][7], 12);
				else 
					$Zeitstempel[$j] = "";
			}
			if ($inwork === false) {
				for ($j = 0; $j < $i; $j++) {
					if ($Status[$j] === "s") {
						$TaskNumber = $j;
						$j = $i;
						$NumberOfTasks = $i;
						$inwork = true;
						$ToWriteDown = "";
						echo ("\n" . time () . "--" . "Beginne Bearbeitung, Auftrag eingereit");
					}
					if ($inwork === true) {
						if ($this-> ControlIgnoreList ($User[$TaskNumber]) === true) {
							$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "u", "Bot: Auftrag abgelehnt, Benutzer steht auf der Ignorierliste", $Zeitstempel);
							echo ("\n" . time () . "--" . "Ignorierter Benutzer");
							$this->NotificateOnTalkPage ($User[$TaskNumber], 4, "");
						} else {
							$CheckUser = $this->CheckUser ($User[$TaskNumber]);
							if ($CheckUser === 3) {
								$CheckUser = 0;/* Bisher deaktiviert.
								if ($Task[$TaskNumber] !== "sr")
									$CheckUser = 0; // Ausnahme gilt nur für die sr Funktion der Queue*/
							}
							/** Job "Suppressredirect"
							* Verschiebt eine Seite ohne Weiterleitung
							* @Version 2.0
							* @Author Luke081515
							* @Status stable
							*/
							if ($Task[$TaskNumber] === "sr") {
								$Was = 0;
								$Possible1 = substr ($StartLemma[$TaskNumber], 0, 9);
								$Possible2 = substr ($StartLemma[$TaskNumber], 0, 11);
								$Possible3 = substr ($StartLemma[$TaskNumber], 0, 5);
								$Possible4 = strtolower ($Possible1);
								$Possible5 = strtolower ($Possible2);
								$Possible6 = strtolower ($Possible3);
								if ($Possible4  === "benutzer:") 
									$Was = 1;
								if ($Possible5  === "benutzerin:") 
									$Was = 2;
								if ($Possible6  === "user:")
									$Was = 3;
								if ($Was != 0) {
									$NextBorder = "/";
									$EndResult = "";
									if ($Was === 1)
										$EndResult = substr ($StartLemma[$TaskNumber], 9);
									if ($Was === 2)
										$EndResult = substr ($StartLemma[$TaskNumber], 11);
									if ($Was === 3)
										$EndResult = substr ($StartLemma[$TaskNumber], 5);
									$ToCheck = explode ($NextBorder, $EndResult);	
									if ($CheckUser === 3) {// Ausnahmefall siehe T249
										$ToAnalyse = $this->readPage($StartLemma[$TaskNumber]);
										if (preg_match("{griech|Α|α|Ά|ά|Β|β|Γ|γ|Δ|δ|Ε|ε|Έ|έ|Ζ|ζ|Η|η|Ή|ή|Θ|θ|Ι|ι|Ί|ί|Ϊ|ϊ|ΐ|Κ|κ|Λ|λ|Μ|μ|Ν|ν|Ξ|ξ|Ο|ο|Ό|ό|Π|π|Ρ|ρ|Σ|σ|ς|Τ|τ|Υ|υ|Ϋ|ϋ|Ύ|ύ|ΰ|Φ|φ|Χ|χ|Ψ|ψ|Ω|ω|Ώ|ώ|ἀ|ἁ|ὰ|ᾶ|ἂ|ἃ|ἄ|ἅ|ἆ|ἇ|ᾳ|ᾀ|ᾁ|ᾴ|ᾲ|ᾷ|ᾄ|ᾅ|ᾂ|ᾃ|ᾆ|ᾇ|ἐ|ἑ|ὲ|ἔ|ἕ|ἒ|ἓ|ἠ|ἡ|ὴ|ῆ|ἤ|ἢ|ἣ|ἥ|ἦ|ἧ|ῃ|ῄ|ῂ|ῇ|ᾐ|ᾑ|ᾔ|ᾒ|ᾕ|ᾓ|ᾖ|ᾗ|ἰ|ἱ|ὶ|ῖ|ἴ|ἲ|ἵ|ἳ|ἶ|ἷ|ὸ|ὀ|ὁ|ὄ|ὅ|ὂ|ὃ|ῤ|ῥ|ὐ|ὑ|ὺ|ῦ|ὔ|ὕ|ὒ|ὓ|ὖ|ὗ|ὠ|ὡ|ὼ|ῶ|ὤ|ὢ|ὥ|ὣ|ὦ|ὧ|ῳ|ῴ|ῲ|ῷ|ᾠ|ᾡ|ᾤ|ᾢ|ᾥ|ᾣ|ᾦ|ᾧ|`|᾿|῾|῍|῎|῏|῟|῞|῝|῍|῎|Ϝ|ϝ|Ϙ|ϙ|Ϡ|ϡ}", $ToAnalyse) === 0) {
											$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "p", "Bot: Auftrag abgelehnt, diese Verschiebung wird nicht zugelassen", $Zeitstempel);
											echo ("\n" . time () . "--" . "Auftrag abgelehnt");
											$this->NotificateOnTalkPage ($User[$TaskNumber], 5, "");
										} else
											$CheckUser = 0; // Falls nicht gefunden, laufe normal weiter
									}
									if ($CheckUser === 0) {
										$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "p", "Bot: Auftrag abgelehnt, die Startseite im BNR entsprach nicht dem Antragstellenden Benutzer", $Zeitstempel);
										echo ("\n" . time () . "--" . "Auftrag abgelehnt");
										$this->NotificateOnTalkPage ($User[$TaskNumber], 5, "");
									} else if ($ToCheck[0] != $User[$TaskNumber]) {
										$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "p", "Bot: Auftrag abgelehnt, die Startseite im BNR entsprach nicht dem Antragstellenden Benutzer", $Zeitstempel);
										echo ("\n" . time () . "--" . "Auftrag abgelehnt");
										$this->NotificateOnTalkPage ($User[$TaskNumber], 5, "");
									} else if (strstr ($StartLemma[$TaskNumber], "/") === false) {
										$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "n", "Bot: Auftrag abgelehnt, die zu verschiebende Seite ist keine Unterseite", $Zeitstempel);
										echo ("\n" . time () . "--" . "Auftrag abgelehnt");
										$this->NotificateOnTalkPage ($User[$TaskNumber], 3, "");
									} else if (strstr ($TargetLemma[$TaskNumber], ":")  === false) {
										if ($this->MovePageProtection ($EndResult) === true) {
											echo ("\n" . time () . "--" . "Verschiebe Seite");
											$answer = $this->MovePage ($StartLemma[$TaskNumber], $TargetLemma[$TaskNumber], "Bot: Verschiebung im Auftrag von [[User:" . $User[$TaskNumber] . "|" . $User[$TaskNumber] ."]]: " . $Summary[$TaskNumber], $Zeitstempel);
											echo ("\n" . $answer);
											if (strstr ($answer, "error") === false) {
												echo ("\n" . time () . "--" . "Seite verschoben");
												$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "e", "Bot: Auftrag bearbeitet", $Zeitstempel);
												$this->NotificateOnTalkPage ($User[$TaskNumber], 1, $TargetLemma[$TaskNumber]);
												$this->WriteLogError ("", "", 5, $StartLemma[$TaskNumber], $TargetLemma[$TaskNumber], $Summary[$TaskNumber]);
												$this->doService( $TargetLemma[$TaskNumber] );
											} else {
												echo ("\n" . time () . "--" . "Seite konnte nicht verschoben werden");
												$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "f", "Bot: Fehler bei der Verschiebung", $Zeitstempel);
												$this->NotificateOnTalkPage ($User[$TaskNumber], 2, "");
											}
											$inwork = false;
											echo ("\n" . time () . "--" . "Auftrag bearbeitet");
										} else {
											$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "f", "Bot: Auftrag abgelehnt, die dazugehörige Diskussionsseite ist vollgeschützt", $Zeitstempel);
											$this->NotificateOnTalkPage ($User[$TaskNumber], 2, "");
											echo ("\n" . time () . "--" . "Auftrag abgelehnt");
										}
									} else if ($CheckUser === 3) {} else {
										$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "n", "Bot: Auftrag abgelehnt, die Zielseite ist nicht im ANR", $Zeitstempel);
										$this->NotificateOnTalkPage ($User[$TaskNumber], 3, "");
										echo ("\n" . time () . "--" . "Auftrag abgelehnt");
									}
								} else {
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "n", "Bot: Auftrag abgelehnt, der Startnamensraum war invalid", $Zeitstempel);
									$this->NotificateOnTalkPage ($User[$TaskNumber], 3, "");
									echo ("\n" . time () . "--" . "Auftrag abgelehnt");
								}	
							}
							/** Job "Massmessage"
							* Verteilt eine Massennachricht
							* @Version 1.0
							* @Author Luke081515
							* @Status stable
							*/
							else if ($Task[$TaskNumber] === "mm") {
								$User[$TaskNumber] = str_replace ("_", " ", $User[$TaskNumber]);
								if ($CheckUser === 2) {
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "a", "Bot: Bearbeite Auftrag", $Zeitstempel);
									$Replaced = str_replace ("_", " ", $StartLemma[$TaskNumber]);
									$Possible1 = substr ($Replaced, 0, 9);
									$Possible2 = substr ($Replaced, 0, 11);
									$Possible3 = substr ($Replaced, 0, 5);
									$Possible7 = substr ($Replaced, 0, 10);
									$Possible9 = substr ($Replaced, 0, 7);
									$Possible4 = strtolower ($Possible1);
									$Possible5 = strtolower ($Possible2);
									$Possible6 = strtolower ($Possible3);
									$Possible8 = strtolower ($Possible7);
									$Possible10 = strtolower ($Possible9);
									if ($Possible4  === "benutzer:") 
										$Was = 1;
									if ($Possible5  === "benutzerin:") 
										$Was = 2;
									if ($Possible6  === "user:")
										$Was = 3;
									if ($Possible8  === "wikipedia:")
										$Was = 4;
									if ($Possible10  === "portal:")
										$Was = 5;
									if ($Was === 1) {
										$EndResult = substr ($Replaced, 9);
										$data = "action=query&prop=links&format=xml&pllimit=5000&pldir=ascending&plnamespace=3&rawcontinue=&titles=Benutzer:" . urlencode($EndResult);
									} else if ($Was === 2) {
										$EndResult = substr ($Replaced, 11);
										$data = "action=query&prop=links&format=xml&pllimit=5000&pldir=ascending&plnamespace=3&rawcontinue=&titles=Benutzer:" . urlencode($EndResult);
									} else if ($Was === 3) {
										$EndResult = substr ($SReplaced, 5);
										$data = "action=query&prop=links&format=xml&pllimit=5000&pldir=ascending&plnamespace=3&rawcontinue=&titles=Benutzer:" . urlencode($EndResult);
									} else if ($Was === 4) {
										$EndResult = substr ($Replaced, 10);
										$data = "action=query&prop=links&format=xml&pllimit=5000&pldir=ascending&plnamespace=3&rawcontinue=&titles=Wikipedia:" . urlencode($EndResult);
									} else if ($Was === 5) {
										$EndResult = substr ($Replaced, 7);
										$data = "action=query&prop=links&format=xml&pllimit=5000&pldir=ascending&plnamespace=3&rawcontinue=&titles=Portal:" . urlencode($EndResult);
									}
									try {
										$website = $this->httpRequest($data, $this->job, 'GET');
									} catch (Exception $e) {
										throw $e;
									}
									$Answer = explode ("\"", $website);
									$b=13;
									$q=0;
									while (isset($Answer [$b]) === true) {
										$Result [$q] = $Answer [$b];
										$b = $b + 4;
										$q++;
									}
									$q=0;
									$f=0;
									$g=0;
									while (isset($Result [$q]) === true) {
										echo ("\nNachricht wird verteilt an: " . $Result [$q]); 
										$answer = $this->editPage ($Result [$q], $this->readPage($Result [$q]) . "\n{{subst:" . $TargetLemma[$TaskNumber] . "}}", "Bot: Nachricht von [[User:" . $User[$TaskNumber] . "|" . $User[$TaskNumber] ."]]: " . $Summary[$TaskNumber]); 
										if (strstr ($answer, "error") !== false) {
											$Error [$f] = $Result [$q];
											$f++;
										} else {
											$Success [$g] = $Result [$q];
											$g++;
										}
										$q++;
										sleep (4);
									}
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "e", "Bot: Auftrag bearbeitet", $Zeitstempel);
									$this->NotificateOnTalkPage ($User[$TaskNumber], 6, "");
									if (isset ($Error [0]) === false)
										$Error = "";
									$this->WriteLogError ($Success, $Error, 6, $StartLemma [$TaskNumber], $TargetLemma [$TaskNumber], $Summary [$TaskNumber]);
								} else if ($CheckUser === 1) {										
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "d", "Bot: Warte auf Best&auml;tigung des Betreibers", $Zeitstempel);
									echo ("\n" . time () . "--" . "Auftrag auf Pause");
								} else if ($CheckUser === 0) {										
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "p", "Bot: Auftrag abgelehnt, du hast einen falschen Namen angegeben", $Zeitstempel);
									echo ("\n" . time () . "--" . "Auftrag abgelehnt");
									$this->NotificateOnTalkPage ($User[$TaskNumber], 5, "");
								} else {}
							}
							/** Job "CategorySearcher"
							* Erstellt eine Tabelle mit allen Seiten einer Kategorie und deren Kategorien
							* @Version 1.0
							* @Author Luke081515
							* @Status stable
							*/
							else if ($Task[$TaskNumber] === "cs") {
								$Was = 0;
								$Possible1 = substr ($StartLemma[$TaskNumber], 0, 9);
								$Possible2 = substr ($StartLemma[$TaskNumber], 0, 11);
								$Possible3 = substr ($StartLemma[$TaskNumber], 0, 5);
								$Possible4 = strtolower ($Possible1);
								$Possible5 = strtolower ($Possible2);
								$Possible6 = strtolower ($Possible3);
								if ($Possible4  === "benutzer:") 
									$Was = 1;
								if ($Possible5  === "benutzerin:") 
									$Was = 2;
								if ($Possible6  === "user:")
									$Was = 3;
								if ($Was != 0) {
									$NextBorder = "/";
									$EndResult = "";
									if ($Was === 1)
										$EndResult = substr ($StartLemma[$TaskNumber], 9);
									if ($Was === 2)
										$EndResult = substr ($StartLemma[$TaskNumber], 11);
									if ($Was === 3)
										$EndResult = substr ($StartLemma[$TaskNumber], 5);
									
									$ToCheck = explode ($NextBorder, $EndResult);	
									if ($CheckUser === 0) {
										$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "p", "Bot: Auftrag abgelehnt, die Seite im BNR entsprach nicht dem Antragstellenden Benutzer", $Zeitstempel);
										echo ("\n" . time () . "--" . "Auftrag abgelehnt");
										$this->NotificateOnTalkPage ($User[$TaskNumber], 5, "");
									} else if ($ToCheck[0] != $User[$TaskNumber]) {
										$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "p", "Bot: Auftrag abgelehnt, die Seite im BNR entsprach nicht dem Antragstellenden Benutzer", $Zeitstempel);
										echo ("\n" . time () . "--" . "Auftrag abgelehnt");
										$this->NotificateOnTalkPage ($User[$TaskNumber], 5, "");
									} else if (strstr ($StartLemma[$TaskNumber], "/") === false) {
										$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "n", "Bot: Auftrag abgelehnt, die zu Seite ist keine Unterseite", $Zeitstempel);
										echo ("\n" . time () . "--" . "Auftrag abgelehnt");
										$this->NotificateOnTalkPage ($User[$TaskNumber], 3, "");
									} else if (strstr ($TargetLemma[$TaskNumber], "Kategorie")  !== false) {
										$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "a", "Bot: Bearbeite Auftrag", $Zeitstempel);
										$SitesS = $this->getCatMembers ($TargetLemma[$TaskNumber]);
										$Sites =  unserialize ($SitesS);
										$Write = "{| class=\"wikitable sortable\"\n! Titel !! class=\"unsortable\"| Kategorie(n)\n|-";
										$a=0;
										if ($Summary[$TaskNumber] > 5000)
												$Summary[$TaskNumber] = 5000;
										while (isset ($Sites [$a]) === true && $a < $Summary[$TaskNumber]) {
											$b=0;
											$CatList = $this->GetPageCats ($Sites [$a]);
											$Cats = explode ($Border, $CatList);
											$Write = $Write . "\n|[[" . $Sites [$a] . "]]";
											$Write = $Write . "\n|";
											while (isset ($Cats [$b]) == true)
											{
												$Write = $Write . "\n* [[:" . $Cats [$b] . "]]";
												$b++;
											}
											$Write = $Write . "\n|-";
											$a++;
										}
										$Listed = $a;
										while (isset ($Sites [$a]) === true)
											$a++;
										$All = $a;
										$Write = $Write . "\n|}";
										$Write = $Write . "\n\nErstellt durch ~~~ um ~~~~~\n\n{{ping|" . $User [$TaskNumber] . "}} Auftrag bearbeitet. ~~~~";
										$this->editPage($StartLemma[$TaskNumber], $Write, "Bot: Erstelle Liste auf Anfrage des Benutzers");
										$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "e", "Bot: Auftrag bearbeitet", $Zeitstempel);
										$this->WriteLogError ($All, $Listed, 7, $StartLemma [$TaskNumber], $TargetLemma [$TaskNumber], $Summary [$TaskNumber]);
									} else {
										$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "n", "Bot: Auftrag abgelehnt, die angebene Seite ist keine Kategorie", $Zeitstempel);
										$this->NotificateOnTalkPage ($User[$TaskNumber], 3, "");
										echo ("\n" . time () . "--" . "Auftrag abgelehnt");
									}	
								} else {
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "n", "Bot: Fehler", $Zeitstempel);
									echo ("\n" . time () . "--" . "Auftrag abgelehnt");
								}
							}
							/** Job "TemplateOperator:Umbennen"
							* Benennt alle Einbindungen einer Vorlage um
							* @Version 0.8
							* @Author Luke081515
							* @Status Alpha
							*/
							else if ($Task[$TaskNumber] === "tr") {
								if ($CheckUser === 2) {
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "a", "Bot: Bearbeite Auftrag", $Zeitstempel);
									$EmbeddingsS = $this->getAllEmbedings ($StartLemma[$TaskNumber]);
									$Embeddings = unserialize ($EmbeddingsS);
									$a=0;
									$b=0;
									$c=0;
									while (isset ($Embeddings [$a]) === true) {
										$Content = $this->readPage ($Embeddings [$a]);
										$NameI = substr ($StartLemma [$TaskNumber], 8);
										$NameII = substr ($TargetLemma [$TaskNumber], 8);
										$Content = str_replace ("{{" . $NameI . "}}", "{{" . $NameII . "}}" , $Content);	
										$Content = str_replace ("{{" . $NameI, "{{" . $NameII, $Content);		
										$answer = $this->editPage ($Embeddings [$a], $Content, "Bot: Umbenennung einer Vorlage aufgrund [[" . $Summary [$TaskNumber] . "|dieser Diskussion]]");
										if (strstr ($answer, "error") === false) {
											$Succesful [$b] = $Embeddings [$a];
											$b++;
										} else {
											$Error [$c] = $Embeddings [$a];
											$c++;
										}
										$a++;
									}
									if (isset ($Error [0]) === true)
										$this->WriteLogError ($Succesful, $Error, 1, $StartLemma[$TaskNumber], $TargetLemma[$TaskNumber], $Summary [$TaskNumber]);
									else
										$this->WriteLogError ($Succesful, "", 1, $StartLemma[$TaskNumber], $TargetLemma[$TaskNumber], $Summary [$TaskNumber]);
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "e", "Bot: Auftrag bearbeitet", $Zeitstempel);
								} else if ($CheckUser === 1) {										
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "d", "Bot: Warte auf Best&auml;tigung des Betreibers", $Zeitstempel);
									echo ("\n" . time () . "--" . "Auftrag auf Pause");
								} else if ($CheckUser === 0) {										
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "p", "Bot: Auftrag abgelehnt, du hast einen falschen Namen angegeben", $Zeitstempel);
									echo ("\n" . time () . "--" . "Auftrag abgelehnt");
									$this->NotificateOnTalkPage ($User[$TaskNumber], 5, "");
								} else {}
							}
							/** Job "TemplateOperator:Substituieren"
							* Substituiert alle Einbindungen einer Vorlage
							* @Version 0.8
							* @Author Luke081515
							* @Status Alpha
							*/
							else if ($Task[$TaskNumber] === "ts") {
								if ($CheckUser === 2) {
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "a", "Bot: Bearbeite Auftrag", $Zeitstempel);
									$EmbeddingsS = $this->getAllEmbedings ($StartLemma[$TaskNumber]);
									$Embeddings = unserialize ($EmbeddingsS);
									$a=0;
									$b=0;
									$c=0;
									while (isset ($Embeddings [$a]) === true) {
										$Content = $this->readPage ($Embeddings [$a]);
										$Name = substr ($StartLemma [$TaskNumber], 8);
										$Content = str_replace ("{{" . $Name . "}}", "{{ers:" . $Name . "}}" , $Content);	
										$Content = str_replace ("{{" . $Name, "{{ers:" . $Name, $Content);		
										$answer = $this->editPage ($Embeddings [$a], $Content, "Bot: Substituierung einer Vorlage aufgrund [[" . $Summary [$TaskNumber] . "|dieser Diskussion]]");
										if (strstr ($answer, "error") === false) {
											$Succesful [$b] = $Embeddings [$a];
											$b++;
										} else {
											$Error [$c] = $Embeddings [$a];
											$c++;
										}
										$a++;
									}
									$this->WriteLogError ($Succesful, $Error, 3, $StartLemma[$TaskNumber], "", $Summary [$TaskNumber]);
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "e", "Bot: Auftrag bearbeitet", $Zeitstempel);
								} else if ($CheckUser === 1) {										
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "d", "Bot: Warte auf Best&auml;tigung des Betreibers", $Zeitstempel);
									echo ("\n" . time () . "--" . "Auftrag auf Pause");
								} else if ($CheckUser === 0) {										
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "p", "Bot: Auftrag abgelehnt, du hast einen falschen Namen angegeben", $Zeitstempel);
									echo ("\n" . time () . "--" . "Auftrag abgelehnt");
									$this->NotificateOnTalkPage ($User[$TaskNumber], 5, "");
								} else {}
							}
							/** Job "CatOvervoew"
							* Erstellt einen Uebersicht ueber einen Teil des Kategoriebaumes
							* @Version 0.3
							* @Author Luke081515
							* @Status Alpha
							* Gebe eine Kategorie an (Oberkat)
							* Gebe eine Kat an, deren Seiten gelistet werden
							* Lese alle Seiten aus der zweiten Kat aus
							* Sortiere die Seiten nach Kategorien der Unterkategorien der Oberkategorie
							*/
							else if ($Task[$TaskNumber] === "co") {
								$Kats = unserialize ($this->getCatMembers ($StartLemma [$TaskNumber], true));
								$a=0;
								while (isset ($Kat [$a]) === true) {
									$PageList = $this->getPages ($Kats [$a]);
									$a++;
								}
								$a=0;
								while (isset ($Kat [$a]) === true) {
									$ResultPages = $this->PageInCat ($TargetLemma [$TaskNumber], $PageList);
									$a++;
								}
								$a=0;
								$output = ("== Liste der Seiten der Kategorie" . $TargetLemma [$TaskNumber] . "in der Kategorie" . $StartLemma [$TaskNumber] . " ==");
								while (isset ($Kat [$a]) === true) {
									$output = $output . "\n * [[:" . $Kat [$a] . "]]";
									$b=0;
									while (isset ($ResultPages [$b]) === true) {
										$output = $output . "\n ** [[:" . $ResultPages [$b] . "]]";
										$b++;
									}
									$a++;
									$output = $output . "\n\nErstellt durch ~~~ um ~~~~~\n\n\n{{ping|" . $User [$TaskNumber] . "}} Liste erstellt. Viele Gr&uuml;ße, ~~~~";
								}
								$this->editPage ($Summary [$TaskNumber], $output, "Bot: Erstelle Liste auf Anfrage", 0);
							}
							else 
							{}
						}
					}
				}
			}
		}
		else
		{
			echo ("\n" . time () . "--" . "Bot gesperrt!");
		}
	}
	/** PageInCat
	* @Param: Eine Kategorie, eine Seitenliste
	* Aus der Kategorie werden alle Unterkategorien ausgelesen
	* Die Kategorien der Seiten in der Seiteliste der ausgelesen
	* Seiten aus der Seiteliste die in der Oberkategorie oder in ihren Subkategorien sind kommen ins Array
	* Das Array wird zurueckgegeben
	*/
	protected function PageInCat ($Kat, $Pages) {
		$a=0;
		$d=0;
		$Subcats = $this->getCatMembers ($Kat, true);
		while (isset ($Pages [$a]) === true) {
			$found = false;
			$b=0;
			$PageKats = $this->getPageCats($Pages [$a]);
			while (isset ($PageKats [$b]) === true && $found === false) {
				$c=0;
				while (isset ($Subcats [$c]) === true && $found === false) {
					if ($PageKats [$b] === $Subcats [$c]) {
						$Result [$d] = $Pages [$a];
						$d++;
						$found = true;
					}
					$c++;
				}
				$b++;
			}
			$a++;
		}
		return $Result;
	}
	/** ChangeStatus
	* Aenderung des Status eines Tasks in der Warteschlange
	*/
	protected function ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, $NewStatus, $Reason, $Zeitstempel) {
		$ToWriteDown = "";
		if ($NewStatus === 'e')
			$ToWriteDown= $ToWriteDown . "{{Benutzer:Luke081515Bot/Warteschlange/Job|TASK=" . $Task[$TaskNumber] . "|USER=" . $User[$TaskNumber] . "|StartLemma=" . $StartLemma[$TaskNumber] . "|TargetLemma=" . $TargetLemma[$TaskNumber] . "|Summary=" . $Summary[$TaskNumber] . "|STATUS=" . $NewStatus . "|Zeitstempel=~~~~~}}"; 
		else
			$ToWriteDown= $ToWriteDown . "{{Benutzer:Luke081515Bot/Warteschlange/Job|TASK=" . $Task[$TaskNumber] . "|USER=" . $User[$TaskNumber] . "|StartLemma=" . $StartLemma[$TaskNumber] . "|TargetLemma=" . $TargetLemma[$TaskNumber] . "|Summary=" . $Summary[$TaskNumber] . "|STATUS=" . $NewStatus . "|Zeitstempel=}}"; 	
		for ($a=0; $a<$NumberOfTasks; $a++) {
			if ($a===$TaskNumber)
			{}
			else {
				$ToWriteDown= $ToWriteDown . "\n{{Benutzer:Luke081515Bot/Warteschlange/Job|TASK=" . $Task[$a] . "|USER=" . $User[$a] . "|StartLemma=" . $StartLemma[$a] . "|TargetLemma=" . $TargetLemma[$a] . "|Summary=" . $Summary[$a] . "|STATUS=" . $Status[$a] . "|Zeitstempel=" . $Zeitstempel[$a] ."}}";
			}
		}
		$this->editPage("Benutzer:Luke081515Bot/Warteschlange/Auftraege", $ToWriteDown, $Reason);
	}
	
	/** getPages
	* liest alle einkategorisierten Seiten einer Kat aus
	* @autor: Luke081515
	* @version: 1.0
	*/
	protected function getPages ($Kat) {
		$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Kat) . "&cmprop=title&cmtype=page&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$a=9;
		$Answer = explode ("\"", $website);
		while (isset ($Answer [$a]) === true) {
			$Exception = false;
			$x = $a + 1;
			while (strstr ($Answer [$x], ";}") === false) {
				if ($Exception === false)
					$New = $Answer [$a] . "\"" . $Answer [$x];
				else
					$New = $New . "\"" . $Answer [$x];
				$Exception = true;
				$a++;
				$x = $a + 1;
			}
			if ($Exception === true)
				$Page [$c] = $New;
			else
				$Page [$c] = $Answer [$a];
			$a = $a + 6;
			$c++;
		}
		$b++;
		$b=0;
		$d=0;
		while (isset ($Page [$b]) === true) {
			$c=0;
			$Found = false;
			while (isset ($PageResults [$c]) === true) {
				if ($Page [$b] === $PageResults [$c])
					$Found = true;
				$c++;
			}
			if ($Found === false) {
				$PageResults [$d] = $Page [$b];
				$d++;
			}
			$b++;
		}
		return serialize ($PageResults);
	}
	
	protected function ControlIgnoreList ($TargetUser) {
		$AllIgnoredUsers = $this->readPage("Benutzer:Luke081515Bot/ignore.js");	
		$Border = "|";
		$IgnoredUsers = explode ($Border, $AllIgnoredUsers);
		$q = 0;
		while (isset($IgnoredUsers[$q]) === true)
			$q++;
		for ($x = 0; $x < $q; $x++) {
			if ($TargetUser === $IgnoredUsers[$x])
				return true;
		}
		return false;
	}
	protected function MovePageProtection ($Page)
	{
		$title = "Benutzer Diskussion:" . $Page;
		$data = "action=query&format=php&prop=info&titles=" . urlencode($title);
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$answer = unserialize($website);
		$data = "action=query&prop=info&format=php&inprop=protection&titles=" . urlencode($title);
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$answer = unserialize($website);
		// do nothing with the answer... ;)
    	echo ("\n". $website);
		if (strstr ($website, "sysop") === false)
			return true;
		return false;
	}
	public function CheckUser ($CheckUser)
	{
		$title = "Benutzer:Luke081515Bot/Warteschlange/Auftraege";
		$data = "action=query&format=php&prop=info&titles=" . urlencode($title);
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$answer = unserialize($website);
		$data = "action=query&prop=revisions&format=php&rvprop=user&titles=Benutzer%3ALuke081515Bot%2FWarteschlange%2FAuftraege";
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$answer = unserialize($website);
		$position = strpos ($website , "user"); 
		$Result = substr ($website, $position);
    	echo ("\n". $website);
		echo ("\n". $Result);
		if (strstr ($Result, "Luke081515\"") !== false) {
			return 2;
		} else if (strstr ($Result, $CheckUser) !== false) {
			return 1;
		} /*else if (strstr ($Result, "Shi Annan\"") !== false) // Ausnahme per T249 {
			return 3;
		}*/ else  {
			return 1;
		}
	}
	public function GetPageCats ($Site)
	{
		echo ("\n Lese: " . $Site);
		$data = "action=query&prop=categories&format=php&cllimit=500&cldir=ascending&rawcontinue=&titles=" . urlencode($Site);
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Result = explode ("\"", $website);
		$a=19;
		$b=0;
		while (isset ($Result [$a]) === true) {
			$Kats [$b] = $Result [$a];
			$a = $a +  6;
			$b++;
		}
		$b=1;
		$Ret = $Kats [0];
		while (isset ($Kats [$b]) === true) {
			$Ret = $Ret . "|" . $Kats [$b];
			$b++;
		}
		echo ("\n" . $Ret);
		return $Ret;
	}
	protected function ControlRobinsonList ($TargetUser)
	{
		$AllRobinsonUsers = $this->readPage("Benutzer:Luke081515Bot/Opt-Out");	
		$Border = "|";
		$RobinsonUsers = explode ($Border, $AllRobinsonUsers);
		$q = 0;
		while (isset($RobinsonUsers[$q]) === true)
			$q++;
		for ($x = 0; $x < $q; $x++) {
			if ($TargetUser === $RobinsonUsers[$x])
				return true;
		}
		return false;
	}
	protected function ControlOnlyErrorList ($TargetUser)
	{
		$AllOnlyErrorUsers = $this->readPage("Benutzer:Luke081515Bot/Only-Error");	
		$Border = "|";
		$OnlyErrorUsers = explode ($Border, $AllOnlyErrorUsers);
		$q = 0;
		while (isset($OnlyErrorUsers[$q]) === true)
			$q++;
		for ($x = 0; $x < $q; $x++) {
			if ($TargetUser === $OnlyErrorUsers[$x])
				return true;
		}
		return false;
	}
	protected function NotificateOnTalkPage ($User, $MessageNumber, $Sitename) {
		if ($MessageNumber === 5)
			$this->editPage("Benutzer Diskussion:Luke081515", "\n" . $this->readPage("Benutzer Diskussion:Luke081515"), "Bot: Benachrichtigung");
		else if ($this->ControlRobinsonList ($User) === true)
		{}
		else if ($this->ControlOnlyErrorList ($User) === true) {
			if ($MessageNumber === 2)
				$Message = "\n{{subst:Benutzer:Luke081515Bot/Nachricht/Fehlerhaft}}";
			if ($MessageNumber === 3)
				$Message = "\n{{subst:Benutzer:Luke081515Bot/Nachricht/Abgelehnt}}";
			if ($MessageNumber === 4)
				$Message =	"\n{{subst:Benutzer:Luke081515Bot/Nachricht/Verweigert}}";
		} else {
			if ($MessageNumber === 1)
				$Message = "\n{{subst:Benutzer:Luke081515Bot/Nachricht/Erledigt|" . $Sitename . "}}";
			if ($MessageNumber === 2)
				$Message = "\n{{subst:Benutzer:Luke081515Bot/Nachricht/Fehlerhaft}}";
			if ($MessageNumber === 3)
				$Message = "\n{{subst:Benutzer:Luke081515Bot/Nachricht/Abgelehnt}}";
			if ($MessageNumber === 4)
				$Message =	"\n{{subst:Benutzer:Luke081515Bot/Nachricht/Verweigert}}";
			if ($MessageNumber === 6)
				$Message =	"\n{{subst:Benutzer:Luke081515Bot/Nachricht/Verteilt}}";
			$this->editPage("Benutzer Diskussion:" . $User, $this->readPage("Benutzer Diskussion:" . $User) . $Message, "Bot:Benachrichtigung aufgrund eines beendeten Auftrages");
		}
	}
	public function getAllEmbedings ($Templ) {
		$b=0;
		$data = "action=query&list=embeddedin&format=php&eititle=" . urlencode($Templ) . "&einamespace=0&eidir=ascending&eilimit=5000&rawcontinue=";
		echo ("\n" . $data);
		sleep (5);
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		echo ("\n" . $website);
		unset ($Answer);
		$Answer = explode ("\"", $website);
		while (strstr ($website, "eicontinue") !== false) {
			$a=19;
			$Continue = $Answer [7];
			while (isset ($Answer [$a]) === true) {
				$x = $a + 1;
				$Exception = false;
				while (strstr ($Answer [$x], ";}") === false) {
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$a = $a +  8;
				$b++;
			} 
			unset ($Answer);
			$data = "action=query&list=embeddedin&format=php&eititle=" . urlencode($Templ [$z]) . "&einamespace=0&eicontinue=" . urlencode($Continue) .  "&eidir=ascending&eifilterredir=nonredirects&eilimit=5000&rawcontinue=";
			try {
				$website = $this->httpRequest($data, $this->job, 'GET');
			} catch (Exception $e) {
				throw $e;
			}
			$Answer = explode ("\"", $website);
		}
		$a=11;
		while (isset ($Answer [$a]) === true) {
			$x = $a + 1;
			$Exception = false;
			while (strstr ($Answer [$x], ";}") === false) {
				if ($Exception === false)
					$New = $Answer [$a] . "\"" . $Answer [$x];
				else
					$New = $New . "\"" . $Answer [$x];
				$Exception = true;
				$a++;
				$x = $a + 1;
			}
			if ($Exception === true)
				$Result [$b] = $New;
			else
				$Result [$b] = $Answer [$a];
			$a = $a +  8;
			$b++;
		}
		$Ret = serialize ($Result);
		return $Ret;
	}
	public function WriteLogError ($LS, $Fehler, $Function, $One, $Two, $Three) {
		$ToWrite = $this->readPage ("User:Luke081515Bot/Log");
		if ($Function === 1)
			$Functiontext = "Log zur Umbenennung der [[" . $One . "]] in [[" . $Two . "]]";
		else if ($Function === 2)
			$Functiontext = "Log zur Entfernung der [[" . $One . "]]";
		else if ($Function === 3)
			$Functiontext = "Log zur Substituierung der [[" . $One . "]]";
		else if ($Function === 4)
			$Functiontext = "";
		else if ($Function === 5)
			$Functiontext = "Log zur Verschiebung der Seite [[" . $One . "]] nach [[" . $Two . "]]"	;
		else if ($Function === 6)
			$Functiontext = "Log zur Verteilung der Nachricht [[" . $Two . "]] an [[" . $One . "]]";
		else if ($Function === 7)
			$Functiontext = "Log zur Erstellung der Kategorieübersicht der [[:" . $Two . "]] auf der Seite [[" . $One . "]]";
		else {}
		if ($Function < 5) {
			$ToWrite = $ToWrite . "\n\n== " . $Functiontext ."  ==\nGrund war [[" . $Three . "|diese Diskussion]]. Die Aktion wurde zu der folgenden Zeit abgeschlossen: ~~~~~\n";
			$a=0;
			if (isset ($LS [$a]) === true)
				$ToWrite = $ToWrite . "\n;Folgende Vorlageeinbindungen wurden erfolgreich bearbeitet:\n";
			while (isset ($LS [$a]) === true) {
				$ToWrite = $ToWrite . "\n* [[" . $LS [$a] . "]]";
				$a++;
			}
			$a=0;
			if (isset ($Fehler [$a]) === true)
				$ToWrite = $ToWrite . ";Bei den folgenden Seiten gab es Fehler:\n";
			while (isset ($Fehler [$a]) === true) {
				$ToWrite = $ToWrite . "\n* [[" . $Fehler [$a] . "]]";
				$a++;
			}
		} else {
			if ($Function === 5) {
				$ToWrite = $ToWrite . "\n\n== " . $Functiontext ."  ==\n* Alter Titel: [[" . $One . "]]";
				$ToWrite = $ToWrite . "\n* Neuer Titel: [[" . $Two . "]]";
				$ToWrite = $ToWrite . "\n* Begründung: " . $Three;
				$ToWrite = $ToWrite . "\n* Status: Erfolgreich verschoben";
			} else if ($Function === 6) {
				$ToWrite = $ToWrite . "\n\n== " . $Functiontext ."  ==\n* Verteilerliste: [[" . $One . "]]";
				$ToWrite = $ToWrite . "\n* Zu verteilende Nachricht: [[" . $Two . "]]";
				$ToWrite = $ToWrite . "\n* Begründung: " . $Three;
				if (isset ($LS [0]) === true) {
					$ToWrite = $ToWrite . "\n;Auf die folgenden Seiten wurde die Nachricht zugestellt:";
					$a=0;
					while (isset ($LS [$a]) === true) {
						$ToWrite = $ToWrite . "\n* [[:" . $LS [$a] . "]]";
						$a++;
					}
				}	
				if (isset ($Fehler [0]) === true) {
					$ToWrite = $ToWrite . "\n;Auf folgenden Seiten konnte keine Nachricht zugestellt werden:";
					$a=0;
					while (isset ($Fehler [$a]) === true) {
						$ToWrite = $ToWrite . "\n* [[:" . $Fehler [$a] . "]]";
						$a++;
					}
				}
			} else if ($Function === 7) {
				$ToWrite = $ToWrite . "\n\n== " . $Functiontext ."  ==\n* Analysierte Kategorie: [[:" . $Two . "]]";
				$ToWrite = $ToWrite . "\n* Zielseite: [[" . $One . "]]";
				$ToWrite = $ToWrite . "\n* Gewähltes Limit: " . $Three;
				$ToWrite = $ToWrite . "\n* Anzahl gefundene Einträge: " . $LS;
				$ToWrite = $ToWrite . "\n* Gelistete Einträge: " . $Fehler; //auch wenn hier der Name der Variable nicht passt
			} else {}
		}
		$ToWrite = $ToWrite . "\n\n--~~~~";
		$this->editPage("User:Luke081515Bot/Log", $ToWrite, "Bot: Schreibe Log &uuml;ber den letzten Auftrag");
	}
	private function doService ($Page) {
		$Content = $this->readPage($Page);
		$Content = str_replace("[[:Kategorie:", "[[Kategorie:", $Content);
		$Content = str_replace("[[:Category:", "[[Kategorie:", $Content);
		$this->editPage($Page, $Content, "Bot:Aktiviere Kategorien nach Verschiebung");
	}
}
$Bot = new QueueBot ('Luke081515Bot@dewiki', 'Queue');
?>