#!/usr/bin/php
<?php
$mwbot = new MwBot("de.wikipedia.org", "Luke081515Bot", "------");

##################
# Status: Stable #
##################

$QSSite = array (    
	"4. August 2015",
    "3. August 2015",
    "2. August 2015",
    "1. August 2015",
    "31. Juli 2015",
    "30. Juli 2015",
    "29. Juli 2015",
	);
$mwbot->main ($QSSite);
//the part till the mark is authored by APPER, and ist avaible here:
//https://de.wikipedia.org/wiki/Benutzer:APPER/MwBot.php
//Under the Creative Commons Attribution/Share Alike License

class MwBot 
{
	protected $host;
	protected $username;
	protected $password;
	protected $cookies;
 
    public function MwBot($host, $username, $password)
    {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
	}
    protected function login() 
    {
		// first step: get login token
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php";
		$website = $this->PostToHost($this->host, "/w/api.php", $data);
		$cookies = $this->GetCookies($website);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);  
		$login_token = $answer['login']['token'];
 
		// second step: get cookies
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php&lgtoken=" . urlencode($login_token);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);  
		$this->cookies = "Cookie: " . $answer['login']['cookieprefix'] . "UserName=" . $answer['login']['lgusername'] . "; " . $answer['login']['cookieprefix'] . "UserID=" . $answer['login']['lguserid'] . "; " . $answer['login']['cookieprefix'] . "Token=" . $answer['login']['lgtoken'] . "; " . $answer['login']['cookieprefix'] . "Session=" . $answer['login']['sessionid'] . ";\r\n";
    }
    public function readPage($title)
    {
		// get edit token
		$data = "action=query&prop=revisions&format=php&rvprop=content&rvlimit=1&rvcontentformat=text%2Fx-wiki&rvdir=older&rawcontinue=&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$Answer = strstr ($website, "s:1:\"*\";");
		$Answer = substr ($Answer, 8);
		$Answer = strstr ($Answer, "\"");
		$Answer = substr ($Answer, 1);
		$Answer = strstr ($Answer, "\";}}}}}}", true);
		//$Answer = urldecode ($Answer);
		return  $Answer;
    }
	// POST-Funktion
    protected function PostToHost($host, $path, $data, $cookies = "") 
    {
		$fp = fsockopen($host, 80, $errno, $errstr, 30);
		if (!$fp) { trigger_error("Cannot create fsock: $errstr ($errno)\n", E_USER_ERROR); }
		fputs($fp, "POST $path HTTP/1.0\r\n");
		fputs($fp, "Host: $host\r\n");
		fputs($fp, "User-Agent: MwBot\r\n");
		fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
		fputs($fp, "Content-length: ". strlen($data) ."\r\n");
		if ($cookies != "") fputs($fp, $cookies);
		fputs($fp, "Connection: close\r\n\r\n");
		fputs($fp, $data);
		$res = "";
		while(!feof($fp)) $res .= fgets($fp, 128);
		fclose($fp);
		return $res;
    }
 
    protected function GetCookies($website)
    {
		$cookies = "";
		$regexp = "/Set-Cookie: (.*)\r\n/iU";
		preg_match_all($regexp, $website, $treffer, PREG_SET_ORDER);
		foreach ($treffer as $wert) 
        if (trim($wert[1]) != "") $cookies .= "" . $wert[1] . "; ";
		$cookies = str_replace("; path=/", "", $cookies);
		$cookies = str_replace("; httponly", "", $cookies);
		$cookies = preg_replace("/expires=[^;]+;/i", "", $cookies);
		$cookies = preg_replace("/domain=[^;]+;/i", "", $cookies);
		return "Cookie: " . $cookies . "\r\n"; 
    }  
	//End of the part written by APPER
    //The following part was written by Luke081515
	public function main ($Site)
	{
		$a=0;
		while (isset ($Site [$a]) === true)
		{
			echo ("\n\n" . $Site [$a]);
			$SitesS = $this->getLinks ($Site [$a]);
			$Sites = unserialize ($SitesS);
			$b=0;
			while (isset ($Sites [$b]) === true)
			{
				if ($this->findLA ($Sites [$b]) === true)
					echo ("\n" . $Sites [$b]);
				$b++;
			}
			$a++;
		}
	}
	public function getLinks ($Site)
	{
		$data = "action=query&prop=links&format=xml&pllimit=5000&pldir=ascending&plnamespace=0&rawcontinue=&titles=" . urlencode("Wikipedia:Qualit&auml;tssicherung/") . urlencode($Site);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer = explode ("\"", $website);
		$b=13;
		$q=0;
		while (isset($Answer [$b]) === true)
		{
			$Result [$q] = $Answer [$b];
			$b = $b + 4;
			$q++;
		}
		$ret = serialize ($Result);
		return $ret;
	}
	public function findLA ($Site)
	{
		$Content = $this->readPage ($Site);
		if (strstr ($Content, "antragstext|tag=") !== false)
			return true;
		return false;
	}	
}
?>