#!/usr/bin/php
<?php
include 'BotCore.php';
/** InfoBoxCheckTool.php
* Analysiert nach Config, ob alle Seiten in einer Kat die Vorlage haben, oder umgekehrt
* @Author Luke081515
* @Version 1.1
* @Status Stable
*/
class IBCT extends Core
{
	protected $host;
	protected $username;
	protected $password;
	protected $cookies;
 
    public $bot = 1; // set to 0 if this edit should not be marked as bot edit

    public function IBCT ($Account, $Job, $pUseHTTPS = true) {
		$this->initcurl($Account, $Job, $pUseHTTPS = true);
		
		# WP:RFF #
		$TemplateForKat [0] = array("Vorlage:Infobox Anime-Fernsehserie", "Vorlage:Infobox Anime-Film", "Vorlage:Infobox Episode", "Vorlage:Infobox Fernsehsender", "Vorlage:Infobox Fernsehsendung", "Vorlage:Infobox Film", "Vorlage:Infobox Film Erweitert", "Vorlage:Infobox Filmreihe", "Vorlage:Infobox Einspielergebnis", "Vorlage:Infobox Original Video Animation", "Vorlage:Infobox Staffel einer Serie", "Vorlage:Infobox Video-on-Demand-Anbieter");
		$TemplateForTemplate [0] [0] = "Vorlage:Infobox Film";
		$TemplateForTemplate [0] [1] = "Vorlage:Infobox Film Erweitert";
		
		# WP:WPYT #
		$TemplateForKat [1] [0] = "Vorlage:Infobox YouTube-Kanal";
		$TemplateForTemplate [1] [0] = "Vorlage:Infobox YouTube-Kanal";
		
		# WP:RMU #
		$TemplateForKat [2] [0] = "Vorlage:Infobox Band";
		$TemplateForTemplate [2] [0] = "Vorlage:Infobox Band";
		
		# WP:WPMS # 
		$TemplateForKat [3] [0] = "Vorlage:Infobox Rennstrecke";
		$TemplateForTemplate [3] [0] = "Vorlage:Infobox Rennstrecke";
		
		# SDKmac #
		$TemplateForKat [4] [0] = "Vorlage:Infobox Flughafen";
		$TemplateForTemplate [4] [0] = "Vorlage:Infobox Flughafen";
				
		# WP:RFF #
		echo ("\nBeginne Auswertung: (1/5)");
		$this->main ("Kategorie:Filmtitel nach Jahr", $TemplateForTemplate [0],  $TemplateForKat [0], "Wikipedia:Redaktion Film und Fernsehen/Kategoriewartung", 0); //Alles normal, bisher keine Einschränkungen
		echo ("\n\n");
		
		# WP:WPYT # 
		echo ("\nBeginne Auswertung: (2/5)");
		$this->main ("Kategorie:YouTube-Kanal", $TemplateForTemplate [1],  $TemplateForKat [1], "Wikipedia:WikiProjekt YouTube/Wartungsliste", 0); //Alles normal, bisher keine Einschränkungen
		echo ("\n\n");
		
		# WP:RMU #
		#################################################################
		# Auf Wunsch der Redaktion wurde die zweite Liste abgeschaltet  #
		# nicht wieder anschalten ohne zustimmung der Redaktion!!       #
		#################################################################
		echo ("\nBeginne Auswertung: (3/5)");
		$this->main ("Kategorie:Musikgruppe nach Genre", $TemplateForTemplate [2],  $TemplateForKat [2], "Wikipedia:Redaktion Musik/Kategorienwartung", 1); //Erste Liste abgeschaltet
		echo ("\n\n");
		
		# WP:WPMS #
		echo ("\nBeginne Auswertung: (4/5)");
		$this->main ("Kategorie:Rennstrecke", $TemplateForTemplate [3],  $TemplateForKat [3], "Vorlage:Infobox Rennstrecke/Wartung", 0); //Alles normal, bisher keine Einschränkungen
		echo ("\n\n");
		
		# SDKmac #
		echo ("\nBeginne Auswertung: (5/5)");
		$this->main ("Kategorie:Flughafen nach Kontinent", $TemplateForTemplate [4],  $TemplateForKat [4], "Benutzer:Luke081515/Infobox Flughafen", 0); //Alles normal, bisher keine Einschränkungen
		echo ("\n\n");
	}
 	
	#################################################################
	#  Definition von $Disable										#
	#  0 => Alle Listen aktiviert									#
	#  1 => Obere Liste deaktiviert, also Katseiten ohne Infobox	#
	#  2 => Untere Liste deakitivert, also Infoboxseiten ohne Kat 	#
	#################################################################
	
	public function main ($Cat, $TemplateI, $TemplateII, $Lemma, $Disable)
	{
		$IGKats = $this->readSection($Lemma, 5);
		$IgnKats = explode ("* [[:", $IGKats);
		$a=1;
		$b=0;
		while (isset ($IgnKats [$a]) === true)
		{
			if (strstr ($IgnKats [$a], "]]", true) !== false)
			{
				$IgnoreKats [$b] = strstr ($IgnKats [$a], "]]", true);
				$b++;
			}
			$a++;
		}
		# Alle Seiten einer Vorlage überprüfen #
		
		if ($Disable === 2) {}
		else
		{
			echo ("\nStarte Phase 1...");
			$AllTemplatePagesS = $this->getAllEmbedings ($TemplateI);
			$AllTemplatePages = unserialize ($AllTemplatePagesS);
			$a=0;
			$b=0;
			$SubCatsS = $this->getSubcats ($Cat);
			$SubCats = unserialize ($SubCatsS);
			echo ("\nStarte Abgleich Phase 1...");
			while (isset ($AllTemplatePages [$a]) === true)
			{
				if ($this->GetPageCats ($AllTemplatePages [$a], $SubCats, $IgnoreKats) === false)
				{
					$TemplateResult [$b] = $AllTemplatePages [$a];
					$b++;
				}
				$a++;
			}
			$this->writeResults ($Lemma, $TemplateResult, false);
		}
		
		# Seiten einer Kategorie überprüfen #
		
		if ($Disable === 1) {}
		else
		{
			echo ("\nStarte Phase 2");
			$AllCatPagesS = $this->getCatMembers ($Cat);
			$AllCatsPages = unserialize ($AllCatPagesS);
			$a=0;
			$b=0;
			echo ("\nStarte Abgleich Phase 2...");
			while (isset ($AllCatsPages [$a]) === true)
			{
				if ($this->CheckIfTemplateIsSet ($AllCatsPages [$a], $TemplateII) === false)
				{
					if ($this->IgnoreCatCheck ($AllCatsPages [$a], $IgnoreKats) === false)
					{
						$CatResult [$b] = $AllCatsPages [$a];
						$b++;
					}
				}
				$a++;
			}
			$this->writeResults ($Lemma, $CatResult, true);
		}
	}
	/** writeResults
	* Speichert Ergebnisse
	* @param $Site - Seite wo gespeichert wird; $Result - Ergebnis, was gespeichert wird; $List - Welche Liste benutzt wird
	*/
	public function writeResults ($Site, $Result, $List)
	{
		$a=0;
		$b=0;
		$IgnoreList =  $this->readSection($Site, 4);
		if ($List === true)
		{
			if (isset ($Result [$a]) === true)
			{
				$Write = "==== Seiten einer Kategorie, die die Vorlage nicht enthalten ====\n\n";
				while (isset ($Result [$a]) === true)
				{
					$ResultWith = "[[" . $Result [$a] . "]]";
					if (strstr ($IgnoreList, $ResultWith) === false)
					{
						if ($b === 0)
							$Write = $Write . "[[" . $Result [$a] . "]]";
						else
							$Write = $Write . " {{subst:Benutzer:Luke081515Bot/Punkt}} [[" . $Result [$a] . "]]";
						$b++;
					}
					$a++;
				}
			}
			else
				$Write = $Write . "''Momentan keine''";
			if ($a === 1)
				$this->editSection($Site, $Write, "Bot: Aktualisiere VWorklist (" . $a . " Eintr&auml;g)", 2);
			else
				$this->editSection($Site, $Write, "Bot: Aktualisiere VWorklist (" . $a . " Eintr&auml;ge)", 2);
		}
		else
		{
			$a=0;
			$Write = "==== Seiten mit der Vorlage, die die Kategorie nicht enthalten ====\n\n";
			if (isset ($Result [$a]) === true)
			{
				while (isset ($Result [$a]) === true)
				{
					$ResultWith = "[[" . $Result [$a] . "]]";
					if (strstr ($IgnoreList, $ResultWith) === false)
					{
						if ($a === 0)
							$Write = $Write . "[[" . $Result [$a] . "]]";
						else
							$Write = $Write . " {{subst:Benutzer:Luke081515Bot/Punkt}} [[" . $Result [$a] . "]]";	
					}
					$a++;
				}
			}
			else 
				$Write = $Write . "''Momentan keine''";
			if ($a === 1)
				$this->editSection($Site, $Write, "Bot: Aktualisiere KWorklist (" . $a . " Eintr&auml;g)", 3);
			else
				$this->editSection($Site, $Write, "Bot: Aktualisiere KWorklist (" . $a . " Eintr&auml;ge)", 3);
		}
	}
	/** getParamContent
	* Liest eine Parameter aus einer Vorlage aus
	* @Param $ParamName - Name des Parameters; $Template - Name der Vorlage; $Page - Name der Seite mit der Vorlage
	* @return String, Parameterinhalt
	*/
	public function getCatMembers ($Kat)
	{
		$Result [0] = $Kat;
		$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Kat) . "&cmprop=title&cmtype=subcat&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$a=9;
		$b=1;
		$Answer = explode ("\"", $website);
		while (isset ($Answer [$a]) === true)
		{
			$Result [$b] = $Answer [$a];
			$a = $a + 6;
			$b++;
		}
		$c=1;
		while (isset ($Result [$c]) === true)
		{
			$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Result [$c]) . "&cmprop=title&cmtype=subcat&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
			try {
				$website = $this->httpRequest($data, $this->job, 'GET');
			} catch (Exception $e) {
				throw $e;
			}
			$a=9;
			$Answer = explode ("\"", $website);
			while (isset ($Answer [$a]) === true)
			{
				$Exception = false;
				$x = $a + 1;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$b++;
				$a = $a + 6;
			}
			$c++;
		}
		$b=0;
		$c=0;
		while (isset ($Result [$b]) === true)
		{
			$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Result [$b]) . "&cmprop=title&cmtype=page&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
			try {
				$website = $this->httpRequest($data, $this->job, 'GET');
			} catch (Exception $e) {
				throw $e;
			}
			$a=9;
			$Answer = explode ("\"", $website);
			while (isset ($Answer [$a]) === true)
			{
				$Exception = false;
				$x = $a + 1;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Page [$c] = $New;
				else
					$Page [$c] = $Answer [$a];
				$a = $a + 6;
				$c++;
			}
			$b++;
		}
		$b=0;
		$d=0;
		while (isset ($Page [$b]) === true)
		{
			$c=0;
			$Found = false;
			while (isset ($PageResults [$c]) === true)
			{
				if ($Page [$b] === $PageResults [$c])
				{
					$Found = true;
				}
				$c++;
			}
			if ($Found === false)
			{
				$PageResults [$d] = $Page [$b];
				$d++;
			}
			$b++;
		}
		$Ret = serialize ($PageResults);
		return $Ret;
	}
	/** getSubcats
	* Liest die Subkategorien einer Kategorie aus
	* @Param $Kat - auszulesende Kategorie
	*/
	public function getSubcats ($Kat)
	{
		$Result [0] = $Kat;
		 
		$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Kat) . "&cmprop=title&cmtype=subcat&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$a=9;
		$b=1;
		$Answer = explode ("\"", $website);
		while (isset ($Answer [$a]) === true)
		{
			$Exception = false;
				$x = $a + 1;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$b++;
				$a = $a + 6;
		}
		$c=1;
		while (isset ($Result [$c]) === true)
		{
			$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Result [$c]) . "&cmprop=title&cmtype=subcat&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
			try {
				$website = $this->httpRequest($data, $this->job, 'GET');
			} catch (Exception $e) {
				throw $e;
			}
			$a=9;
			$Answer = explode ("\"", $website);
			while (isset ($Answer [$a]) === true)
			{
				$Exception = false;
				$x = $a + 1;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$b++;
				$a = $a + 6;
			}
			$c++;
		}
		$Ret = serialize ($Result);
		return $Ret;
	}
	/** getPageCats
	* Liest die Kategorien einer Seite aus
	* @Param $Site - Seite die ausgelesen wird; $Cat - Kategorie die die Seite haben soll; $IgnoreCat - Kategorien die die Seite nicht haben soll (wird zuerst analysiert)
	* @return true - Seite hat $Kat; false - Seite hat $Kat nicht, oder hat $IgnoreCats
	*/
	public function GetPageCats ($Site, $Cat, $IgnoreCats)
	{
		$data = "action=query&prop=categories&format=php&cllimit=5000&cldir=ascending&rawcontinue=&titles=" . urlencode($Site);
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Result = explode ("\"", $website);
		$a=19;
		$b=0;
		while (isset ($Result [$a]) === true)
		{
			$Kats [$b] = $Result [$a];
			$a = $a +  6;
			$b++;
		}
		$a=0;
		$b=0;
		while (isset ($IgnoreCats [$b]) === true)
		{
			while (isset ($Kats [$a]) === true)
			{
				if ($IgnoreCats [$b] === $Kats [$a])
					return true;
				$a++;			
			}
			$b++;
			$a=0;
		}
		$b=0;
		while (isset ($Cat [$b]) === true)
		{
			while (isset ($Kats [$a]) === true)
			{
				if ($Cat [$b] === $Kats [$a])
					return true;
				$a++;			
			}
			$b++;
			$a=0;
		}
		return false;
	}
	/** IgnoreCatCheck
	* Liest die Kategorien einer Seite aus
	* @Param $Site - Seite die ausgelesen wird; $IgnoreCat - Kategorien die die Seite nicht haben soll
	* @return true - Seite hat $IgnoreCats; false - Seite hat $IgnoreCats nicht
	*/
	public function IgnoreCatCheck ($Site, $IgnoreCats)
	{
		$data = "action=query&prop=categories&format=php&cllimit=5000&cldir=ascending&rawcontinue=&titles=" . urlencode($Site);
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Result = explode ("\"", $website);
		$a=19;
		$b=0;
		while (isset ($Result [$a]) === true)
		{
			$Kats [$b] = $Result [$a];
			$a = $a +  6;
			$b++;
		}
		$a=0;
		$b=0;
		while (isset ($IgnoreCats [$b]) === true)
		{
			while (isset ($Kats [$a]) === true)
			{
				if ($IgnoreCats [$b] === $Kats [$a])
					return true;
				$a++;			
			}
			$b++;
			$a=0;
		}
		return false;
	}
	/** CheckIfTemplateIsSet
	* Schaut nach, ob die Vorlage auf der Seite vorhanden ist
	* $Site - Seite; $Template - Vorlage
	* @return true - vorhanden; false - nicht vorhanden
	*/
	public function CheckIfTemplateIsSet ($Site, $Template)
	{
		$Content = $this->readPage($Site);
		$a=0;
		while (isset ($Template [$a]) === true)
		{
			$Name = substr ($Template [$a], 8);
			$StrTemplate = "{{" . $Name;
			$AlternateName = str_replace (" ", "_", $StrTemplate);
			if (strstr ($Content, $StrTemplate) !== false)
				return true;
			if (strstr ($Content, $AlternateName) !== false)
				return true;
			$a++;
		}
		return false;
	}
	/** getAllEmbedings
	* Gibt alle Einbindungen der Vorlage aus
	* $Templ - Vorlage
	*/
	public function getAllEmbedings ($Templ)
	{
		$b=0;
		$z=0;
		while (isset ($Templ [$z]) === true)
		{
			$data = "action=query&list=embeddedin&format=php&eititle=" . urlencode($Templ [$z]) . "&einamespace=0&eidir=ascending&eifilterredir=nonredirects&eilimit=5000&rawcontinue=";
			try {
				$website = $this->httpRequest($data, $this->job, 'GET');
			} catch (Exception $e) {
				throw $e;
			}
			unset ($Answer);
			$Answer = explode ("\"", $website);
			while (strstr ($website, "eicontinue") !== false)
			{
				$a=19;
				$Continue = $Answer [7];
				while (isset ($Answer [$a]) === true)
				{
					$x = $a + 1;
					$Exception = false;
					while (strstr ($Answer [$x], ";}") === false)
					{
						if ($Exception === false)
							$New = $Answer [$a] . "\"" . $Answer [$x];
						else
							$New = $New . "\"" . $Answer [$x];
						$Exception = true;
						$a++;
						$x = $a + 1;
					}
					if ($Exception === true)
						$Result [$b] = $New;
					else
						$Result [$b] = $Answer [$a];
					$a = $a +  8;
					$b++;
				} 
				unset ($Answer);
				$data = "action=query&list=embeddedin&format=php&eititle=" . urlencode($Templ [$z]) . "&einamespace=0&eicontinue=" . urlencode($Continue) .  "&eidir=ascending&eifilterredir=nonredirects&eilimit=5000&rawcontinue=";
				try {
					$website = $this->httpRequest($data, $this->job, 'GET');
				} catch (Exception $e) {
					throw $e;
				}
				$Answer = explode ("\"", $website);
			}
			$a=11;
			while (isset ($Answer [$a]) === true)
			{
				$x = $a + 1;
				$Exception = false;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$a = $a +  8;
				$b++;
			}
			$z++;
		}
		$Ret = serialize ($Result);
		return $Ret;
	}
}
$IBCT = new IBCT('Luke081515Bot@dewiki', 'IBCT');
?>