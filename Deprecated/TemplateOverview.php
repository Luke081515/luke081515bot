#!/usr/bin/php
<?php
include 'BotCore.php';

########################
## Status: Deprecated ##
########################

class TemplateOverview extends Core
{
    protected $host;
    protected $username;
    protected $password;
    protected $cookies;
 
    public $bot = 1; // set to 0 if this edit should not be marked as bot edit
 
    public function TemplateOverview ($host, $username, $password)
    {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
		$this->main (10);
	}
	public function main ($Namespace) 
	{
		# Auslesen aller Seiten #
		$AllPages = unserialize ($this->getAllPages ($Namespace));
		$a=0;	
		$b=0;
		while (isset ($AllPages [$a]) === true)
		{
			if (strstr ($AllPages [$a], "/") === false)
			{
				$Site [$b] = 0;
				$Number [$b] = $this->getAllEmbedings ($AllPages [$a]);
				$b++;
			}
			$a++;
		}
		$Limit = 10000;
		$a=0;
		$x=0;
		while (isset ($Site [$a]) === true)
		{
			$WriteContent = "{| class=\"wikitable sortable\"\n|-\n!Seite\n!Anzahl der Einbindungen";
			while (isset ($Site [$a]) === true && $a < $Limit)
			{	
				$WriteContent = $WriteContent . "|[[" . $Site [$a] . "]]\n|" . $Number [$a] . "\n|-";
				$a++;
			}
			$WriteContent = $WriteContent . "\n|}";
			echo ($WriteContent);
			$this->savePage ("Benutzer:Luke081515Bot/Vorlagen/" . $x, $WriteContent, "Bot: Aktualisiere Liste");
			$x++;
			$Limit = $Limit + 10000;
		}
	}
	public function getAllPages ($Namespace)
	{
		$Continue = "";
		$data = "action=query&list=allpages&format=php&apcontinue=" . $Continue . "&apnamespace=" . $Namespace . "&aplimit=5000&apdir=ascending&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer = explode ("\"", $website);
		$Sites = "";
		$q = 0;
		while (strstr ($website, "apcontinue") !== false)
		{
			$b=19;
			$Continue = $Answer [7];
			echo ("\n Continue: " . $Continue);
			while (isset($Answer [$b]) === true)
			{
				$Exception = false;
				$x = $b + 1;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$b] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$b++;
					$x = $b + 1;
				}
				if ($Exception === true)
					$Sites [$q] = $New;
				else
					$Sites [$q] = $Answer [$b];
				$b = $b + 8;
				$q++;
			}
			$data = "action=query&list=allpages&format=php&apcontinue=" . $Continue . "&apnamespace=" . $Namespace . "&aplimit=5000&apdir=ascending&rawcontinue=";
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
			$Answer = explode ("\"", $website);
		}
		$b=11;
		$data = "action=query&list=allpages&format=php&apcontinue=" . $Continue . "&apnamespace=" . $Namespace . "&aplimit=5000&apdir=ascending&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer = explode ("\"", $website);
		while (isset($Answer [$b]) === true)
		{
			$Exception = false;
			$x = $b + 1;
			while (strstr ($Answer [$x], ";}") === false)
			{
				if ($Exception === false)
					$New = $Answer [$b] . "\"" . $Answer [$x];
				else
					$New = $New . "\"" . $Answer [$x];
				$Exception = true;
				$b++;
				$x = $b + 1;
			}
			if ($Exception === true)
				$Sites [$q] = $New;
			else
				$Sites [$q] = $Answer [$b];
			$b = $b + 8;
			$q++;
		}
		$ret = serialize ($Sites);
		return $ret;
	}
	public function getAllEmbedings ($Templ)
	{
		$b=0;
		$z=0;
		while (isset ($Templ [$z]) === true)
		if (isset ($Templ) === true)
		{
			$data = "action=query&list=embeddedin&format=php&eititle=" . urlencode($Templ [$z]) . "&einamespace=0&eidir=ascending&eifilterredir=nonredirects&eilimit=5000&rawcontinue=";
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
			unset ($Answer);
			$Answer = explode ("\"", $website);
			while (strstr ($website, "eicontinue") !== false)
			{
				$a=19;
				$Continue = $Answer [7];
				while (isset ($Answer [$a]) === true)
				{
					$x = $a + 1;
					$Exception = false;
					while (strstr ($Answer [$x], ";}") === false)
					{
						if ($Exception === false)
							$New = $Answer [$a] . "\"" . $Answer [$x];
						else
							$New = $New . "\"" . $Answer [$x];
						$Exception = true;
						$a++;
						$x = $a + 1;
					}
					if ($Exception === true)
						$Result [$b] = $New;
					else
						$Result [$b] = $Answer [$a];
					$a = $a +  8;
					$b++;
				} 
				unset ($Answer);
				$data = "action=query&list=embeddedin&format=php&eititle=" . urlencode($Templ [$z]) . "&einamespace=0&eicontinue=" . urlencode($Continue) .  "&eidir=ascending&eifilterredir=nonredirects&eilimit=5000&rawcontinue=";
				$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
				$website = trim(substr($website, strpos($website, "\n\r\n")));
				$Answer = explode ("\"", $website);
			}
			$a=11;
			while (isset ($Answer [$a]) === true)
			{
				$x = $a + 1;
				$Exception = false;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$a = $a +  8;
				$b++;
			}
			$z++;
		}
		$a=0;
		while (isset ($Result [$a]) === true)
			$a++;
		$a++;
		return $a;
	}	
}
$Bot = new TemplateOverview ("de.wikipedia.org", "Luke081515Bot", "------");
?>