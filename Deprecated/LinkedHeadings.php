#!/usr/bin/php
<?php

#######################
# Status: Deprecated  #
#######################

$mwbot = new MwBot("de.wikipedia.org", "Luke081515Bot", "------");
$mwbot-> getArticles ("A", "B", 2);

//the part till the mark is authored by APPER, and ist avaible here:
//https://de.wikipedia.org/wiki/Benutzer:APPER/MwBot.php
//Under the Creative Commons Attribution/Share Alike License

class MwBot
{
    protected $host;
    protected $username;
    protected $password;
    protected $cookies;
	protected $inwork;
	protected $auftraege;
	protected $artikellist;
	protected $artikellistnumber;

    public $bot = 1; // set to 0 if this edit should not be marked as bot edit

    public function MwBot($host, $username, $password)
    {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
		$artikellistnumber = 0;
	}

    protected function login()
    {
		// first step: get login token
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php";
		$website = $this->PostToHost($this->host, "/w/api.php", $data);
		$cookies = $this->GetCookies($website);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);
		$login_token = $answer['login']['token'];

		// second step: get cookies
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php&lgtoken=" . urlencode($login_token);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);
		$this->cookies = "Cookie: " . $answer['login']['cookieprefix'] . "UserName=" . $answer['login']['lgusername'] . "; " . $answer['login']['cookieprefix'] . "UserID=" . $answer['login']['lguserid'] . "; " . $answer['login']['cookieprefix'] . "Token=" . $answer['login']['lgtoken'] . "; " . $answer['login']['cookieprefix'] . "Session=" . $answer['login']['sessionid'] . ";\r\n";
    }

    public function savePage($title, $content, $summary, $Sectionnumber)
    {
		// get edit token
		$data = "action=query&format=php&prop=info&intoken=edit&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$answer = unserialize($website);
		$token = array_pop($answer['query']['pages']);
		$token = $token['edittoken'];

		// now save using the edit token
		$data = "action=edit&format=php&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=" . $this->bot . "&section=" . urlencode($Sectionnumber);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);
		// do nothing with the answer... ;)
    }

    public function readPage($title)
    {
		$crl = curl_init();
		curl_setopt ($crl, CURLOPT_URL, "http://" . $this->host . "/w/index.php?title=" . urlencode($title) . "&action=raw");
		curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, 5); // 5 seconds
		curl_setopt ($crl, CURLOPT_USERAGENT, "MwBot");
		$ret = curl_exec($crl);
		curl_close($crl);
		return $ret;
    }

    // POST-Funktion
    protected function PostToHost($host, $path, $data, $cookies = "")
    {
		$fp = fsockopen($host, 80, $errno, $errstr, 30);
		if (!$fp) { trigger_error("Cannot create fsock: $errstr ($errno)\n", E_USER_ERROR); }
		fputs($fp, "POST $path HTTP/1.0\r\n");
		fputs($fp, "Host: $host\r\n");
		fputs($fp, "User-Agent: MwBot\r\n");
		fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
		fputs($fp, "Content-length: ". strlen($data) ."\r\n");
		if ($cookies != "") fputs($fp, $cookies);
		fputs($fp, "Connection: close\r\n\r\n");
		fputs($fp, $data);
		$res = "";
		while(!feof($fp)) $res .= fgets($fp, 128);
		fclose($fp);
		return $res;
    }

    protected function GetCookies($website)
    {
		$cookies = "";
		$regexp = "/Set-Cookie: (.*)\r\n/iU";
		preg_match_all($regexp, $website, $treffer, PREG_SET_ORDER);
		foreach ($treffer as $wert)
        if (trim($wert[1]) != "") $cookies .= "" . $wert[1] . "; ";
		$cookies = str_replace("; path=/", "", $cookies);
		$cookies = str_replace("; httponly", "", $cookies);
		$cookies = preg_replace("/expires=[^;]+;/i", "", $cookies);
		$cookies = preg_replace("/domain=[^;]+;/i", "", $cookies);
		return "Cookie: " . $cookies . "\r\n";
    }
	//End of the part written by APPER
    //The following part was written by Luke081515

	public function getArticles ($Start, $End, $Sectionnumber)
	{
		$Step = 0;
		$From = $Start;
		$To = $End;
		$Continue = "";
		$data = "action=query&list=allpages&format=xml&apfrom=" . $From . "&apcontinue=" . $Continue . "&apto=" . $To . "&apnamespace=0&apfilterredir=nonredirects&apprfiltercascade=noncascading&aplimit=5000&apdir=ascending&apfilterlanglinks=all&apprexpiry=all&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer [$Step] = explode ("\"", $website);
		$Sites = "";
		$q = 0;
		while (strstr ($website, "<allpages apcontinue=") != false)
		{
			$b=9; //Origin: 7, also +2
			$Continue = $Answer [$Step][3];
			echo ("\n Continue: " . $Continue);
			while (isset($Answer [$Step][$b]) === true)
			{
				$Sites [$q] = $Answer [$Step][$b];
				$b = $b + 6;
				$q++;
			}
			sleep (1);
			$data = "action=query&list=allpages&format=xml&apfrom=" . $From . "&apcontinue=" . $Continue . "&apto=" . $To . "&apnamespace=0&apfilterredir=nonredirects&apprfiltercascade=noncascading&aplimit=5000&apdir=ascending&apfilterlanglinks=all&apprexpiry=all&rawcontinue=";
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
			$Answer [$Step] = explode ("\"", $website);
		}
		$b=7; //Origin: 7, also +2
		$data = "action=query&list=allpages&format=xml&apfrom=" . $From . "&apcontinue=" . $Continue . "&apto=" . $To . "&apnamespace=0&apfilterredir=nonredirects&apprfiltercascade=noncascading&aplimit=5000&apdir=ascending&apfilterlanglinks=all&apprexpiry=all&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer [$Step] = explode ("\"", $website);
		while (isset($Answer [$Step][$b]) === true)
		{
			$Sites [$q] = $Answer [$Step][$b];
			$b = $b + 6;
			$q++;
		}
		echo ("\nFertig mit auslesen!");
		while (isset($Answer [$Step][$b]) === true)
		{
			$Sites [$q] = $Answer [$Step][$b];
			$b = $b + 6;
			$q++;
		}
		$h = 0;
		$Hits = "";
		$j = 0;
		while (isset ($Sites [$h]) === true)
		{
			if ($this->searchLink($Sites [$h]) === true)
			{
				$Hits [$j] = $Sites [$h];
				$j++;
				echo (" " . $j . " ");
			}
			$h++;
		}
		$Result = "";
		echo ("\nBeginne Schreiben....");
		$Result = "=== " . $Start . " ===";
		$v = 0;
		while (isset ($Hits [$v]) === true)
		{
			$Result = $Result . "\n* [[" . $Hits [$v] . "]]";
			$v++;
		}
		//$this->savePage ("Benutzer:Luke081515/VerlinkteUeberschriften", $Result, "Bot: Aktualisiere Liste", $Sectionnumber);
	}
	
	public function searchLink($Sites)
	{
		$i = true;
		$Result = "";
		$Page = $this->readPage($Sites);
		if ($i === true)
		{
			if (strstr ($Page, "====== ") != false)
            {
                $Zwischenergebnis = $this->readPage($Sites);
                while (strstr ($Zwischenergebnis, "====== ") != false)
                {
					$Zwischenergebnis = strstr ($Zwischenergebnis, "== ");
					$Level6 = strstr ($Zwischenergebnis, "== ", false);
					$Pos = strpos ($Zwischenergebnis, "==");
					$Zwischenergebnis = substr ($Zwischenergebnis, 6);
					$Level6 = strstr ($Zwischenergebnis, "==", true);
					$Pos = strpos ($Zwischenergebnis, "==");
					$Zwischenergebnis = substr ($Zwischenergebnis, $Pos+5);
					$i = true;
                    //echo ("\n" . $Level2);
                    if (strstr ($Level6, "[") != false || (strstr ($Level6, "]") != false))
                    {
                        echo ("\n" . $Level6);
						return true;
                    }
                }
            }
			if (strstr ($Page, "===== ") != false)
            {
                $Zwischenergebnis = $this->readPage($Sites);
				while (strstr ($Zwischenergebnis, "===== ") != false)
                {
                    $Zwischenergebnis = strstr ($Zwischenergebnis, "== ");
					$Level5 = strstr ($Zwischenergebnis, "== ", false);
					$Pos = strpos ($Zwischenergebnis, "==");
					$Zwischenergebnis = substr ($Zwischenergebnis, 5);
					$Level5 = strstr ($Zwischenergebnis, "==", true);
					$Pos = strpos ($Zwischenergebnis, "==");
					$Zwischenergebnis = substr ($Zwischenergebnis, $Pos+5);
					$i = true;
                    //echo ("\n" . $Level2);
                    if (strstr ($Level5, "[") != false || (strstr ($Level5, "]") != false))
					{
                        echo ("\n" . $Level5);
						return true;
						
                    }
                }
            }
			if (strstr ($Page, "==== ") != false)
            {
                $Zwischenergebnis = $this->readPage($Sites);
                while (strstr ($Zwischenergebnis, "==== ") != false)
                {
                    $Zwischenergebnis = strstr ($Zwischenergebnis, "== ");
					$Level4 = strstr ($Zwischenergebnis, "== ", false);
					$Pos = strpos ($Zwischenergebnis, "==");
					$Zwischenergebnis = substr ($Zwischenergebnis, 4);
					$Level4 = strstr ($Zwischenergebnis, "==", true);
					$Pos = strpos ($Zwischenergebnis, "==");
					$Zwischenergebnis = substr ($Zwischenergebnis, $Pos+4);
					$i = true;
                    //echo ("\n" . $Level2);
                    if (strstr ($Level4, "[") != false || (strstr ($Level4, "]") != false))
                    {
						echo ("\n" . $Level4);
						return true;
						
                    }
                }
            }
			if (strstr ($Page, "=== ") != false)
            {
                $Zwischenergebnis = $this->readPage($Sites);
                while (strstr ($Zwischenergebnis, "=== ") != false)
                {
                   $Zwischenergebnis = strstr ($Zwischenergebnis, "== ");
					$Level3 = strstr ($Zwischenergebnis, "== ", false);
					$Pos = strpos ($Zwischenergebnis, "==");
					$Zwischenergebnis = substr ($Zwischenergebnis, 3);
					$Level3 = strstr ($Zwischenergebnis, "==", true);
					$Pos = strpos ($Zwischenergebnis, "==");
					$Zwischenergebnis = substr ($Zwischenergebnis, $Pos+3);
					$i = true;
                    //echo ("\n" . $Level2);
                    if (strstr ($Level3, "[") != false || (strstr ($Level3, "]") != false))
                    {
                        echo ("\n" . $Level3);
						return true;
						
					}
                }
            }
			if (strstr ($Page, "== ") != false)
			{
				$Zwischenergebnis = $this->readPage($Sites);
				while (strstr ($Zwischenergebnis, "== ") != false)
				{
					$Zwischenergebnis = strstr ($Zwischenergebnis, "== ");
					$Level2 = strstr ($Zwischenergebnis, "== ", false);
					$Pos = strpos ($Zwischenergebnis, "==");
					$Zwischenergebnis = substr ($Zwischenergebnis, 2);
					$Level2 = strstr ($Zwischenergebnis, "==", true);
					$Pos = strpos ($Zwischenergebnis, "==");
					$Zwischenergebnis = substr ($Zwischenergebnis, $Pos+3);
					$i = true;
					//echo ("\n" . $Level2);
					if (strstr ($Level2, "[") != false || (strstr ($Level2, "]") != false))
					{
						echo ("\n" . $Level2);
						return true;
						
					}
				}
			}
		}
		return false;
	}
}
?>