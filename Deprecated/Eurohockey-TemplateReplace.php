#!/usr/bin/php
<?php
$mwbot = new MwBot("de.wikipedia.org", "Luke081515Bot", "------.");
$mwbot->FindTask();
//the part till the mark is authored by APPER, and ist avaible here:
//https://de.wikipedia.org/wiki/Benutzer:APPER/MwBot.php
//Under the Creative Commons Attribution/Share Alike License

#######################
# Status: Deprecated  #
#######################

class MwBot 
{
    protected $host;
    protected $username;
    protected $password;
    protected $cookies;
	protected $LogSites;
	protected $Number;
	protected $LogNumbers;
 
    public $bot = 1; // set to 0 if this edit should not be marked as bot edit
 
    public function MwBot($host, $username, $password)
    {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
		$Number = 0;
	}
 
    protected function login() 
    {
		// first step: get login token
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php";
		$website = $this->PostToHost($this->host, "/w/api.php", $data);
		$cookies = $this->GetCookies($website);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);  
		$login_token = $answer['login']['token'];
 
		// second step: get cookies
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php&lgtoken=" . urlencode($login_token);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);  
		$this->cookies = "Cookie: " . $answer['login']['cookieprefix'] . "UserName=" . $answer['login']['lgusername'] . "; " . $answer['login']['cookieprefix'] . "UserID=" . $answer['login']['lguserid'] . "; " . $answer['login']['cookieprefix'] . "Token=" . $answer['login']['lgtoken'] . "; " . $answer['login']['cookieprefix'] . "Session=" . $answer['login']['sessionid'] . ";\r\n";
    }
 
    public function savePage($title, $content, $summary)
    {
		// get edit token
		$data = "action=query&format=php&prop=info&intoken=edit&titles=" . urlencode($title);
		//$data = "action=query&meta=tokens&format=php&type=csrf";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$answer = unserialize($website);
		$token = array_pop($answer['query']['pages']);
		$token = $token['edittoken'];
 
		// now save using the edit token
		$data = "action=edit&format=php&nocreate=&maxlag=5&assert=bot&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=" . $this->bot;
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);
		return $website;
    }
 
    public function readPage($title)
    {
		// get edit token
		$data = "action=query&prop=revisions&format=php&rvprop=content&rvlimit=1&rvcontentformat=text%2Fx-wiki&rvdir=older&rawcontinue=&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$Answer = strstr ($website, "s:1:\"*\";");
		$Answer = substr ($Answer, 8);
		$Answer = strstr ($Answer, "\"");
		$Answer = substr ($Answer, 1);
		$Answer = strstr ($Answer, "\";}}}}}}", true);
		//$Answer = urldecode ($Answer);
		return  $Answer;
    }
 
    // POST-Funktion
    protected function PostToHost($host, $path, $data, $cookies = "") 
    {
		$fp = fsockopen($host, 80, $errno, $errstr, 30);
		if (!$fp) { trigger_error("Cannot create fsock: $errstr ($errno)\n", E_USER_ERROR); }
		fputs($fp, "POST $path HTTP/1.0\r\n");
		fputs($fp, "Host: $host\r\n");
		fputs($fp, "User-Agent: MwBot\r\n");
		fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
		fputs($fp, "Content-length: ". strlen($data) ."\r\n");
		if ($cookies != "") fputs($fp, $cookies);
		fputs($fp, "Connection: close\r\n\r\n");
		fputs($fp, $data);
		$res = "";
		while(!feof($fp)) $res .= fgets($fp, 128);
		fclose($fp);
		return $res;
    }
 
    protected function GetCookies($website)
    {
		$cookies = "";
		$regexp = "/Set-Cookie: (.*)\r\n/iU";
		preg_match_all($regexp, $website, $treffer, PREG_SET_ORDER);
		foreach ($treffer as $wert) 
        if (trim($wert[1]) != "") $cookies .= "" . $wert[1] . "; ";
		$cookies = str_replace("; path=/", "", $cookies);
		$cookies = str_replace("; httponly", "", $cookies);
		$cookies = preg_replace("/expires=[^;]+;/i", "", $cookies);
		$cookies = preg_replace("/domain=[^;]+;/i", "", $cookies);
		return "Cookie: " . $cookies . "\r\n"; 
    }  
	//End of the part written by APPER
    //The following part was written by Luke081515
	
	public function FindTask()
	{
		$Number = 0;
		$c=0;
		$d=0;
		$Sites = $this->readPage("Benutzer:Luke081515/EHC");
		$List = explode ("|-", $Sites);
		$a=2;
		while (isset ($List [$a]) === true)
		{
			if (strstr ($List [$a], "|}") === false)
			{	
				$Result = explode (" || ", $List [$a]);
				$b=0;
				if (isset ($Result [1]) === true)
				{
					$Result [0] = trim ($Result [0], "]]");
					if ($c === 0)
					{
						$Result [0] = substr ($Result [0], 5);//Test
						$c++;
					}
					else
						$Result [0] = substr ($Result [0], 5);
					$Site [$d] = $Result [0];
					echo ("\n" . $Site [$d]);
					$ID [$d] = $Result [1];
					$ID [$d] = str_replace ("\n", "", $ID [$d]);
					echo ("\n" . $ID [$d] . "\"");
					$d++;
				}
				unset ($Result);
			}
			$a++;
		}
		$a=0;
		while (isset ($Site [$a]) === true)
		{		
			$Content = $this->readPage ($Site [$a]);
			while (strstr ($Content, "{{Eurohockey-net|") !== false)
			{
				$Template = strstr ($Content, "{{Eurohockey-net|");
				$Template = strstr ($Template, "}}", true);					
				$Template = $Template . "}}";
				$Content = str_replace ($Template, "{{Eurohockey-com|ID=" . $ID[$a] . "}}", $Content);	
			}
			while (strstr ($Content, "{{eurohockey-net|") !== false)
			{
				$Template = strstr ($Content, "{{eurohockey-net|");
				$Template = strstr ($Template, "}}", true);					
				$Template = $Template . "}}";
				$Content = str_replace ($Template, "{{Eurohockey-com|ID=" . $ID[$a] . "}}", $Content);	
			}
			$this->savePage ($Site [$a], $Content, "Bot: Ersetze eine Vorlage, siehe [[Spezial:PermaLink/146203506#Ersetzen_der_Vorlage_Eurohockey-net|Diskussion]]");
			echo ("\n" . $Site [$a]);
			$a++;
			sleep (4);
		}
	}
}
?>