#!/usr/bin/php
<?php
$mwbot = new MwBot("de.wikipedia.org", "Luke081515Bot", "------");
$mwbot->main ("a", "Benutzer:Luke081515Bot/BKL/Computerspiele/A", "Kategorie:Computerspiel");
################################
# Status: Broken, unmaintained #
################################

//the part till the mark is authored by APPER, and ist avaible here:
//https://de.wikipedia.org/wiki/Benutzer:APPER/MwBot.php
//Under the Creative Commons Attribution/Share Alike License

class MwBot 
{
	protected $host;
	protected $username;
	protected $password;
	protected $cookies;
 
    public $bot = 1; // set to 0 if this edit should not be marked as bot edit
 
    public function MwBot($host, $username, $password)
    {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
	}
 
    protected function login() 
    {
		// first step: get login token
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php";
		$website = $this->PostToHost($this->host, "/w/api.php", $data);
		$cookies = $this->GetCookies($website);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);  
		$login_token = $answer['login']['token'];
 
		// second step: get cookies
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php&lgtoken=" . urlencode($login_token);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);  
		$this->cookies = "Cookie: " . $answer['login']['cookieprefix'] . "UserName=" . $answer['login']['lgusername'] . "; " . $answer['login']['cookieprefix'] . "UserID=" . $answer['login']['lguserid'] . "; " . $answer['login']['cookieprefix'] . "Token=" . $answer['login']['lgtoken'] . "; " . $answer['login']['cookieprefix'] . "Session=" . $answer['login']['sessionid'] . ";\r\n";
    }
 
    public function savePage($title, $content, $summary)
    {
		// get edit token
		$data = "action=query&format=php&maxlag=5&prop=info&intoken=edit&nocreate=1&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$answer = unserialize($website);
		$token = array_pop($answer['query']['pages']);
		$token = $token['edittoken'];
 
		// now save using the edit token
		$data = "action=edit&format=php&maxlag=3&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=" . $this->bot;
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		sleep (3);
		while (strstr ($website, "Waiting for") !== false)
		{
			sleep (10);
			// get edit token
			$data = "action=query&format=php&prop=info&intoken=edit&nocreate=&titles=" . urlencode($title);
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
			$answer = unserialize($website);
			$token = array_pop($answer['query']['pages']);
			$token = $token['edittoken'];
	 
			// now save using the edit token
			$data = "action=edit&format=php&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=" . $this->bot;
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
		}
	}
	public function saveSection($title, $content, $summary, $Sectionnumber)
    {
		// get edit token
		$data = "action=query&format=php&prop=info&intoken=edit&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$answer = unserialize($website);
		$token = array_pop($answer['query']['pages']);
		$token = $token['edittoken'];

		// now save using the edit token
		$data = "action=edit&format=php&maxlag=3&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=" . $this->bot . "&section=" . urlencode($Sectionnumber);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);
		// do nothing with the answer... ;)
		sleep (3);
		while (strstr ($website, "Waiting for") !== false)
		{
			sleep (10);
			// get edit token
			$data = "action=query&format=php&prop=info&intoken=edit&nocreate=&titles=" . urlencode($title);
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
			$answer = unserialize($website);
			$token = array_pop($answer['query']['pages']);
			$token = $token['edittoken'];
	 
			// now save using the edit token
			$data = "action=edit&format=php&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=" . $this->bot . "&section=" . urlencode($Sectionnumber);
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
		}
    }
    public function readPage($title)
    {
		// get edit token
		$data = "action=query&prop=revisions&format=php&rvprop=content&rvlimit=1&rvcontentformat=text%2Fx-wiki&rvdir=older&rawcontinue=&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$Answer = strstr ($website, "s:1:\"*\";");
		$Answer = substr ($Answer, 8);
		$Answer = strstr ($Answer, "\"");
		$Answer = substr ($Answer, 1);
		$Answer = strstr ($Answer, "\";}}}}}}", true);
		//$Answer = urldecode ($Answer);
		return  $Answer;
    }
	// POST-Funktion
    protected function PostToHost($host, $path, $data, $cookies = "") 
    {
		$fp = fsockopen($host, 80, $errno, $errstr, 30);
		if (!$fp) { trigger_error("Cannot create fsock: $errstr ($errno)\n", E_USER_ERROR); }
		fputs($fp, "POST $path HTTP/1.0\r\n");
		fputs($fp, "Host: $host\r\n");
		fputs($fp, "User-Agent: MwBot\r\n");
		fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
		fputs($fp, "Content-length: ". strlen($data) ."\r\n");
		if ($cookies != "") fputs($fp, $cookies);
		fputs($fp, "Connection: close\r\n\r\n");
		fputs($fp, $data);
		$res = "";
		while(!feof($fp)) $res .= fgets($fp, 128);
		fclose($fp);
		return $res;
    }
    protected function GetCookies($website)
    {
		$cookies = "";
		$regexp = "/Set-Cookie: (.*)\r\n/iU";
		preg_match_all($regexp, $website, $treffer, PREG_SET_ORDER);
		foreach ($treffer as $wert) 
        if (trim($wert[1]) != "") $cookies .= "" . $wert[1] . "; ";
		$cookies = str_replace("; path=/", "", $cookies);
		$cookies = str_replace("; httponly", "", $cookies);
		$cookies = preg_replace("/expires=[^;]+;/i", "", $cookies);
		$cookies = preg_replace("/domain=[^;]+;/i", "", $cookies);
		return "Cookie: " . $cookies . "\r\n"; 
    }  
	//End of the part written by APPER
    //The following part was written by Luke081515
	public function main ($Letter, $Lemma, $Kat)
	{
		$KatsS = $this->getSubcats ($Kat);
		$Kats = unserialize ($KatsS);
		$c=0;
		$AllBKLSS = $this->getCatMembers();
		$AllBKLS = unserialize ($AllBKLSS);
		$a=0;
		while (isset ($AllBKLS [$a]) === true)
		{
			$Lower = strtolower ($AllBKLS [$a]);
			if (substr ($Lower, 0, 1) === $Letter)
			{
				$LinksS = $this->getLinks ($AllBKLS [$a]);
				if (isset ($LinksS) === true && $LinksS === -1)
				{}
				else if (isset ($LinksS) === true)
				{
					$Links = unserialize ($LinksS);
					$b=0;
					while (isset ($Links [$b]) === true)
					{
						echo ("\n" . $Links [$b]);
						if ($this->ExcludeFalsePositives ($Links [$b], $AllBKLS [$a]) === false)
						{
							if ($this->GetPageCats ($Links [$b], $Kats) === true)
							{
								echo ("     true");
								$BKLResult [$c] = $AllBKLS [$a];
								$SiteResult [$c] = $Links [$b];
								$c++;
							}
						}
						$b++;
					}				
				}
				else {}
			}		
			$a++;
		}
		$this->writeResults ($BKLResult, $SiteResult, $Lemma);
	}
	public function writeResults ($BKLResult, $SiteResult, $Lemma)
	{
		$Write = "\n<small>Letzte Aktualisierung der Seite: ~~~~</small>";
		$a=0;
		while ((isset ($BKLResult [$a]) === true) && (isset ($SiteResult [$a]) === true))
		{
			$Write = $Write . "\n# Seite: [[:" . $SiteResult [$a] . "]], BKL: [[:" . $BKLResult [$a] . "]],";
			$a++;
		}
		$this->savePage($Lemma, $Write, "Bot: Aktualisiere Liste");
	}
	public function getCatMembers ()
	{
		$b=0;
		$data = "action=query&list=categorymembers&format=php&cmtitle=Kategorie%3ABegriffskl%C3%A4rung&cmprop=title&cmnamespace=0&cmtype=page&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer = explode ("\"", $website);
		while (strstr ($website, "cmcontinue") !== false)
		{
			$Continue = $Answer [7];
			$a=17;
			while (isset ($Answer [$a]) === true)
			{
				$x = $a + 1;
				$Exception = false;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$a = $a +  6;
				$b++;
				sleep (0.3);
			}
			$data = "action=query&list=categorymembers&format=php&cmtitle=Kategorie%3ABegriffskl%C3%A4rung&cmprop=title&cmnamespace=0&cmtype=page&cmlimit=5000&cmsort=sortkey&cmdir=ascending&cmcontinue=" . urlencode ($Continue) . "&rawcontinue=";
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
			$Answer = explode ("\"", $website);
		}
		$a=9;
		while (isset ($Answer [$a]) === true)
		{
			while (isset ($Answer [$a]) === true)
			{
				$x = $a + 1;
				$Exception = false;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === true)
						$Result [$b] = $New;
					else
						$Result [$b] = $Answer [$a];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$a = $a +  6;
				$b++;
			}
		}
		$ret = serialize ($Result);
		return $ret;
	}	
	public function getLinks ($Site)
	{
		$data = "action=query&prop=linkshere&format=php&lhprop=title&lhnamespace=0&lhshow=!redirect&lhlimit=5000&rawcontinue=&titles=" . urlencode ($Site);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer = explode ("\"", $website);
		$a=19;
		$b=0;
		while (isset ($Answer [$a]) === true)
		{
			$x = $a + 1;
			$Exception = false;
			while (strstr ($Answer [$x], ";}") === false)
			{
				if ($Exception === false)
					$New = $Answer [$a] . "\"" . $Answer [$x];
				else
					$New = $New . "\"" . $Answer [$x];
				$Exception = true;
				$a++;
				$x = $a + 1;
			}
			if ($Exception === true)
				$Result [$b] = $New;
			else
				$Result [$b] = $Answer [$a];
			$a = $a +  6;
			$b++;
		}
		if (isset ($Result [0]) === true)
		{
			$ret = serialize ($Result);
			return $ret;
		}
		return -1;
	}
	public function ExcludeFalsePositives ($Site, $BKL)
	{
		$Content = $this->readPage ($Site);
		if (strstr ($Content, "|" . $BKL . "}}") !== false)
			return true;
		if (strstr ($Content, "|1=" . $BKL . "}}") !== false)
			return true;
		if (strstr ($Content, "{{Dieser Artikel") !== false)
		{
			if (strstr ($Content, "[[" . $BKL . "]].}}") !== false)
				return true;
			if (strstr ($Content, "[[" . $BKL . "]]}}") !== false)
				return true;
		}
		return false;
	}
	public function GetPageCats ($Site, $Cat)
	{
		$data = "action=query&prop=categories&format=php&cllimit=5000&cldir=ascending&rawcontinue=&titles=" . urlencode($Site);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Result = explode ("\"", $website);
		$a=19;
		$b=0;
		while (isset ($Result [$a]) === true)
		{
			$Kats [$b] = $Result [$a];
			$a = $a +  6;
			$b++;
		}
		$a=0;
		$b=0;
		while (isset ($Cat [$b]) === true)
		{
			while (isset ($Kats [$a]) === true)
			{
				if ($Cat [$b] === $Kats [$a])
					return true;
				$a++;			
			}
			$b++;
			$a=0;
		}
		$b=0;
		return false;
	}
	public function getSubcats ($Kat)
	{
		$Result [0] = $Kat;
		$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Kat) . "&cmprop=title&cmtype=subcat&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$a=9;
		$b=1;
		$Answer = explode ("\"", $website);
		while (isset ($Answer [$a]) === true)
		{
			$Exception = false;
				$x = $a + 1;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$b++;
				$a = $a + 6;
		}
		$c=1;
		while (isset ($Result [$c]) === true)
		{
			$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Result [$c]) . "&cmprop=title&cmtype=subcat&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
			$a=9;
			$Answer = explode ("\"", $website);
			while (isset ($Answer [$a]) === true)
			{
				$Exception = false;
				$x = $a + 1;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$b++;
				$a = $a + 6;
			}
			$c++;
		}
		$Ret = serialize ($Result);
		return $Ret;
	}	
}
?>	