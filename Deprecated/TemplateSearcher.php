#!/usr/bin/php
<?php
$mwbot = new MwBot("de.wikipedia.org", "Luke081515Bot", "------");

//the part till the mark is authored by APPER, and ist avaible here:
//https://de.wikipedia.org/wiki/Benutzer:APPER/MwBot.php
//Under the Creative Commons Attribution/Share Alike License


#######################
# Status: Deprecated  #
#######################


class MwBot 
{
	protected $host;
	protected $username;
	protected $password;
	protected $cookies;
	protected $inwork;
	protected $auftraege;
 
    public $bot = 1; // set to 0 if this edit should not be marked as bot edit
 
    public function MwBot($host, $username, $password)
    {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
		$RevLists ="Liste der britischen Meister im Schach;Liste der französischen Meister im Schach;Liste der Nationalen Meister des Deutschen Schachbundes;Liste der russischen Meister im Schach;Liste der aserbaidschanischen Meister im Schach;Liste der Berliner Meister im Schach;Liste der deutschen Meisterschaften im Schach;Liste der Ehren-Großmeister im Schach;Liste der Schachgroßmeister;Liste der Schachspieler mit einer Elo-Zahl über 2700;Liste historischer Schachspieler;Liste der polnischen Meister im Schach;Liste der Schachspieler in der US Chess Hall of Fame;Jacob Aagaard;Manuel Aaron;Fərid Abbasov;Nicat Abbasov;Thal Abergel;István Abonyi;Gerald Abrahams;Boško Abramović;Lew Jakowlewitsch Abramow;Aryam Abreu Delgado;Péter Ács;Michael Adams (Schachspieler);Weaver W. Adams;B. Adhiban;Utut Adianto;Ahmed Adly;András Adorján (Schachspieler);Yochanan Afek;Wladimir Iljitsch Afromejew;Cəmil Ağamalıyev;Simen Agdestein;Nelli Aghinjan;Evgeny Agrest;Georgi Tadschikanowitsch Agsamow;Volker Ahmels;Carl Ahues;Jurij Ajrapetjan;Ralf Åkesson;Varuzhan Akobian;Katrin Aladjova-Kusznirczuk;Simon Sinowjewitsch Alapin;Arseni Nailewitsch Alawkin;Adolf Albin;Maria Albuleț;Lew Ossipowitsch Alburt;Conel Hugh O’Donel Alexander;Nana Alexandria;Jewgeni Wladimirowitsch Alexejew;Aljaksej Aljaksandrau;Alexander Alexandrowitsch Aljechin;Johann Baptist Allgaier;Enrique Almada (Schachspieler);Zoltán Almási;Omar Almeida Quintana;Salvador Alonso;Ladislav Alster;Boris Alterman;Aramis Álvarez Pedraza;Mesgen Amanov;Friedrich Amelung;Bassem Amin;Farruch Amonatow;Claudia Amura;Viswanathan Anand;Alisher Anarkulow;Aschot Anastassjan;Bajanmönchiin Anchtschimeg;Adolf Anderssen;Ulf Andersson;Dmitri Wladimirowitsch Andreikin;Sawen Andriasjan;Darko Anić;Emil Anka;Wenzel Edler von Ankerberg;Gergely Antal;Eugène Michel Antoniadi;Wladimir Sergejewitsch Antoschin;Manuel Apicella;Ralf Appel;Meri Arabidse;Ketewan Arachamia-Grant;Giovanna Arbunic Castro;Walter Arencibia;Olexander Areschtschenko;Keith Arkell;Eric Arnlind;Lothar Arnold;Jonathan Arnott;Lewon Aronjan;Lew Abramowitsch Aronson;Lewan Aroschidse;Georgy Arsumanjan;Sergei Asarow;Andreas Ascharin;Maurice Ashley;Surab Asmaiparaschwili;Karen Asrjan;Lajos Asztalos;Ekaterina Atalık;Suat Atalık;Henry Ernest Atkins;Jeanne d’Autremont;Uri Avner;Juri Lwowitsch Awerbach;Boris Awruch;Wladislaw Glebowitsch Axelrod;Helga Axt;Rəşad Babayev;Vlastimil Babula;Alexander Baburin;Andreas Bachmann (Schachspieler);Axel Bachmann;Étienne Bacrot;Paul Baender;Camilla Baginskaite;Rıfat Bağırov;Wladimir Bagirow;Walter Bähr;Mary Bain;John Washington Baird;Wolodymyr Baklan;Juri Sergejewitsch Balaschow;Jakob Balcerak;Rosendo Balinas;Ilija Balinow;Imre Balog;Csaba Balogh;János Balogh (Schachspieler);Alexander Bangiev;Anatoly Bannik;Tamás Bánusz;David Baramidze;Dmitry Barash;Gerardo Barbero;Gedeon Barcza;László Barczay;Olaf Barda;Curt von Bardeleben;Leonard Barden;Jewgeni Ilgisowitsch Barejew;Jonas Barkhagen;Daniel Barría Zuñiga;Alexei Barsov;Mateusz Bartel;Michael Basman;Herbert Bastian;Christian Bauer (Schachspieler);Johann Hermann Bauer;Fritz Baumbach;Siegfried Baumegger;Reinhard Baumhus;Gertrude Baumstark;Bernhard Bayer;Werner Beckemeyer;Albert Becker (Schachspieler);Martin Alexander Becker;Martin Beheim-Schwarzbach;Carl Behting;Johann Behting;Günther Beikert;Waleri Beim;Mindaugas Beinoras;Alexander Beliavsky;Jordanka Belić;Erika Belle;Mario Belli Pino;Jana Bellin;Wladimir Sergejewitsch Below;Martina Beltz;Frans G. Bengtsson;Alfred Beni;Clarice Benini;Joel Benjamin;Otto Benkner;Francisco Benkö;Pál Benkő;Kiprian Berbatow;Salvijus Berčys;Dávid Bérczes;Alexander Berelowitsch;Irina Berezina;Igor Berezovsky;Emanuel Berg;Johann Berger (Schachtheoretiker);Volf Bergraser;Ferenc Berkes (Schachspieler);Hans Berliner;Ivar Bern;Ossip Bernstein;Dieter Bertholdt;Mario Bertok;Ilze Bērziņa;Katarina Beskow;Michail Stepanowitsch Beskrowny;Michael Bezold;Max Bezzel;Edwin Bhend;Carlos Bielicki;István Bilek;Paul Rudolf von Bilguer;Falko Bindrich;Henry Edward Bird;Klaus Bischoff;Arthur Bisguier;Peter Biyiasas;Joseph Henry Blackburne;Dragiša Blagojević;Pavel Blatný (Schachspieler);Jörg Blauert;Ludwig Bledow;Paweł Blehm;Erik Blomqvist (Schachspieler);Claude Bloodgood;Benjamin Markowitsch Blumenfeld;Oscar Blumenthal;Max Blümich;Milko Bobozow;Piotr Bobras;Samuel Boden;Jozef Boey;Sebastian Bogner;Efim Bogoljubow;Boban Bogosavljević;Fedir Bohatyrtschuk;Wolf Böhringer;Paolo Boi;Yuri Boidman;Nataša Bojković;Julio Bolbochán;Isaak Efremowitsch Boleslawski;Viktor Bologan;Igor Sacharowitsch Bondarewski;Uwe Bönsch;Eero Böök;Otto Borik;Borislava Borisova;Georgi Borissenko;Dénes Boros;Zuzana Borošová;Ekaterina Borulya;Alan Borwell;Michail Moissejewitsch Botwinnik;César Boutteville;Hans Bouwmeester;Thomas Bowdler;Daan Brandenburg;Arik Braun;Dennis Breder;Ilja Brener;Egon Brestian;Martin Breutigam;Gyula Breyer;Alfred Brinckmann;Wilhelm J. Brinkmann;Ante Brkić (Schachspieler);Stefan Bromberger;Dawid Ionowitsch Bronstein;Walter Browne;Hans Moritz von Brühl (Astronom);Lucas Brunner;Agnieszka Brustman;Lázaro Bruzón;Stellan Brynell;Bu Xiangzhi;Eduard Israilowitsch Buchman;Stefan Bücker;Henry Thomas Buckle;Vladimir Budde;Alexander Semjonowitsch Budo;Edmund Budrich;Rainer Buhmann;Enver Bukić;Dimitri Bunzmann;Brigitte Burchardt;Heinrich Burger (Schachspieler);Wladimir Burmakin;Amos Burn;Anna Denissowna Burtassowa;Ralph Buss;Algimantas Butnorius;Marija Butuk;Anatoli Awraamowitsch Bychowski;Jelisaweta Iwanowna Bykowa;Donald Byrne;Robert Byrne (Schachspieler);Wassili Michailowitsch Bywschew;Ricardo Calvo Mínguez;Florencio Campomanes;Daniel Cámpora;Emre Can (Schachspieler);Esteban Canal;Arianne Caoili;José Raúl Capablanca;Carl Carls;Magnus Carlsen;Pontus Carlsson;Horatio Caro;George Shoobridge Carr;Berna Carrasco;Pietro Carrera;Fabiano Caruana;Francesc de Castellví i de Vic;Mišo Cebalo;Ildar Amirowitsch Chairullin;Oscar Chajes;Alexander Walerjewitsch Chalifman;Ferenc Chalupetzky;Murray Chandler;Chapais (Schachspieler);Andrei Nikolajewitsch Chardin;Andrei Wassiljewitsch Charlow;Rudolf Charousek;Abram Josifowitsch Chasin;Fljura Chassanowa;Sergei Wladimirowitsch Chawski;Irving Chernev;André Chéron (Schachspieler);Reinhard Cherubim;Evgueni Chevelevitch;Denis Rimowitsch Chismatullin;Tamar Chmiadaschwili;Ratmir Dmitrijewitsch Cholmow;Larry Christiansen;Martin Christoffel;Vladimir Chuchelov;Ann Chumpitaz;Nino Churzidse;Slavko Cicak;Brigitta Cimarolli;Victor Ciocâltea;Dragoljub Ćirić;Dagnė Čiukšytė;Christian Clemens (Schachspieler);Hermann Clemenz;Viktorija Čmilytė-Nielsen;John Cochrane (Schachspieler);Erich Cohn;Wilhelm Cohn;Niala Collazo Hidalgo-Gato;Edgard Colle;John W. Collins;Camila Colombo;Stuart Conquest;Oskar Cordel;Emilio Córdova;Nicolaas Cortlever;Juan Corzo;Carlo Cozio;Pia Cramling;Alex Crisovan;Rachel Crotto;Alexander Csernyin;István Csom;Tünde Csonkics;Carlos Cuartas;Alexander Cunningham of Block;Alexander Cunningham (Historiker);Robert Cvek;Ognjen Cvitan;Łukasz Cyborowski;Paweł Czarnota;Richard Czaya;Attila Czebe;Moshe Czerniak;Krystyna Dąbrowska;Andria Dadiani;Eduardo Dagnino;Arthur Dake;Martijn Dambacher;Damiano de Odemira;Mato Damjanović;Branko Damljanović;Silvio Danailow;Elina Danieljan;Henrik Danielsen;Klaus Darga;Neelotpal Das;Deimantė Daulytė;Rustem Dautov;Alberto David;Nigel Davies;Cecil De Vere;Frederic Deacon;Jean-Marc Degraeve;Isabel Delemarre;Eugene Delmar;Alexandar Deltschew;Péter Dely;Jelena Dembo;Arnold Denker;Georg Deppe;Erald Dervishi;Alexandre Deschapelles;Matthias Deutschmann;Alexandre Dgebuadze;André Diamant (Schachspieler);Emil Joseph Diemer;Andreas Diermair;Mark Diesen;Ding Liren;Volkmar Dinstuhl;Egon Ditt;Sieghart Dittmann;Julija Wiktorowna Djomina;Rune Djurhuus;Maxim Dlugy;Henryk Dobosz;Jan Dobruský;Zbigniew Doda;Erik van den Doel;Sergei Wiktorowitsch Dolmatow;Jewhenija Doluchanowa;Zsófia Domány;Leinier Domínguez;Elena Donaldson-Akhmilovskaya;Alexander Donchenko;Anatoly Donchenko;Ivo Donev;Johannes Hendrikus Donner;Josif Dorfman;Fabian Döttling;Alexei Sergejewitsch Drejew;Petar Drentschew;Nana Dsagnidse;Lela Dschawachischwili;Emilija Dschingarowa;Baadur Dschobawa;Dawit Dschodschua;Pjotr Wassiljewitsch Dubinin;Marc Narciso Dublan;Serafino Dubois;Marcel Duchamp;Andreas Dückstein;Jan-Krzysztof Duda;Jürgen Dueball;Jean Dufresne;Heinz-Wilhelm Dünhaupt;Matthias Duppel;Tina Duppel;Joaquim Durão;Vasif Durarbəyli;Oldřich Duras;Fjodor Dus-Chotimirski;Marc Dutreeuw;Joanna Dworakowska;Mark Israilewitsch Dworezki;Eduard Dyckhoff;Roman Dzindzichashvili;Romain Édouard;Marsel Efroimski;Jaan Ehlvest;Louis Eichborn;Isaak Naumowitsch Eisenstadt;Johannes Eising;Max Eisinger;Ove Ekebjærg;Roland Ekström;Felipe El Debs;Erich Eliskases;Pawel Eljanow;Holger Ellers;Arpad Elo;Rudolf Elstner;Hans-Marcus Elwert;John Emms (Schachspieler);Peter Enders (Schachspieler);Lūcijs Endzelīns;Ludwig Engels;Berthold Englisch;Fritz Carl Anton Englund;Viktor Erdős;Hanna Ereńska-Barlo;Květa Eretová;Axel Martinius Erichsen;Johan Eriksson (Schachspieler);Ferenc Erkel;Sipke Ernst;Wilhelm Ernst (Schachspieler);Jaan Eslon;Lutz Espig;Johannes Esser;Jakow Borissowitsch Estrin;Max Euwe;Larry Evans;William Davies Evans;Hans Fahrni;Wadim Selmanowitsch Faibissowitsch;Sammi Fajarowicz;Rafail Alexandrowitsch Falk;Ernst Falkbeer;Iván Faragó;François Fargère;Jelena Abramowna Fatalibekowa;John Fedorowicz;Sergei Fedortschuk;Michael Fehling;Petra Feibert;Rubén Felgaer;Sébastien Feller;Bernat Fenollar;Richard Henry Falkland Fenton;Bernd Feustel;Michael Feygin;Alexandr Fier;Miroslav Filip;Andrzej Filipowicz;Waleri Wladimirowitsch Filippow;Reuben Fine;Ben Finegold;Nasar Firman;Nick de Firmian;Ingo Firnhaber;Gisela Fischdick;Bobby Fischer;Gennadij Fish;Alexander Fishbein;Aljaksej Fjodarau;Alexander Flamberg;Glenn Flear;Ernst Flechsig;Jürgen Fleck;János Flesch;Salo Flohr;Tibor Flórián;András Flumbort;Tibor Fogarasi;Alberto Foguelman;Mihaela-Veronica Foișor;Anna Fojudzka;Jan Foltys;Robert Fontaine;Leó Forgács;Győző Forintos;Eckhard Freise;Laurent Fressinet;Ali Frhat;Daniel Fridman;Friðrik Ólafsson;Henryk Friedman;Alexander Fritz;Martin Severin From;Georg Fröwis;Paulino Frydman;Ľubomír Ftáčnik;Judith Fuchs;Reinhart Fuchs;Andrija Fuderer;Maxwell Fuller;Johan Furhoff;Semjon Abramowitsch Furman;Christian Gabriel;Merab Gagunaschwili;Luís Galego;Alexander Alexandrowitsch Galkin;Joseph Gallagher;Norbert Gallinnis;Alissa Michailowna Galljamowa;Surya Shekhar Ganguly;Nona Gaprindaschwili;Anita Gara;Tícia Gara;Guillermo García González;Timur Gareev;Guntram Gärtner;Genrich Gasparjan;Viktor Gasthofer;Georgi Gatschetschiladse;Einar Gausel;Viktor Gavrikov;Jürgen Gburek;Jacek Gdański;Hans Gebhard-Elsaß;Vladas Gefenas;Hans Geiger (Schachspieler);Oscar Gelbfuhs;Mähri Geldiýewa;Boris Gelfand;Alexander Girschewitsch Geller;Efim Geller;Nico Georgiadis;Kiril Georgiew;Eugênio German;Alik Gershon;Mathias Gerusel;Bella Gesser;Ehsan Ghaem Maghami;Tigran Gharamian;Florin Gheorghiu;Ghulam Kassim;Fritz Giegold;Jessie Gilbert;Mary Gilchrist;Karl Gilg;Gennadij Ginsburg;Tamas Giorgadse;Aivars Gipslis;Anish Giri;Olga Alexandrowna Girja;Alberto Giustolisi;Igor Wladimirowitsch Glek;Svetozar Gligorić;Fernand Gobet;Michele Godena;Antje Göhler;Alexander Goldin;Rusudan Goletiani;Celso Golmayo Torriente;Manuel Golmayo Torriente;Celso Golmayo Zúpide;Vitali Golod;Harry Golombek;Werner Golz;Mary Ann Gomes;László Gonda;Alexei Fjodorowitsch Gontscharow;Stephen Gordon;Carl Göring;Daniel Gormally;George Gossip;Rumjana Gotschewa;Gottardo Gottardi;Hermann von Gottschall;Otto Götz;Mirosław Grabarczyk;Mónika Grábics;Alexander Graf;Jürgen Graf (Schachspieler);Rena Graf;Sonja Graf;Florian Grafl;Julio Ernesto Granda Zúñiga;Nils Grandelius;Boris Pawlowitsch Gratschow;Roberto Grau;Gioachino Greco;Alon Greenfeld;John Grefe;Bernhard Gregory;Gisela Gresser;Karen Aschotowitsch Grigorjan;Sergei Michailowitsch Grigorjanz;Nikolai Dmitrijewitsch Grigorjew;Boris Grimberg;Vincenz Grimm;Alexander Igorewitsch Grischtschuk;Henry Grob;Adriaan de Groot;Kiti Grosar;Attila Grószpéter;Harald Grötz;Hans-Ulrich Grünberg;Ernst Grünfeld (Schachspieler);Yehuda Grünfeld;Boris Naumowitsch Grusman;Guðfríður Lilja Grétarsdóttir;Guðmundur Sigurjónsson;Eduard Gufeld;Carlos Guimard;Boris Franzewitsch Gulko;Charles Godfrey Gümpel;Ruben Gunawan;Gerhart Gunderam;Walentina Jewgenjewna Gunina;Isidor Gunsberg;Emma Guo;Abhijeet Gupta;Ilya Gurevich;Michail Naumowitsch Gurewitsch;Buchuti Gurgenidse;Jan Gustafsson;Lev Gutman;Franz Gutmayer;Zoltán Gyimesi;Petr Hába;Yoshiharu Habu;Wilhelm Hagemann (Schachkomponist);Anna Hahn (Schachspielerin);Wladimir Hakobjan;Vitali Halberstadt;B. Hallegua;Tunç Hamarat;Hicham Hamdouchi;Désiree Hamelink;Morton Hamermesh;Rani Hamid;Jon Ludvig Hammer;Michael Hammes;Carl Hamppe;Florian Handke;Edhi Handoko;Curt Hansen;Lars Bo Hansen;Sune Berg Hansen;Wilhelm Hanstein;Khosro Harandi;D. Harika;P. Harikrishna;Ruth Haring;Daniel Harrwitz;Carl Hartlaub;William Hartston;Vüqar Həşimov;Stewart Haslinger;Thorsten Michael Haub;Haukur Angantýsson;Daniel Hausrath;Kristýna Havlíková;Mark Hebden;Hans-Joachim Hecht;Jonny Hector;Hermann Heemsoth;Anja Hegeler;Wolfgang Heidenfeld;Rita Heigl;Andreas Heimann;Gundula Heinatz;Thies Heinemann;Růžena Heinen;Herbert Heinicke;Marion Heintze;Axel Heinz;Grigori Alexandrowitsch Helbach;Helgi Áss Grétarsson;Helgi Ólafsson;Karl Helling;Johan Hellsten;Ron Henley;Walter Henneberger;Heinz von Hennig;Artur Hennings;Wally Henschel;Imre Héra;Gilberto Hernández (Schachspieler);Irisberto Herrera;Peter Hertel (Schachspieler);Gerald Hertneck;Peter Hesse;Paul Heuäcker;Tassilo von Heydebrand und der Lasa;Jörg Hickl;Wilfried Hilgert;Tiger Hillarp Persson;Hermann Hirschbach;Philipp Hirschfeld;Gregory Hjorth;Hoàng Thanh Trang;Georg Hodakowsky;Julian Hodgson;Ragnar Hoen;Michael Hoffmann (Schachspieler);Heinz Hohlfeld;Carsten Høi;Adrian S. Hollis;Oleksandr Holoschtschapow;Walther Freiherr von Holzhausen;Frank Holzke;Károly Honfi;Baldur Hönlinger;Bill Hook;Sarah Hoolt;Israel Albert Horowitz;Vlastimil Hort;Ádám Horváth;Csaba Horváth (Schachspieler);József Horváth;Péter Horváth (Schachspieler);Bernhard Horwitz;Hou Yifan;Jovanka Houska;Frank Hovde;David Howell (Schachspieler);Zbyněk Hráček;Natalja Hryhorenko;Robert Hübner;Werner Hug;Krunoslav Hulak;K. Humpy;Barbara Hund;Gerhard Hund;Isabel Hund;Juliane Hund;Harriet Hunt;Niclas Huschenbeth;Aydın Hüseynov;Qədir Hüseynov;Rəsul İbrahimov;Alexander Fjodorowitsch Iljin-Schenewski;Miguel Illescas Córdoba;Ernesto Kasbekowitsch Inarkiew;Iulia-Ionela Ionicǎ;Sergei Dmitrijewitsch Ionow;Viorel Iordăchescu;Nana Iosseliani;Alexander Ipatov;Marc Léon Bruno Joseph Gustave d’Isoard-Vauvenargue;Swiad Isoria;Andrei Istrățescu;Mária Ivánka-Budinsky;Alexander Ivanov (Schachspieler);Božidar Ivanović;Borislav Ivkov;Igor Wassiljewitsch Iwanow;Michail Iwanow (Schachspieler);Sergei Wladimirowitsch Iwanow (Schachspieler);Wassyl Iwantschuk;Jana Jacková;Michel Jadoul;Walter Jäger (Schachspieler);Dmitri Olegowitsch Jakowenko;Lora Grigorjewna Jakowlewa;Natalie Jamalia;Carl Ferdinand Jänisch;Alojzije Janković;Dragoljub Janošević;Inna Janowskaja;Dawid Janowski;Vlastimil Jansa;Ruud Janssen (Schachspieler);Artur Janturin;Linda Jap Tjoen San;Barbara Jaracz;Paweł Jaracz;Sachar Jefymenko;Wassili Wladimirowitsch Jemelin;Florian Jenni;Wladimir Wiktorowitsch Jepischin;Denis Sergejewitsch Jewsejew;Pjotr Artemjewitsch Jewtifejew;Miloš Jirovský;Jóhann Hjartarson;Leif Erlend Johannessen;Darryl Johansen;Hans Johner;Gawain Jones;Harmen Jonkman;Ju Wenjun;Leonid Grigorjewitsch Judassin;Tobias Jugelt;Alexandra Jule;Margareta Juncu;Alexei Michailowitsch Junejew;Klaus Junge;Wladimir Nikolajewitsch Jurewitsch;Vera Jürgens;Artur Jussupow;Gabriele Just;Alexander Kabatianski;Ketino Kachiani-Gersinska;Gregory Kaidanov;Osmo Kaila (Schachspieler);Sergei Leonidowitsch Kalinitschew;Gábor Kállai;Heikki Kallio;Paula Kalmar-Wolf;Radek Kalod;Sergei Michailowitsch Kaminer;Robin van Kampen;Gata Kamsky;Meelis Kanep;Marcus Kann;Albert Kapengut;Julio Kaplan;Darja Kapš;Éva Karakas;Wladimir Iwanowitsch Karassjow;Eesha Karavade;Mona Karff;Sergei Alexandrowitsch Karjakin;Lars Karlsson (Schachspieler);Anatoli Jewgenjewitsch Karpow;Isaac Kashdan;Rustam Kasimjanov;Garri Kimowitsch Kasparow;Vesta Kasputė;Miroslav Katětov;Larry Kaufman;Arthur Kaufmann (Schachspieler);Nərmin Kazımova;Raymond Keene;Ludger Keitlinghaus;Petri Kekki;Rudolf Keller (Schachspieler);Edith Keller-Herrmann;Narelle Kellner;Robert Kempiński;Edvīns Ķeņģis;Hugh Alexander Kennedy;Paul Keres;Guido Kern;Alexander Kevitz;Mona Khaled;Ibragim Khamrakulov;Boris Khanukov;Igor Khenkin;Georg Kieninger;Lionel Kieseritzky;Stefan Kindermann;Daniel King (Schachspieler);Alfred Kinzel;Simona Kiseleva;Georg Klaus;Markus Klauser;Hanna Marie Klek;Josef Klinger (Schachspieler);Tamara Klink;Michail Gerzowitsch Kljazkin;Jānis Klovāns;Klaus Klundt;Hans Kmoch;Rainer Knaak;Rolf Knobel;Hermann Knoll (Schachspieler);Viktor Knorre;Michail Robertowitsch Kobalija;Watu Kobese;Alexander Koblenz;Júlia Kočetková;Berthold Koch;Thomas Koch (Schachspieler);Boris Kogan;Friedrich Köhnlein;Dmitri Nikolajewitsch Kokarew;Dietmar Kolbus;Ignaz von Kolisch;George Koltanowski;Pawel Jewsejewitsch Kondratjew;Imre König;Jerzy Konikowski;Tatiana Kononenko;Alexander Markowitsch Konstantinopolski;Christian Köpke;Jekaterina Walerjewna Korbut;Anton Korobow;Sergei Iwanowitsch Koroljow;Viktor Kortschnoi;Dragan Kosić;Walentina Jakowlewna Koslowskaja;Nadeschda Anatoljewna Kossinzewa;Tatjana Anatoljewna Kossinzewa;Anthony Kosten;Alexandra Konstantinowna Kostenjuk;Boris Kostić;Tigran Kotanjian;Alexander Alexandrowitsch Kotow;Jan Kotrč;Vasilios Kotronias;Alexander Wassiljewitsch Kotschijew;Cenek Kottnauer;Maria Kouvatsou;Jekaterina Walentinowna Kowalewskaja;Baira Sergejewna Kowanowa;Szaja Kozłowski;Zdenko Kožul;Tim Krabbé;Yair Kraidman;Māris Krakops;Martin Krämer;Wladimir Borissowitsch Kramnik;Michał Krasenkow;Martyn Krawziw;Sandra Krege;Jan Krejčí (Schachspieler);Josef Krejcik;Martin Kreuzer;Matthias Kribben;Jens Kristiansen;Leonid Kritz;Jana Krivec;Zdenko Krnić;Nikolai Wladimirowitsch Krogius;Beate Krum;Petra Krupková;Irina Krush;Anatoli Abramowitsch Krutjanski;Jurij Kryworutschko;Günter Kuba;Wilhelm von Kügelgen;Peter Kühn (Schachspieler);Kaido Külaots;Aleksei Kulashko;Klaudia Kulon;Eva Kulovaná;Vitaly Kunin;Abraham Kupchik;Wiktar Kuprejtschyk;Bojan Kurajica;Igor Dmitrijewitsch Kurnossow;Alla Kuschnir;Henadij Kusmin;Jewgeni Filippowitsch Kusminych;Juryj Kusubow;Aloyzas Kveinys;Anni Laakmann;Aidas Labuckas;Tuulikki Laesson;Jekaterina Alexandrowna Lagno;Alexander Lagunow;Cyrus Lakdawala;Nikoletta Lakos;Erwin l’Ami;Markus Lammers;Frank Lamprecht;Konstantin Jurjewitsch Landa;Salo Landau;Claude Landenbergue;Vytautas Landsbergis;Wilfried Lange;Hannes Langrock;Zigurds Lanka;Bent Larsen;Bertold Lasker;Edward Lasker;Emanuel Lasker;Ralf Lau;Arnd Lauber;Milda Lauberte;Bruno Laurent;Tomas Laurušas;Joël Lautier;Frédéric Lazard;Viktor Láznička;Lê Quang Liêm;Daniel Alsina Leal;Jiří Lechtýnský;François Antoine de Legall;Christina Lehmann;Heinz Lehmann (Schachspieler);Bernard Leiber;Anatoli Jakowlewitsch Lein;Rafael Leitão;Catarina Leite;Giuseppe Leiva;Péter Lékó;Tatjana Lematschko;Alex Lenderman;Reinhard Lendwai;Levente Lengyel;Luka Lenič;Giovanni Leonardo da Cutri;Paul Saladin Leonhardt;Benoît Lepelletier;Kostjantyn Lerner;Felix Levin;Irina Levitina;David Levy (Schachspieler);Jerzy Lewi;Alexander Mitrofanowitsch Lewin;William Lewis (Schachspieler);Stepan Michailowitsch Lewitski;Moritz Lewitt;Elena Lewuschkina;Héctor Leyva;Li Chao;Li Ruofan;Wladimir Michailowitsch Liberson;Fabien Libiszewski;Kjetil Lie;Heinz Liebert;Tomáš Likavský;Darcy Lima;Daniil Lwowitsch Lintschewski;Paul Lipke;Isaak Lipnitzky;Samuel Lipschütz;Marta Litinskaja;Liu Wenzhe;Ljubomir Ljubojević;Jaime Lladó Lumbera;Lisandra Llaudy Pupo;Eric Lobron;Friedrich Löchner;Waleri Alexandrowitsch Loginow;Gerhard Löh;Josef Lokvenc;Giambattista Lolli;Rudolf Loman;André Lombard;William Lombardy;Maia Lomineischwili;Ruy López de Segura;Edward Löwe;Grigori Jakowlewitsch Löwenfisch;Johann Jacob Löwenthal;Samuel Loyd;Smbat Lputjan;Lu Shanglei;Norbert Lücke (Schachspieler);Myrta Ludwig;Péter Lukács;Erik Lundin (Schachspieler);Constantin Lupulescu;Thomas Luther;Anatoli Stepanowitsch Lutikow;Anke Lutz;Christopher Lutz;Patrik Lyrberg;Igor Iljitsch Lyssy;Štefan Mačák;Ronald Cadell Macdonald;Gottlieb Machate;Bartłomiej Macieja;George Henry Mackenzie;Norman Macleod;Manuela Mader;Ildikó Mádl;Karl-Heinz Maeder;Elmar Məhərrəmov;Louis-Charles Mahé de La Bourdonnais;Iris Mai;Romuald Mainka;Ilja Lwowitsch Maiselis;Jens-Uwe Maiwald;Joanna Majdan-Gajewska;Kazimierz Makarczyk;Marat Anatoljewitsch Makarow;Sergei Jurjewitsch Makarytschew;Wladimir Makogonow;Gyula Makovetz;Georgios Makropoulos;Marina Makropoulou;Suzana Maksimović;Andrij Maksymenko;Wadym Malachatko;Wladimir Nailjewitsch Malachow;Burkhard Malich;Ralph Mallée;Witali Markowitsch Malykin;Nicat Məmmədov;Rauf Məmmədov;Şəhriyar Məmmədyarov;Marina Manakov;Marija Manakova;Mikuláš Maník;Christian Mann;Marius Manolache;Georg Marco;Margeir Pétursson;Boris Margolin;Alisa Marić;Rudolf Marić;Valentí Marín;Sergio Mariotti;Alexander Markgraf;Peter Markland;Ján Markoš;Tomasz Markowski;Robert Markuš;Géza Maróczy;Sue Maroroa;Dražen Marović;Frank Marshall (Schachspieler);Andrew David Martin;Dion Martinez;William Edward Martz;Isaak Jakowlewitsch Masel;Konstantin Alexandrowitsch Maslak;James Mason (Schachspieler);Aleksandar Matanović;Ramón Mateo;Hermanis Matisons;Maxim Sergejewitsch Matlakow;Ana Matnadse;Milan Matulović;Lorenzo Mavilis;Carl Mayet;Alexander McDonnell;Colin McNab;Luke McShane;Henrique da Costa Mecking;Edmar Mednis;Eduard Meduna;Nóra Medvegy;Zoltán Medvegy;Susanto Megaranto;Annemarie Sylvia Meier;Georg Meier (Schachspieler);Kurt Meier-Boudane;Gerlef Meins;Jakob Meister;Philipp Meitner;Tatiana Melamed;Csaba Meleghegyi;Alisa Melekhina;Salome Melia;Hrant Melkumjan;Olga Menchik;Vera Menchik;Julius Mendheim;Bela Mesaros;Jonathan Mestel;Johannes Metger;Werner Metz;Ferdinand Metzenauer;Marany Meyer;Peter Michalík;Paul Michel (Schachspieler);George Michelakis;Bart Michiels;Marta Michna;David Miedema;Jacques Mieses;Normunds Miezis;Adrian Mihalčišin;Vladas Mikėnas;Igor Miladinović;Tony Miles;Borislav Milić;Sophie Milliet;Philip Stuart Milner-Barry;Gilberto Milos;Leonid Milov;Vadim Milov;Artasches Minassjan;Johannes Minckwitz;Nikolay Minev;Nicholas Theodore Miniati;Dragoljub Minić;Helene Mira;Jewhen Miroschnytschenko;Nikolai Michailowitsch Mischutschkow;Aleksander Miśta;Nikola Mitkov;Kamil Mitoń;Lilit Mkrttschjan;Nisha Mohota;Stefan Mohr;Günther Möhring;Dieter Mohrlok;Oleksandr Mojissejenko;Karel Mokrý;Batchujagiin Möngöntuul;Jorcerys Montilla;María Teresa Mora;Luciana Morales Mendoza;Olexandr Moros (Schachspieler);Alexander Sergejewitsch Morosewitsch;Paul Morphy;James Morris (Schachspieler);John Stuart Morrison;El Morro (Schachspieler);Eva Moser (Schachspielerin);Cristina Moșin;Wiktor Moskalenko;Henk Mostert;Paul Motwani;Alexander Anatoljewitsch Motyljow;Karen Movsesjan;Sergej Movsesjan;Laura Moylan;Martin Mrva;Micheil Mtschedlischwili;Muhammad al-Mudiyahki;André Muffang;Gerhard Müller (Schachspieler);Hans Müller (Schachspieler);Karsten Müller;Monika Müller-Seps;Nafisa Muminova;Andrei Murariu;Piotr Murdzia;Jacob Murey;Mladen Muše;Anna Musytschuk;Marija Musytschuk;Hugh Myers;Tamir Nabaty;Aschot Nadanjan;Kruttika Nadig;Wladimir Naef;Géza Nagy;Arkadij Naiditsch;Yuliya Naiditsch;Miguel Najdorf;Jewgeni Jurjewitsch Najer;Hikaru Nakamura;William Ewart Napier;Lancelot von Naso;Alexander Naumann (Schachspieler);David Navara;Wera Walerjewna Nebolsina;Srećko Nedeljković;Trajče Nedev;Parimarjan Negi;György Négyesy;Iivo Nei;Archibald Johnson Neilson;Jakow Issajewitsch Neistadt;Kateřina Němcová (Schachspielerin);Ivan Nemet;Wladimir Iwanowitsch Nenarokow;Jan Alexandrowitsch Nepomnjaschtschi;Michail Israiljewitsch Nepomnjaschtschi;Raschid Gibyatowitsch Neschmetdinow;Gennadi Jefimowitsch Nessis;Martin Neubauer;Thomas Neuer;Gustav Richard Neumann;Nguyễn Thị Mai Hưng;Ni Hua;Viktorija Ni;Peter Heine Nielsen;Torkil Nielsen;Walter Niephaus;Friso Nijboer;Oliver Niklasch;Lothar Nikolaiczuk;Predrag Nikolić;Adriana Nikolowa;Manfred Nimtz;Aaron Nimzowitsch;Liviu-Dieter Nisipeanu;Victor Nithander;Josef Noa;Jesús Nogueiras;Jeroen Noomen;Daniël Noteboom;Waltraud Nowarra;Nikolai Alexandrowitsch Nowotelnow;Eveline Nünchert;John Nunn;Alfred van Nüß;Tomi Nybäck;Illja Nyschnyk;Sampsa Nyysti;Leif Øgaard;John O’Hanlon;Melanie Ohme;Tõnu Õim;Kaarle Ojanen;Albéric O’Kelly de Galway;Lembit Oll;Adolf Olland;Anders Olsson (Schachspieler);Lew Jewgenjewitsch Omeltschenko;Alexander Onischuk;Abdelaziz Onkoud;Joop van Oosterom;Karel Opočenský;Tomáš Oral;Lisandra Teresa Ordaz Valdés;Jean-Louis Ormond;Rogelio Ortega;Nadya Ortiz;Filiz Osmanodja;Peter Ostermeyer;Predrag Ostojić;Mark Ousatchi;John Owen (Schachspieler);Karlis Ozols;Johanna Paasikangas-Tella;Jorma Paavilainen;Silvana Pacheco Gallardo;Luděk Pachman;Nikola Padewski;Ioana-Smaranda Pădurariu;Elisabeth Pähtz;Thomas Pähtz;Nasi Paikidse;Victor Palciauskas;Rudolf Palme;Davor Palo;Óscar Panno;Wassili Nikolajewitsch Panow;Peter Panzer;Lewan Panzulaia;Enrico Paoli;Gyula Pap (Schachspieler);Wassili Wiktorowitsch Papin;Gábor Papp;Petra Papp;Mircea Pârligras;Bruno Parma;Leili Pärnpuu;Arman Paschikjan;Louis Paulsen;Wilfried Paulsen;Duško Pavasovič;Miloš Pavlović (Schachspieler, 1964);Sven Pedersen;Jorge Pelikan;Yannick Pelletier;Peng Zhaoqin;Jonathan Penrose;Corina-Isabela Peptan;Fernando Peralta (Schachspieler);Julius Perlis (Schachspieler);Gintautas Petraitis;Tomáš Petrík;Arschak Petrosjan;Tigran Petrosjan;Vladimirs Petrovs;Alexander Dmitrijewitsch Petrow;Marian Petrow;Edin Pezerović;Michael Pfannkuche;Gerhard Pfeiffer (Schachspieler);Helmut Pfleger;Phạm Lê Thảo Nguyên;François-André Danican Philidor;Jacqueline Piatigorsky;Jürgen Pichler;Wolfgang Pietzsch;Jeroen Piket;Harry Nelson Pillsbury;Herman Pilnik;József Pintér;Zygmunt Pioch;Vasja Pirc;Gregory Pitl;Ján Plachetka;Josef Plachutta;Albin Planinc;Joseph Platz;Edward Plunkett, 18. Baron of Dunsany;Jiří Podgorný;Karl-Heinz Podzielny;Hagen Poetsch;Natalja Andrejewna Pogonina;Tomáš Polák;Giulio Cesare Polerio;Judit Polgár;Zsófia Polgár;Zsuzsa Polgár;Elisabeta Polihroniade;Igor Alexejewitsch Polowodin;Lew Abramowitsch Polugajewski;Rainer Polzin;Arturo Pomar Salamanca;Ruslan Ponomarjow;Domenico Lorenzo Ponziani;Stephan Popel;Ignacy Popiel;Gil Popilski;Artur Popławski;Petar Popović (Schachspieler);Waleri Sergejewitsch Popow (Schachspieler);Milko Poptschew;Josef Porath;James Alexander Porterfield Rynd;Lajos Portisch;Ehrhardt Post;Evgeny Postny;Wladimir Alexejewitsch Potkin;William Norwood Potter;Florian Pötz;Borki Predojević;Mignon Pretorius;Vítězslav Priehoda;Lodewijk Prins;Péter Prohászka;Ladislav Prokeš;František Josef Prokop;Swetlana Alexandrowna Prudnikowa;Roeland Pruijssers;Michael Prusikin;Dawid Przepiórka;Lew Psachis;Lenka Ptáčníková;Michail Borissowitsch Pukschanski;Cecil Purdy;Scharawyn Pürewdschaw;Yuniesky Quesada;Oscar Quiñones Carrillo;Miguel Quinteros;Namiq Quliyev;Robert Rabiega;Ilja Leontjewitsch Rabinowitsch;Itchak Radashkovich;Iwan Radulow;Julian Radulski;Krystyna Radzikowska;Markus Ragger;Wjatscheslaw Wassiljewitsch Ragosin;Vladimir Raičević;Iweta Rajlich;Vasik Rajlich;Abdullah Al-Rakib;Alejandro Ramírez;Yrjö Rantanen;Richárd Rapport;Gediminas Rastenis;Vüqar Rəsulov;Juri Sergejewitsch Rasuwajew;Wsewolod Alfredowitsch Rauser;Igor Rausis;Teymur Rəcəbov;Hans Ree;Birdie Reeve Kay;Teodor Regedziński;Thomas Reich;Dimitri Reinderman;Heinrich Reinhardt (Schachspieler);Dana Reizniece-Ozola;Ludwig Rellstab (Schachspieler);Adelquis Remón Gay;Eva Repková;Stefan Reschke;Aron Grigorjewitsch Reschko;Samuel Reshevsky;Richard Réti;Ramón Rey Ardid;Zoltán Ribli;Eros Riccio;Isaac Rice;Christian Richter (Schachspieler);Kurt Richter;Michael Richter (Schachspieler);Wolfgang Richter (Schachspieler);Eliška Richtrová;Dieter Riegler;Fritz Riemann (Schachspieler);Jean-Noël Riff;Tore Rilton;Friedl Rinder;Gerd Rinder;Domenico Ercole del Rio;Horst Rittner;Jules Arnous de Rivière;Alexander Wladimirowitsch Rjasanzew (Schachspieler);Karl Robatsch;Ray Robson;Ludwig Rödl;Orestes Rodríguez Vargas;Maxim Rodshtein;Ian Rogers;Kenneth S. Rogoff;Dorian Rogozenco;Laura Rogule;Juan Röhl;Katerina Rohonyan;Michael Roiz;Jewgeni Anatoljewitsch Romanow;Pjotr Arsenjewitsch Romanowski;Oleh Romanyschyn;Max Romih;Catharina Roodzant;Ksenia Roos;Jacob Rosanes;Alexander Borissowitsch Roschal;Carl Wilhelm Rosenkrantz;Samuel Rosenthal;Héctor Rossetto;Arkadi Rotstein;Jefim Rotstein;Dmitri Ossipowitsch Rowner;Jonathan Rowson;Eduardas Rozentalis;Vesna Rožič;Ruan Lufei;Igor Georgijewitsch Rubel;Akiba Rubinstein;Sergei Wladimirowitsch Rubljowski;Olga Nikolajewna Rubzowa;Tatjana Alexejewna Rubzowa;Róbert Ruck;Ljudmila Wladimirowna Rudenko;Mary Rudge;Anna Rudolf;Anna Rudolph;Alexander Rueb;Matthias Rüfenacht;Volkhard Rührig;Alexander Rustemow;Darius Ruželė;Fernando Saavedra;Tania Sachdev;Matthew Sadler;Darmen Sadwakasow;Saeed-Ahmed Saeed;Eltac Səfərli;Shuhrat Safin;Mark Safjanowski;Wladimir Pawlowitsch Sagorowski;Pierre Saint-Amant;Alexander Nikolajewitsch Saizew;Igor Arkadjewitsch Saizew;Michail Wiktorowitsch Saizew;Ljudmila Georgijewna Saizewa;Wladimir Sak;Konstantin Rufowitsch Sakajew;Vaidas Sakalauskas;Irina Wjatscheslawowna Sakurdjajewa;Aura Cristina Salazar;Sergej Salov;Waleri Borissowitsch Salow;Alessandro Salvio;Carlo Salvioli;Hersz Salwe;Sergiu Samarian;Friedrich Sämisch;Chanda Sandipan;Mihaela Sandu;Vasile Sănduleac;Raúl Sanguineti;Anthony Santasiere;Gediminas Šarakauskas;Živilė Šarakauskienė;Oksana Sarana-Hungeling;Ortvin Sarapu;Ante Šarić;Ibro Šarić;Ivan Šarić;Gabriel Sarkissjan;Zoltán Sárosy;Jacob Henry Sarratt;K. Sasikiran;Tatjana Jakowlewna Satulowskaja;Wilfried Sauermann;Anastassija Sergejewna Sawina;Wolodymyr Sawon;Boris Wladimirowitsch Sawtschenko;Gyula Sax;Mohamad Al-Sayed;Emil Schallopp;Leonid Alexandrowitsch Schamkowitsch;Konstantine Schanawa;Lars Schandorff;Jewgeni Nikolajewitsch Schaposchnikow;Anna Scharewitsch;Gerhard Schebler;Theo van Scheltinga;Dawit Schengelia;Andreas Schenk;Jaroslaw Scherebuch;Alexei Nikolajewitsch Schestoperow;Theodor von Scheve;Emanuel Schiffers;Alexander Alexandrowitsch Schimanow;Alexei Schirow;Wladimir Wassiljewitsch Schischkin;Werner Schlachetka;Willi Schlage;Carl Schlechter;Zoya Schleining;Adolph Schliemann;Rolf Schlindwein;Philipp Schlosser;Roland Schmaltz;Lothar Schmid;Shahanah Schmid;Jessica Schmidt;Paul Felix Schmidt;Włodzimierz Schmidt;Eckhard Schmittdiel;Bernd Schneider (Schachspieler);Ilja Schneider;Veronika Schneider;Gert Schnider;Maria Schöne;Manfred Schöneberg;Wilhelm Schönmann;Georg Schories;Karl Schorn (Maler);Arnold Schottländer;Samuil Markowitsch Schuchowizki;Hans Jakob Schudel;Natalja Schukowa (Schachspielerin);Ulrich Schulze (Schachspieler);Ilja Stepanowitsch Schumow;Wladimir Fjodorowitsch Schuschpanow;Theo Schuster (Schachspieler);Lisa Schut;Aaron Schwartzman;Paulette Schwartzmann;Adolf Schwarz (Schachspieler);Miroslav Schwarz;Rolf Schwarz;Sjarhej Schyhalka;Almira Scripcenco;Marie Sebag;Elena Sedina;Christian Seel;Rüdiger Seger;Ulricke Seidemann;Yasser Seirawan;Alexei Sergejewitsch Selesnjow;Lydyja Semenowa;Martin Senff;Olav Sepp;Alfred Seppelt;Lajos Seres;Jean-Luc Seret;Dražen Sermek;Georg Seul;Samuel Sevian;Hans Seyboth;Silvia-Raluca Sgîrcea;Alexander Shabalov;Jennifer Shahade;Tal Shaked;Nikolai Shalnev;Akter Liza Shamima;Samuel Shankland;John Shaw (Schachspieler);Shen Yang;Amina Sherif;Kamran Shirazi;Evgenija Shmirina;Samy Shoker;Nigel Short;Jackson Whipps Showalter;Yury Shulman;Sebastian Siebrecht;Ulrich Sieg;Georg Siegel;Christof Sielecki;Chantal Chaudé de Silans;Jacob Silbermann;Jeremy Silman;Wladimir Pawlowitsch Simagin;Vilka Sipilä;Sanan Wjatscheslawowitsch Sjugirow;Hermann Skarke;Spyridon Skembris;Diana Skibbe;Katrīna Šķiņķe;Bogdan Śliwa;Roman Slobodjan;Jørn Sloth;Jan Smeets;Jan Smejkal;Ilia Smirin;Wassili Wassiljewitsch Smyslow;Wesley So;Bartosz Soćko;Monika Soćko;Andrei Sokolov;Ivan Sokolov;Semka Sokolović-Bertok;Alexei Pawlowitsch Sokolski;Maarten Solleveld;Stefan Solonar;Jewgeni Alexandrowitsch Soloschenkin;Achim Soltau;Hans-Hubert Sonntag;Arne Sørensen (Schachspieler);Bent Sørensen (Schachspieler);Maxim Iljitsch Sorokin;Weniamin Innokentjewitsch Sosin;Gennadi Sosonko;Georgios Souleidis;Boris Wassiljewitsch Spasski;Ljuben Spassow;Wassil Spassow;Jonathan Speelman;Abraham Speijer;Rudolf Spielmann;Gunter Spieß;Wouter Spoelman;Kevin Spraggett;Jan Michael Sprenger;Ana Srebrnič;Yaroslav Srokovski;Gideon Ståhlberg;Philipp Stamma;Nikolaus Stanec;Markus Stangl (Schachspieler);Charles Henry Stanley;Zvonko Stanojoski;Lars Stark;Nava Starr;Hans-Hilmar Staudte;Howard Staunton;Michael Stean;Antoaneta Stefanowa;Elias Stein (Schachspieler);Georg Stein (Schachspieler);Leonid Stein;Michail Steinberg;Endre Steiner;Herman Steiner;Lajos Steiner;Wilhelm Steinitz;Daniël Stellwagen;Adolf Stern (Schachspieler);Dieter Stern;René Stern;Paul van der Sterren;Agnes Stevenson;Hrvoje Stević;Wjatscheslaw Nikolajewitsch Stjaschkin;Jiří Štoček;Friedrich A. Stock;Lara Stock;Zuzana Štočková;Igor Štohl;Mark Moissejewitsch Stolberg;Gösta Stoltz;Natalia Straub;Raoul Strohhäker;Andreas Strunski;Norbert Stull;Kiryl Stupak;Surab Sturua;Mihai Șubă;Hugo Süchting;Alexei Stepanowitsch Suetin;Berthold Suhle;Šarūnas Šulskis;Mir Sultan Khan;Emil Sutovsky;Duncan Suttles;Jevgēņijs Svešņikovs;Rudolf Swiderski;Pjotr Weniaminowitsch Swidler;Wadim Wiktorowitsch Swjaginzew;Krisztián Szabó;László Szabó (Schachspieler);Péter Székely;József Szén;Narelle Szuveges;Fərhad Tahirov;Mark Jewgenjewitsch Taimanow;Jelena Kairatowna Tairowa;Michail Tal;Vladimír Talla;James Tarjan;Siegbert Tarrasch;Richard Teichmann;Sven Telljohann;George Teodoru;Igor Teplyi;Rudolf Teschner;Henrik Teske;Regina Theissl-Pokorná;Erich Thiele (Schachspieler);Marco Thinius;George Alan Thomas;Tian Tian (Schachspielerin);Róbert Tibenský;Hans Tikkanen;Jan Timman;Gert Jan Timmerman;Artjom Walerjewitsch Timofejew;Gennadij Timoščenko;Raj Tischbierek;Sergey Tiviakov;Katrine Tjølsen;Vladislav Tkachiev;Tibor Tolnai;Josep Tolosa;Alexander Kasimirowitsch Tolusch;Jewgeni Jurjewitsch Tomaschewski;Wesselin Topalow;Dirk Topolewski;Carlos Torre Repetto;Eugenio Torre;Bela Toth;Lili Tóth;Bettina Trabert;Marko Tratar;Karel Traxler;Pawel Wladimirowitsch Tregubow;Thomas Trella;Lawrence Trent;Markus Trepp;Karel Treybal;Petar Trifunović;Georgi Tringow;Eugen Tripolsky;Kayden Troff;Paul Tröger;Octávio Trompowsky;Aleksandr Truskavetsky;Cindy Tsai;Wjatscheslaw Andrejewitsch Tschebanenko;Waleri Alexandrowitsch Tschechow;Witali Alexandrowitsch Tschechower;Iwan Tscheparinow;Oleg Leonidowitsch Tschernikow;Konstantin Walerjewitsch Tschernyschow;Maia Tschiburdanidse;Michail Iwanowitsch Tschigorin;Wolodymyr Tukmakow;Renata Turauskienė;Maxim Wladimirowitsch Turow;Batchimeg Tuvshintugs;Alexandru Tyroler;Andreas Tzermiadianos;Mijo Udovčić;Anatoli Gawrilowitsch Ufimzew;Wolfgang Uhlmann;Michail Umansky;Laura Unuk;Wolfgang Unzicker;Anicetas Uogelė;Ricardo Urbina (Schachspieler);Henry Urday Cáceres;Frode Urkedal;Sergei Semjonowitsch Urussow;Anna Uschenina;Maxime Vachier-Lagrave;László Vadász;Rafael Vaganian;Tuuli Vahtra;Alexander Vaisman;Árpád Vajda;Szidónia Vajda;Francisco Vallejo Pons;Geert Van der Stricht;Craig Van Tilbury;Zoltán Varga (Schachspieler);Petr Velička;Dragoljub Velimirović;Reynaldo Vera;Verdoni;Zsuzsa Verőci;Giovanni Vescovi;Hans Vetter;Francesc Vicent;Milan Vidmar;Milan Vidmar (1909–1980);S. Vijayalakshmi;Comte Jean de Villeneuve-Esclapon;Narcís Vinyoles;Yge Visser;Erwin Voellmy;Lothar Vogt;Carmen Voicu-Jagodzinsky;Jovana Vojinović;Marek Vokáč;Leonid Vološin;Larissa Volpert;Jan Voormans;Jan Votava;Oksana Vovk;Corry Vreeken;Bojan Vučković (Schachspieler);Milan Vukcevich;Milan Vukić;Robert Graham Wade;Gertrude Wagner;Heinrich Wagner (Schachspieler);Annett Wagner-Michel;Matthias Wahls;Anatoli Wolfowitsch Waisser;Joshua Waitzkin;Carl August Walbrodt;George Walker (Schachspieler);Wang Hao (Schachspieler);Wang Yue (Schachspieler);Wang Pin;Ursula Wasnetsky;Jewgeni Andrejewitsch Wassjukow;Miyoko Watai;John L. Watson;William N. Watson;Simon Webb;Wolfgang Weber (Schachspieler);Tom Wedberg;Henri Weenink;Arlette van Weersel;Olaf Wegener (Schachspieler);Stefan Wehmeier;Wei Yi;Wolfgang Weil;Miksa Weiß;Gerhard Weißgerber;Robert K. von Weizsäcker;Iogannes Gugowitsch Weltmander;Loek van Wely;Carl Wemmers;Jan Werle;Boris Markowitsch Werlinski;Clemens Werner;Heikki Westerinen;Carola von der Weth;Jay Whitehead;Ken Whyld;Georg Wiarda;Cliff Wichmann;Iwa Widenowa;Michael Wiedenkeller;John van der Wiel;Oskar Wielgos;Cor van Wijgerden;Christian Wilhelmi;Elijah Williams;Luc Winants;Szymon Winawer;Rikard Winsnes;Heinz Wirthensohn;Andreas Wissemann;Nikita Kirillowitsch Witjugow;Alexander Wittek;Boris Timofejewitsch Wladimirow;Jewgeni Wladimirow;Klaus Wockenfuß;Alexander Wohl;Antoni Wojciechowski;Radosław Wojtaszek;Aleksander Wojtkiewicz;Siegfried Reginald Wolf;Sergei Wiktorowitsch Wolkow;Andrij Wolokitin;Mathias Womacka;Baruch Harold Wood;Alexei Borissowitsch Wyschmanawin;Marmaduke Wyvill;Xie Jun;Xu Yuanyuan;Xu Yuhua;Daniel Abraham Yanofsky;Frederick Dewhurst Yates;Ye Jiangchuan;Alex Yermolinsky;Betül Cemre Yıldız;Jouni Yrjölä;Yu Yangyi;Yuanling Yuan;Jacob Yuchtman;Willy Zabasajja;Salomėja Zaksaitė;Osvaldo Zambrana;Karen Zapata;Byron Zappas;Ilja Zaragatski;Pablo Zarnicki;Anna Zatonskih;Beata Zawadzka;Keti Zazalaschwili;Michael Zeitlein;Patrick Zelbel;Frank Zeller;Elmars Zemgalis;Sonia Zepeda;Witali Walerjewitsch Zeschkowski;Zhang Jilin;Zhang Pengxiang;Zhang Zhong;Zhao Xue;Zhou Jianchao;Zhu Chen;Ludwig Zier;Dov Zifroni;Polina Zilberman;Olga Zimina;Lothar Zinn;Helgi Ziska;Eugène Znosko-Borovsky;Anna Zozulia;Manfred Zucker;Arno Zude;Beat Züger;Coen Zuidema;Johannes Hermann Zukertort;Martin Zumsande;";
		//$this->getLists ($RevLists);
		$this->GetCategoryMembers (true);
		$this->GetCategoryMembers (false);
	}
 
    protected function login() 
    {
		// first step: get login token
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php";
		$website = $this->PostToHost($this->host, "/w/api.php", $data);
		$cookies = $this->GetCookies($website);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);  
		$login_token = $answer['login']['token'];
 
		// second step: get cookies
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php&lgtoken=" . urlencode($login_token);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);  
		$this->cookies = "Cookie: " . $answer['login']['cookieprefix'] . "UserName=" . $answer['login']['lgusername'] . "; " . $answer['login']['cookieprefix'] . "UserID=" . $answer['login']['lguserid'] . "; " . $answer['login']['cookieprefix'] . "Token=" . $answer['login']['lgtoken'] . "; " . $answer['login']['cookieprefix'] . "Session=" . $answer['login']['sessionid'] . ";\r\n";
    }
 
   public function savePage($title, $content, $summary)
    {
		// get edit token
		$data = "action=query&format=php&maxlag=5&prop=info&intoken=edit&nocreate=1&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$answer = unserialize($website);
		$token = array_pop($answer['query']['pages']);
		$token = $token['edittoken'];
 
		// now save using the edit token
		$data = "action=edit&format=php&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=" . $this->bot;
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);
		sleep (10);
		while (strstr ($website, "Waiting for") !== false)
		{
			sleep (10);
			// get edit token
			$data = "action=query&format=php&prop=info&intoken=edit&nocreate=&titles=" . urlencode($title);
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
			$answer = unserialize($website);
			$token = array_pop($answer['query']['pages']);
			$token = $token['edittoken'];
	 
			// now save using the edit token
			$data = "action=edit&format=php&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=" . $this->bot;
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
			$answer = unserialize($website);
		}
		// do nothing with the answer... ;)
    }
 
    public function readPage($title)
    {
		// get edit token
		$data = "action=query&prop=revisions&format=php&rvprop=content&rvlimit=1&rvcontentformat=text%2Fx-wiki&rvdir=older&rawcontinue=&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$Answer = strstr ($website, "s:1:\"*\";");
		$Answer = substr ($Answer, 8);
		$Answer = strstr ($Answer, "\"");
		$Answer = substr ($Answer, 1);
		$Answer = strstr ($Answer, "\";}}}}}}", true);
		//$Answer = urldecode ($Answer);
		return  $Answer;
    }
 
    // POST-Funktion
    protected function PostToHost($host, $path, $data, $cookies = "") 
    {
		$fp = fsockopen($host, 80, $errno, $errstr, 30);
		if (!$fp) { trigger_error("Cannot create fsock: $errstr ($errno)\n", E_USER_ERROR); }
		fputs($fp, "POST $path HTTP/1.0\r\n");
		fputs($fp, "Host: $host\r\n");
		fputs($fp, "User-Agent: MwBot\r\n");
		fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
		fputs($fp, "Content-length: ". strlen($data) ."\r\n");
		if ($cookies != "") fputs($fp, $cookies);
		fputs($fp, "Connection: close\r\n\r\n");
		fputs($fp, $data);
		$res = "";
		while(!feof($fp)) $res .= fgets($fp, 128);
		fclose($fp);
		return $res;
    }
 
    protected function GetCookies($website)
    {
		$cookies = "";
		$regexp = "/Set-Cookie: (.*)\r\n/iU";
		preg_match_all($regexp, $website, $treffer, PREG_SET_ORDER);
		foreach ($treffer as $wert) 
        if (trim($wert[1]) != "") $cookies .= "" . $wert[1] . "; ";
		$cookies = str_replace("; path=/", "", $cookies);
		$cookies = str_replace("; httponly", "", $cookies);
		$cookies = preg_replace("/expires=[^;]+;/i", "", $cookies);
		$cookies = preg_replace("/domain=[^;]+;/i", "", $cookies);
		return "Cookie: " . $cookies . "\r\n"; 
    }  
	//End of the part written by APPER
    //The following part was written by Luke081515
	
	public function GetCategoryMembers ($Time)
	{
		$Step = 0;
		$Continue = "";
		$data = "action=query&list=categorymembers&format=xml&cmtitle=Kategorie%3AVorlage%3Anur%20Subst&cmprop=title&cmcontinue=" . $Continue . "&cmnamespace=0%7C1%7C2%7C3%7C4%7C5%7C6%7C7%7C8%7C9%7C10%7C11%7C12%7C13%7C14%7C15%7C100%7C101%7C828%7C829&cmtype=page&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer [$Step] = explode ("\"", $website);
		$Sites = "";
		$q = 0;
		$b = 5;
		while (isset($Answer [$Step][$b]) === true)
		{
			$Sites [$q] = $Answer [$Step][$b];
			$b = $b + 4;
			$q++;
		}
		$b = 5;
		echo ("\nFertig mit auslesen!");
		$q = 1;
		$Save = "";
		while (isset($Sites [$q]) === true)
		{
			$Save = $Save . $this->searchembeddings ($Sites [$q], $Time);
			$q++;
		}
		if ($Time === true)
			$this->savePage ("Benutzer:Luke081515/Eingebundene_Subst_Vorlagen/Neuere", $Save, "Bot: Aktualisiere Liste");
		else
			$this->savePage ("Benutzer:Luke081515/Eingebundene_Subst_Vorlagen/Alle", $Save, "Bot: Aktualisiere Liste");
	}
	public function searchembeddings ($Sites, $Time)
	{
		$Result = "";
		$Continue = "";
		$data = "action=query&list=embeddedin&format=xml&eititle=" . urlencode($Sites) . "&eidir=ascending&eifilterredir=nonredirects&eilimit=5000";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer = explode ("\"", $website);
		$q = 0;
		while (strstr ($website, "<embeddedin eicontinue=") != false)
		{
			$b=9; //Origin: 7, also +2
			$Continue = $Answer [3];
			echo ("\n Continue: " . $Continue);
			sleep (1);
			$data = "action=query&list=embeddedin&format=xml&eititle=" . urlencode($Sites) . "&eidir=ascending&eicontinue=" . $Continue . "&eifilterredir=nonredirects&eilimit=5000";
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
			$Answer = explode ("\"", $website);
			while (isset($Answer [$b]) === true)
			{
				$Result [$q] = $Answer [$b];
				$b = $b + 6;
				$q++;
			}
		}
		$b=7; //Origin: 7, also +2
		$data = "action=query&list=embeddedin&format=xml&eititle=" . urlencode($Sites) . "&eidir=ascending&eifilterredir=nonredirects&eilimit=5000";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer = explode ("\"", $website);
		while (isset($Answer [$b]) === true)
		{
			$Result [$q] = $Answer [$b];
			$b = $b + 6;
			$q++;
		}
		$Save = "";
		$a = 0;
		$b = 0;
		$c = 0;
	if (isset($Result [0]) === true)
		{
			if ($Result [0] === "Wikipedia:WikiProjekt Vorlagen/Unbenutzte Vorlagen/subst" || $Sites !== "Vorlage:Nur Subst" || $Result [0] === $Sites)
			{
				if (isset($Result [1]) === true)
				{
					if ($Result [1] === "Wikipedia:WikiProjekt Vorlagen/Unbenutzte Vorlagen/subst" || $Result [1] === $Sites)
					{
						if (isset($Result [2]) === true)
						{
							$a = 1;
						}
					}
					else
					{
						if (isset($Result [1]) === true)
						{
							$a = 1;
						}
					}
				}
				else
				{
					if (isset($Result [1]) === true)
					{
						$a = 1;
					}
				}
			}
			else 
				$a = 1;
			if ($a === 1)
			{
				$Save = "\n" . "\n=== [[" . $Sites . "]] ===";
			}
		}
		$q = 0;
		while (isset($Result [$q]) === true)
		{
			if ($Result [$q] === "Wikipedia:WikiProjekt Vorlagen/Unbenutzte Vorlagen/subst" || $Result [$q] === $Sites || $Result [$q] === "Wikipedia:Textbausteine/Benutzerseiten")
			{}
			else
			{	
				if($Time === true)
				{
					if ($this->CheckTime ($Result [$q]) === true)
						$Save = $Save . "\n* [[" . $Result [$q] . "]]";
				}
				else
					$Save = $Save . "\n* [[" . $Result [$q] . "]]";
			}
			$q++;
		}
		return $Save;
	}
	protected function CheckTime ($Site)
	{
		$data = "action=query&prop=revisions&format=json&rvprop=timestamp&rvlimit=1&rawcontinue=&titles=" . urlencode($Site);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		if (strstr ($website, "2015") === false)
			return false;
		return true;
	}
	public function getLists ($RevLists)
	{
		$Border = ";";
		$RevList = explode ($Border, $RevLists);
		$a=0;
		$x=0;
		while (isset ($RevList[$x]) === true)
		{
			if ($this->analyse ($RevList[$x]) === true)
			{
				$Results [$a] = $RevList[$x];
				$a++;
			}
			$x++;
			echo (" " . $x);
		}
		$a=0;
		$ToWrite = "== Ergebnis von ~~~ von ~~~~~ ==";
		while (isset($Results [$a]) === true)
		{
			$ToWrite = $ToWrite . "\n* [[" . $Results [$a] . "]]";
			$a++;				
		}
		$this->savePage("Benutzer:Luke081515/Schachspielerliste", $ToWrite, "Bot: Aktualisiere Liste");
		echo ("\nAbgeschlossen!");
	}
	public function analyse ($Site)
	{
		if (strstr ($Site, "Liste") !== false)
			return false;
		else
		{
			$AnalysePage = $this->readPage ($Site);
			if (strstr ($AnalysePage, "www.chessgames.com") !== false)
				return false;
			if (strstr ($AnalysePage, "www.365chess.com") !== false)
				return false;
			if (strstr ($AnalysePage, "{{365chess") !== false)
				return false;
			return true;
		}
	}
}
?>