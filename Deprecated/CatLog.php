#!/usr/bin/php
<?php
include './BotCore.php';

class CatLogger extends Core 
{
	######################
    # Status: Deprecated #
	######################
    public $bot = 1; // set to 0 if this edit should not be marked as bot edit
 
    public function MwBot($account)
    {
		$this->start($account);
		$this->processLog ("Kategorie:Wikipedia:URV", "Benutzer:Luke081515/URV-Log", true);
		$this->processLog ("Kategorie:Benutzer:Importartikel", "Benutzer:Luke081515/Import-Log", true);
	}
 
	/** processLog
	* Notiert Änderungen einer Kategorie nach dem letzten Lauf
	* Speichert akuellen Zustand in einer JS-Datei
	* Gleich Änderungen gegenüber dem letzten JS-Edit ab
	* Speichert diese auf der angegebenen Seite
	* @author Luke081515 
	* @version 1.0
	*/
	public function processLog ($Kat, $Page, $Subcats)
	{
		$Content = $this->readPageJs ("Benutzer:Luke081515Bot/" . $Kat . ".js");
		$b=0;
		$c=0;
		$d=0;
		$OldSet = explode ("|", $Content);
		$NEWS = $this->getCatMembers ($Kat, $Subcats);
		$NewSet = unserialize ($NEWS);
		while (isset ($OldSet [$b]) === true)
		{
			$a=0;
			$Found = false;
			while (isset ($NewSet [$a]) === true)
			{
				if ($OldSet [$b] === $NewSet [$a]) 
					$Found = true;
				$a++;
			}
			if ($Found === false)
			{
				$Removed [$c] = $OldSet [$b];
				$c++;
			}
			$b++;
		}
		#######################################
		$b=0;
		$c=0;
		while (isset ($NewSet [$b]) === true)
		{
			$a=0;
			$Found = false;
			while (isset ($OldSet [$a]) === true)
			{
				if ($NewSet [$b] === $OldSet [$a]) 
					$Found = true;
				$a++;
			}
			if ($Found === false)
			{
				$Added [$d] = $NewSet [$b];
				$d++;
			}
			$b++;
		}
		$d=0;
		$c=0;
		$Content = $this->readPage ($Page);
		$Content = $Content . "\n== ~~~~~ ==";
		if (isset ($Added [0]) === true)
			$Content = $Content . "\n=== Hinzugekommen ===";
		while (isset ($Added [$c]) === true)
		{
			$Content = $Content . "\n* [[" . $Added [$c] . "]]";
			$c++;
		}
		if ($Removed [0] !== "")
			$Content = $Content . "\n=== Entfernt ===";
		while (isset ($Removed [$d]) === true)
		{
			if ($Removed [$d] === "") {}
			else
				$Content = $Content . "\n* [[" . $Removed [$d] . "]]";
			$d++;
		}
		$Content = $Content . "\n--~~~~";
		if (isset ($Added [0]) === true || $Removed [0] !== "")
			$this->savePage ($Page, $Content, "Bot: Aktualisiere Liste");		
		$Content = "";
		$a=0;
		while (isset ($NewSet [$a]) === true)
		{
			$Content = $Content . $NewSet [$a] . "|";
			$a++;
		}
		if (isset ($Added [0]) === true || $Removed [0] !== "")
			$this->savePage ("Benutzer:Luke081515Bot/" . $Kat . ".js", $Content, "Bot: Aktualisiere Liste");	
	}
	public function getCatMembers ($Kat, $Subcats)
	{
		$Result [0] = $Kat;
		if ($Subcats === true)
		{
			$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Kat) . "&cmprop=title&cmtype=subcat&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
			$a=9;
			$b=1;
			$Answer = explode ("\"", $website);
			while (isset ($Answer [$a]) === true)
			{
				$Result [$b] = $Answer [$a];
				$a = $a + 6;
				$b++;
			}
			$c=1;
			while (isset ($Result [$c]) === true)
			{
				$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Result [$c]) . "&cmprop=title&cmtype=subcat&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
				$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
				$website = trim(substr($website, strpos($website, "\n\r\n")));
				$a=9;
				$Answer = explode ("\"", $website);
				while (isset ($Answer [$a]) === true)
				{
					$Exception = false;
					$x = $a + 1;
					while (strstr ($Answer [$x], ";}") === false)
					{
						if ($Exception === false)
							$New = $Answer [$a] . "\"" . $Answer [$x];
						else
							$New = $New . "\"" . $Answer [$x];
						$Exception = true;
						$a++;
						$x = $a + 1;
					}
					if ($Exception === true)
						$Result [$b] = $New;
					else
						$Result [$b] = $Answer [$a];
					$b++;
					$a = $a + 6;
				}
				$c++;
			}
	}
		$b=0;
		$c=0;
		while (isset ($Result [$b]) === true)
		{
			$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Result [$b]) . "&cmprop=title&cmtype=page&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
			$a=9;
			$Answer = explode ("\"", $website);
			while (isset ($Answer [$a]) === true)
			{
				$Exception = false;
				$x = $a + 1;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Page [$c] = $New;
				else
					$Page [$c] = $Answer [$a];
				$a = $a + 6;
				$c++;
			}
			$b++;
		}
		$b=0;
		$d=0;
		while (isset ($Page [$b]) === true)
		{
			$c=0;
			$Found = false;
			while (isset ($PageResults [$c]) === true)
			{
				if ($Page [$b] === $PageResults [$c])
				{
					$Found = true;
				}
				$c++;
			}
			if ($Found === false)
			{
				$PageResults [$d] = $Page [$b];
				$d++;
			}
			$b++;
		}
		$Ret = serialize ($PageResults);
		return $Ret;
	}		
}
$Bot = new CatLogger("Luke081515Bot@dewiki");
?>