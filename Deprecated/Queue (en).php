#!/usr/bin/php
<?php

#######################
# Status: Deprecated  #
#######################

class QueueBot extends Core
{
	protected $host;
    protected $username;
    protected $password;
	public $bot = 1; // set to 0 if this edit should not be marked as bot edit
	public function QueueBot ($host, $username, $password)
    {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
		$this->mainEngine();
	}
 
	public function detectTasks ()
	{
		$inwork = false;
		echo ("\n" . time () . "--" . "Starte Task");
		if ($this->readPageJs("User:Luke081515/Bot.js") === "true")
		{
			$AllTasks = $this->readPage("User:Luke081515Bot/Queue/Requests");
			$Border = "}}";
			//echo ("\n" . time () . "--" . "Beginne Auslesen");
			$Tasks = explode ($Border, $AllTasks);
			$i = 0;
			while (isset($Tasks[$i]) === true)
			{
				$i++;
			}
			$Border = "|";
			$i--;
			for ($j = 0; $j < $i; $j++) 
			{
				$Tasks[$j] = explode ($Border, $Tasks[$j]);
				$Task[$j] = substr ($Tasks[$j][1], 5);
				$User[$j] = substr ($Tasks[$j][2], 5);
				$StartLemma[$j] = substr ($Tasks[$j][3], 11);
				$TargetLemma[$j] = substr ($Tasks[$j][4], 12);
				$Summary[$j] = substr ($Tasks[$j][5], 8);
				$Status[$j] = substr ($Tasks[$j][6], 7);
				if (isset ($Tasks[$j][7]) === true)
					$Zeitstempel[$j] = substr ($Tasks[$j][7], 12);
				else 
					$Zeitstempel[$j] = "";
			}
			//echo ("\n" . time () . "--" . "Werte ausgelesen und getrennt");
			if ($inwork===false)
			{
				//echo ("true");
				for ($j = 0; $j < $i; $j++)
				{
					if ($Status[$j] === "s")
					{
						echo ("\n Start");
						$TaskNumber = $j;
						$j = $i;
						$NumberOfTasks = $i;
						$inwork=true;
						$ToWriteDown = "";
						//$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "a", "Bot: Bearbeite Auftrag");
						echo ("\n" . time () . "--" . "Beginne Bearbeitung, Auftrag eingereit");
						//sleep (180);
					}
					//Suppressredirect
					if ($inwork===true)
					{
						echo ("\nSP");
						if ($this-> ControlIgnoreList ($User[$TaskNumber]) === true)
						{
							$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "u", "bot: user on ignorelist", $Zeitstempel);
							echo ("\n" . time () . "--" . "Ignorierter Benutzer");
							$this->NotificateOnTalkPage ($User[$TaskNumber], 4, "");
						}
						else
						{
							#####################################################
							# Verschieben ohne Weiterleitung # Status: Stable   #
							#####################################################
							if ($Task[$TaskNumber] === "sr")
							{
								$this->login();
								$NextBorder = "/";
								$EndResult = substr ($StartLemma[$TaskNumber], 5);
								$ToCheck = explode ($NextBorder, $EndResult);	
								if ($this->CheckUser ($User[$TaskNumber]) === 0)
								{
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "p", "bot: this is not your own usernamespace", $Zeitstempel);
									echo ("\n" . time () . "--" . "Auftrag abgelehnt");
									$this->NotificateOnTalkPage ($User[$TaskNumber], 5, "");
								}
								elseif ($ToCheck[0] != $User[$TaskNumber])
								{
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "p", "bot: this is not your own usernamespace", $Zeitstempel);
									echo ("\n" . time () . "--" . "Auftrag abgelehnt");
									$this->NotificateOnTalkPage ($User[$TaskNumber], 5, "");
								}
								elseif (strstr ($StartLemma[$TaskNumber], "/") === false)
								{
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "n", "bot: this is not a subpage", $Zeitstempel);
									echo ("\n" . time () . "--" . "Auftrag abgelehnt");
									$this->NotificateOnTalkPage ($User[$TaskNumber], 3, "");
								}
								elseif (strstr ($TargetLemma[$TaskNumber], ":")  === false)
								{
									if ($this->MovePageProtection ($EndResult) === true)
									{
										echo ("\n" . time () . "--" . "Verschiebe Seite");
										$answer = $this->MovePage ($StartLemma[$TaskNumber], $TargetLemma[$TaskNumber], "bot: move per request by [[User:" . $User[$TaskNumber] . "|" . $User[$TaskNumber] ."]]: " . $Summary[$TaskNumber], $Zeitstempel);
										echo ("\n" . $answer);
										if (strstr ($answer, "error") === false)
										{
											echo ("\n" . time () . "--" . "Seite verschoben");
											$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "e", "bot: request successful", $Zeitstempel);
											$this->NotificateOnTalkPage ($User[$TaskNumber], 1, $TargetLemma[$TaskNumber]);
										}
										else
										{
											echo ("\n" . time () . "--" . "Seite konnte nicht verschoben werden");
											$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "f", "bot: page protected", $Zeitstempel);
											$this->NotificateOnTalkPage ($User[$TaskNumber], 2, "");
										}
										$inwork = false;
										echo ("\n" . time () . "--" . "Auftrag bearbeitet");
									}
									else
									{
										$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "f", "bot: the talkpage is protected", $Zeitstempel);
										$this->NotificateOnTalkPage ($User[$TaskNumber], 2, "");
										echo ("\n" . time () . "--" . "Auftrag abgelehnt");
									}
								}
								else
								{
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "n", "bot: the target of your request is not at the article namespace", $Zeitstempel);
									$this->NotificateOnTalkPage ($User[$TaskNumber], 3, "");
									echo ("\n" . time () . "--" . "Auftrag abgelehnt");
								}
								/*else
								{
									$this->ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, "n", "bot: page is not at your usernamespace", $Zeitstempel);
									$this->NotificateOnTalkPage ($User[$TaskNumber], 3, "");
									echo ("\n" . time () . "--" . "Auftrag abgelehnt");
								}*/	
							}
							else 
							{}
						}
					}
				}
			}
		}
		else
		{
			echo ("\n" . time () . "--" . "Bot gesperrt!");
		}
	}
	protected function ChangeStatus ($NumberOfTasks, $TaskNumber, $Task, $User, $StartLemma, $TargetLemma, $Summary, $Status, $NewStatus, $Reason, $Zeitstempel)
	{
		$ToWriteDown = "";
		if ($NewStatus === 'e')
			$ToWriteDown= $ToWriteDown . "{{User:Luke081515Bot/Queue/Job|TASK=" . $Task[$TaskNumber] . "|USER=" . $User[$TaskNumber] . "|StartLemma=" . $StartLemma[$TaskNumber] . "|TargetLemma=" . $TargetLemma[$TaskNumber] . "|Summary=" . $Summary[$TaskNumber] . "|STATUS=" . $NewStatus . "|Zeitstempel=~~~~~}}"; 
		else
			$ToWriteDown= $ToWriteDown . "{{User:Luke081515Bot/Queue/Job|TASK=" . $Task[$TaskNumber] . "|USER=" . $User[$TaskNumber] . "|StartLemma=" . $StartLemma[$TaskNumber] . "|TargetLemma=" . $TargetLemma[$TaskNumber] . "|Summary=" . $Summary[$TaskNumber] . "|STATUS=" . $NewStatus . "|Zeitstempel=}}"; 	
		for ($a=0; $a<$NumberOfTasks; $a++)
		{
			if ($a===$TaskNumber)
			{}
			else
			{
				$ToWriteDown= $ToWriteDown . "\n{{User:Luke081515Bot/Queue/Job|TASK=" . $Task[$a] . "|USER=" . $User[$a] . "|StartLemma=" . $StartLemma[$a] . "|TargetLemma=" . $TargetLemma[$a] . "|Summary=" . $Summary[$a] . "|STATUS=" . $Status[$a] . "|Zeitstempel=" . $Zeitstempel[$a] ."}}";
			}
		}
		$this->savePage("User:Luke081515Bot/Queue/Requests", $ToWriteDown, $Reason);
	}
	
	protected function ControlIgnoreList ($TargetUser)
	{
		$AllIgnoredUsers = $this->readPageJs("User:Luke081515/ignore.js");	
		$Border = "|";
		$IgnoredUsers = explode ($Border, $AllIgnoredUsers);
		$q = 0;
		while (isset($IgnoredUsers[$q]) === true)
		{
			$q++;
		}
		for ($x = 0; $x < $q; $x++)
		{
			if ($TargetUser === $IgnoredUsers[$x])
			{
				return true;
			}
		}
		return false;
	}
	public function mainEngine ()
	{
		echo ("\n" . time () . "--" . "Programm gestartet");
		$Set = true;
		while ($Set === true)
		{
			$this->detectTasks ();
			sleep (10);
		}
		echo ("\n" . time () . "--" . "Programm beendet");
	}
	protected function MovePageProtection ($Page)
	{
		$title = "User talk:" . $Page;
		$data = "action=query&format=php&prop=info&intoken=edit&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$answer = unserialize($website);
		$token = array_pop($answer['query']['pages']);
		$token = $token['edittoken'];
 
		// now save using the edit token
		$data = "action=query&prop=info&format=php&inprop=protection&intoken=edit&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);
		// do nothing with the answer... ;)
    	echo ("\n". $website);
		if (strstr ($website, "sysop") === false)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	public function CheckUser ($CheckUser)
	{
		$title = "User:Luke081515Bot/Queue/Requests";
		$data = "action=query&format=php&prop=info&intoken=edit&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$answer = unserialize($website);
		$token = array_pop($answer['query']['pages']);
		$token = $token['edittoken'];
 
		// now save using the edit token
		$data = "action=query&prop=revisions&format=php&rvprop=user&titles=User%3ALuke081515Bot%2FQueue%2FRequests";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);
		$position = strpos ($website , "user"); 
		$Result = substr ($website, $position);
    	echo ("\n". $website);
		echo ("\n". $Result);
		if (strstr ($Result, $CheckUser) !== false)
		{
			return 1;
		}
		else if (strstr ($Result, "Luke081515\"") !== false)
		{
			return 2;
		}
		else 
		{
			return 0;
		}
	}
	
	protected function ControlRobinsonList ($TargetUser)
	{
		$AllRobinsonUsers = $this->readPage("User:Luke081515Bot/Opt-Out");	
		$Border = "|";
		$RobinsonUsers = explode ($Border, $AllRobinsonUsers);
		$q = 0;
		while (isset($RobinsonUsers[$q]) === true)
		{
			$q++;
		}
		for ($x = 0; $x < $q; $x++)
		{
			if ($TargetUser === $RobinsonUsers[$x])
			{
				return true;
			}
		}
		return false;
	}
	protected function ControlOnlyErrorList ($TargetUser)
	{
		$AllOnlyErrorUsers = $this->readPage("User:Luke081515Bot/Error-Only");	
		$Border = "|";
		$OnlyErrorUsers = explode ($Border, $AllOnlyErrorUsers);
		$q = 0;
		while (isset($OnlyErrorUsers[$q]) === true)
		{
			$q++;
		}
		for ($x = 0; $x < $q; $x++)
		{
			if ($TargetUser === $OnlyErrorUsers[$x])
			{
				return true;
			}
		}
		return false;
	}
	protected function NotificateOnTalkPage ($User, $MessageNumber, $Sitename)
	{
		if ($MessageNumber === 5)
		{
			$this->savePage("User talk:Luke081515", "\n" . $this->readPage("User talk:Luke081515"), "bot: message");
		}
		else if ($this->ControlRobinsonList ($User) === true)
		{}
		else if ($this->ControlOnlyErrorList ($User) === true)
		{
			if ($MessageNumber === 2)
				$Message = "\n{{subst:User:Luke081515Bot/Message/successful}}";
			if ($MessageNumber === 3)
				$Message = "\n{{subst:User:Luke081515Bot/Message/error}}";
			if ($MessageNumber === 4)
				$Message =	"\n{{subst:User:Luke081515Bot/Message/abborted}}";
		}
		else
		{
			if ($MessageNumber === 1)
				$Message = "\n{{subst:User:Luke081515Bot/Message/successful|" . $Sitename . "}}";
			if ($MessageNumber === 2)
				$Message = "\n{{subst:User:Luke081515Bot/Message/error}}";
			if ($MessageNumber === 3)
				$Message = "\n{{subst:User:Luke081515Bot/Message/declined}}";
			if ($MessageNumber === 4)
				$Message =	"\n{{subst:User:Luke081515Bot/Message/abborted}}";
			$this->savePage("User talk:" . $User, $this->readPage("User talk:" . $User) . $Message, "bot: information about your request");
		}
	}
}
?>
