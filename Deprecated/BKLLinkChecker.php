#!/usr/bin/php
<?php
include './BotCore.php';
######################
# Status: Deprecated #
######################
class BKLChecker extends Core {
	public function BKLChecker ($Account, $Job, $pUseHTTPS = true) {
		$this->initcurl($Account, $Job, $pUseHTTPS = true);
		$this->main ("<Buchstabe>", "<Seite>");
	}
	public function main ($Letter, $Lemma) {
		$c=0;
		$AllBKLSS = $this->getCatMembers();
		$AllBKLS = unserialize ($AllBKLSS);
		$a=0;
		while (isset ($AllBKLS [$a]) === true) {
			$Lower = strtolower ($AllBKLS [$a]);
			if (substr ($Lower, 0, 1) === $Letter) {
				$LinksS = $this->getLinks ($AllBKLS [$a]);
				if (isset ($LinksS) === true && $LinksS === -1) {}
				else if (isset ($LinksS) === true) {
					$Links = unserialize ($LinksS);
					$b=0;
					while (isset ($Links [$b]) === true) {
						echo ("\n" . $Links [$b]);
						if ($this->ExcludeFalsePositives ($Links [$b], $AllBKLS [$a]) === false) {
							if ($this->GetPageCats ($Links [$b]) === true) {
								echo ("     true");
								$BKLResult [$c] = $AllBKLS [$a];
								$SiteResult [$c] = $Links [$b];
								$c++;
							}
						}
						$b++;
					}				
				}
				else {}
			}		
			$a++;
		}
		$this->writeResults ($BKLResult, $SiteResult, $Lemma);
	}
	public function writeResults ($BKLResult, $SiteResult, $Lemma) {
		$Write = "\n<small>Letzte Aktualisierung der Seite: ~~~~</small>";
		$a=0;
		while ((isset ($BKLResult [$a]) === true) && (isset ($SiteResult [$a]) === true)) {
			$Write = $Write . "\n# Seite: [[:" . $SiteResult [$a] . "]], BKL: [[:" . $BKLResult [$a] . "]],";
			$a++;
		}
		$this->editPage($Lemma, $Write, "Bot: Aktualisiere Liste");
	}
	public function getCatMembers () {
		$b=0;
		$data = "action=query&list=categorymembers&format=php&cmtitle=Kategorie%3ABegriffskl%C3%A4rung&cmprop=title&cmnamespace=0&cmtype=page&cmlimit=5000&cmsort=sortkey&cmdir=ascending&rawcontinue=";
		try {
			$result = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Answer = explode ("\"", $result);
		while (strstr ($website, "cmcontinue") !== false) {
			$Continue = $Answer [7];
			$a=17;
			while (isset ($Answer [$a]) === true) {
				$x = $a + 1;
				$Exception = false;
				while (strstr ($Answer [$x], ";}") === false) {
					if ($Exception === false)
						$New = $Answer [$a] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$a = $a +  6;
				$b++;
				sleep (0.3);
			}
			$data = "action=query&list=categorymembers&format=php&cmtitle=Kategorie%3ABegriffskl%C3%A4rung&cmprop=title&cmnamespace=0&cmtype=page&cmlimit=5000&cmsort=sortkey&cmdir=ascending&cmcontinue=" . urlencode ($Continue) . "&rawcontinue=";
			try {
				$result = $this->httpRequest($data, $this->job, 'GET');
			} catch (Exception $e) {
				throw $e;
			}
			$Answer = explode ("\"", $result);
		}
		$a=9;
		while (isset ($Answer [$a]) === true) {
			while (isset ($Answer [$a]) === true) {
				$x = $a + 1;
				$Exception = false;
				while (strstr ($Answer [$x], ";}") === false) {
					if ($Exception === true)
						$Result [$b] = $New;
					else
						$Result [$b] = $Answer [$a];
					$Exception = true;
					$a++;
					$x = $a + 1;
				}
				if ($Exception === true)
					$Result [$b] = $New;
				else
					$Result [$b] = $Answer [$a];
				$a = $a +  6;
				$b++;
			}
		}
		$ret = serialize ($Result);
		return $ret;
	}	
	public function getLinks ($Site) {
		$data = "action=query&prop=linkshere&format=php&lhprop=title&lhnamespace=0&lhshow=!redirect&lhlimit=5000&rawcontinue=&titles=" . urlencode ($Site);
		try {
			$result = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Answer = explode ("\"", $result);
		$a=19;
		$b=0;
		while (isset ($Answer [$a]) === true) {
			$x = $a + 1;
			$Exception = false;
			while (strstr ($Answer [$x], ";}") === false) {
				if ($Exception === false)
					$New = $Answer [$a] . "\"" . $Answer [$x];
				else
					$New = $New . "\"" . $Answer [$x];
				$Exception = true;
				$a++;
				$x = $a + 1;
			}
			if ($Exception === true)
				$Result [$b] = $New;
			else
				$Result [$b] = $Answer [$a];
			$a = $a +  6;
			$b++;
		}
		if (isset ($Result [0]) === true) {
			$ret = serialize ($Result);
			return $ret;
		}
		return -1;
	}
	public function ExcludeFalsePositives ($Site, $BKL) {
		$Content = $this->readPage ($Site);
		if (strstr ($Content, "|" . $BKL . "}}") !== false)
			return true;
		if (strstr ($Content, "|1=" . $BKL . "}}") !== false)
			return true;
		if (strstr ($Content, "{{Dieser Artikel") !== false) {
			if (strstr ($Content, "[[" . $BKL . "]].}}") !== false)
				return true;
			if (strstr ($Content, "[[" . $BKL . "]]}}") !== false)
				return true;
		}
		return false;
	}
	public function GetPageCats ($Site) {
		$Cat [0] = "Kategorie:Mann";
		$Cat [1] = "Kategorie:Frau";
		$data = "action=query&prop=categories&format=php&cllimit=5000&cldir=ascending&rawcontinue=&titles=" . urlencode($Site);
		try {
			$result = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$Result = explode ("\"", $result);
		$a=19;
		$b=0;
		while (isset ($Result [$a]) === true) {
			$Kats [$b] = $Result [$a];
			$a = $a +  6;
			$b++;
		}
		$a=0;
		$b=0;
		while (isset ($Cat [$b]) === true) {
			while (isset ($Kats [$a]) === true) {
				if ($Cat [$b] === $Kats [$a])
					return true;
				$a++;			
			}
			$b++;
			$a=0;
		}
		$b=0;
		return false;
	}	
}
$mwbot = new BKLChecker('Luke081515Bot@dewiki', 'BKLLC');
?>