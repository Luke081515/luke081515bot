#!/usr/bin/php
<?php
include 'BotCore.php';

########################################################
## Status: Unstable, Eingestellt, da kein Konsens ##
########################################################

class SPPBot extends Core
{
	protected $host;
	protected $username;
	protected $password;
	protected $cookies;
 
    public function MwBot($host, $username, $password)
    {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
		$Enable = true;
		while ($Enable === true)
		{
			$this->setSP ();
			sleep (10);
		}
	}
    protected function login() 
    {
		// first step: get login token
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php";
		$website = $this->PostToHost($this->host, "/w/api.php", $data);
		$cookies = $this->GetCookies($website);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);  
		$login_token = $answer['login']['token'];
 
		// second step: get cookies
		$data = "action=login&lgname=" . urlencode($this->username) . "&lgpassword=" . urlencode($this->password) . "&format=php&lgtoken=" . urlencode($login_token);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$answer = unserialize($website);  
		$this->cookies = "Cookie: " . $answer['login']['cookieprefix'] . "UserName=" . $answer['login']['lgusername'] . "; " . $answer['login']['cookieprefix'] . "UserID=" . $answer['login']['lguserid'] . "; " . $answer['login']['cookieprefix'] . "Token=" . $answer['login']['lgtoken'] . "; " . $answer['login']['cookieprefix'] . "Session=" . $answer['login']['sessionid'] . ";\r\n";
    }
	public function savePage($title, $content, $summary, $bot, $minor)
    {
		// get edit token
		$data = "action=query&meta=tokens&format=php&type=csrf&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$answer = unserialize($website);
		$Answer = explode ("\"", $website);
		$token = $Answer [7];
 
		// now save using the edit token
		if ($bot === true && $minor === true)
			$data = "action=edit&format=php&maxlag=3&nocreate=1&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=&minor=";
		if ($bot === false && $minor === true)
			$data = "action=edit&format=php&maxlag=3&nocreate=1&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&minor=";
		if ($bot === false && $minor === false)
			$data = "action=edit&format=php&maxlag=3&nocreate=1&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary);
		if ($bot === true && $minor === false)
			$data = "action=edit&format=php&maxlag=3&nocreate=1&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		echo ("\n" . $website);
		sleep (3);
		while (strstr ($website, "Waiting for") !== false)
		{
			sleep (10);
			// get edit token
			$data = "action=query&format=php&prop=info&intoken=edit&titles=" . urlencode($title);
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
			$answer = unserialize($website);
			$token = array_pop($answer['query']['pages']);
			$token = $token['edittoken'];
	 
			// now save using the edit token
			if ($bot === true && $minor === true)
				$data = "action=edit&format=php&maxlag=3&nocreate=1&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=&minor=";
			if ($bot === false && $minor === true)
				$data = "action=edit&format=php&maxlag=3&nocreate=1&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&minor=";
			if ($bot === false && $minor === false)
				$data = "action=edit&format=php&maxlag=3&nocreate=1&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary);
			if ($bot === true && $minor === false)
				$data = "action=edit&format=php&maxlag=3&nocreate=1&title=" . urlencode($title) . "&text=" . urlencode($content) . "&token=" . urlencode($token) . "&summary=" . urlencode($summary) . "&bot=";
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
		}
	}
    public function readPage($title)
    {
		// get edit token
		$data = "action=query&prop=revisions&format=php&rvprop=content&rvlimit=1&rvcontentformat=text%2Fx-wiki&rvdir=older&rawcontinue=&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$Answer = strstr ($website, "s:1:\"*\";");
		$Answer = substr ($Answer, 8);
		$Answer = strstr ($Answer, "\"");
		$Answer = substr ($Answer, 1);
		$Answer = strstr ($Answer, "\";}}}}}}", true);
		//$Answer = urldecode ($Answer);
		return  $Answer;
    }
	
	public function readSection($title, $section)
    {
		// get edit token
		$data = "action=query&prop=revisions&format=php&rvprop=content&rvlimit=1&rvcontentformat=text%2Fx-wiki&rvdir=older&rvsection=" . urlencode($section) . "&rawcontinue=&titles=" . urlencode($title);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n"))); // remove header
		$Answer = strstr ($website, "s:1:\"*\";");
		$Answer = substr ($Answer, 8);
		$Answer = strstr ($Answer, "\"");
		$Answer = substr ($Answer, 1);
		$Answer = strstr ($Answer, "\";}}}}}}", true);
		//$Answer = urldecode ($Answer);
		return  $Answer;
    }
	
	// POST-Funktion
    protected function PostToHost($host, $path, $data, $cookies = "") 
    {
		$fp = fsockopen($host, 80, $errno, $errstr, 30);
		if (!$fp) { trigger_error("Cannot create fsock: $errstr ($errno)\n", E_USER_ERROR); }
		fputs($fp, "POST $path HTTP/1.0\r\n");
		fputs($fp, "Host: $host\r\n");
		fputs($fp, "User-Agent: MwBot\r\n");
		fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
		fputs($fp, "Content-length: ". strlen($data) ."\r\n");
		if ($cookies != "") fputs($fp, $cookies);
		fputs($fp, "Connection: close\r\n\r\n");
		fputs($fp, $data);
		$res = "";
		while(!feof($fp)) $res .= fgets($fp, 128);
		fclose($fp);
		return $res;
    }
 
    protected function GetCookies($website)
    {
		$cookies = "";
		$regexp = "/Set-Cookie: (.*)\r\n/iU";
		preg_match_all($regexp, $website, $treffer, PREG_SET_ORDER);
		foreach ($treffer as $wert) 
        if (trim($wert[1]) != "") $cookies .= "" . $wert[1] . "; ";
		$cookies = str_replace("; path=/", "", $cookies);
		$cookies = str_replace("; httponly", "", $cookies);
		$cookies = preg_replace("/expires=[^;]+;/i", "", $cookies);
		$cookies = preg_replace("/domain=[^;]+;/i", "", $cookies);
		return "Cookie: " . $cookies . "\r\n"; 
    }  
	//End of the part written by APPER
    //The following part was written by Luke081515
	public function setSP ()
	{
		# Sucht neue SPP Anfragen #
		$SPsS = $this->getCatMembers ("Kategorie:Wikipedia:Sperrpr&uuml;fung (nicht eingetragen)");
		if ($SPsS === -1){}
		else
		{
			$SPs = unserialize ($SPsS);
			$a=0;
			$b=0;
			while (isset ($SPs [$a]) === true)
			{
				if (strstr ($SPs [$a], "Benutzerin Diskussion:") !== false)
					$Username [$a] = substr ($SPs [$a], 22);
				else
					$Username [$a] = substr ($SPs [$a], 20);
				if ($this->CheckIfSPPIsset ($Username [$a]) === false)
				{
					$NewSPSite [$b] = $SPs [$a];
					$NewSPUser [$b] = $Username [$a];
					$b++;
				}
				$a++;
			}
			
			# Trägt neue SPP Anfragen ein #
			unset ($b);
			$a=0;
			while (isset ($NewSPSite [$a]) === true)
			{
				$this->setTags ($NewSPSite [$a]);
				$this->WriteSPPEntry ($NewSPUser [$a]);
				$a++;
			}
			$a=0;
			while (isset ($NewSPSite [$a]) === true)
			{
				if ($this->findSection ($NewSPSite [$a]) !== -1)
				{
					$Section = $this->findSection ($NewSPSite [$a]);
					echo ("\n" . $Section);
					$Admin = $this->findBlockingAdmin ($NewSPSite [$a], $Section);
					$AdminDisk = "Benutzer Diskussion:" . $Admin;
					$this->sendMessage ($AdminDisk);
				}
				$a++;
			}
		}
	}
	protected function CheckIfSPPIsset ($Username)
	{
		$Content = $this->readPage ("Wikipedia:Sperrpr&uuml;fung");
		if (strstr ($Content, "== [[Benutzer:" . $Username . "]] ==") !== false)
			return true;
		return false;
	}
	protected function getCatMembers ($Kat)
	{		
		$data = "action=query&list=categorymembers&format=php&cmtitle=" . urlencode($Kat) . "&cmprop=title&cmtype=page&cmlimit=5000&cmsort=sortkey&cmdir=ascending&cmnamespace=3&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		echo ($website);
		$a=9;
		$c=0;
		$Answer = explode ("\"", $website);
		while (isset ($Answer [$a]) === true)
		{
			echo ("\n" . $Answer [$a]);				
			$Exception = false;
			$x = $a + 1;
			while (strstr ($Answer [$x], ";}") === false)
			{
				if ($Exception === false)
					$New = "";
				$Exception = true;
				$New = $New . $Answer [$a] . "\"" . $Answer [$x];
				$a++;
				$x = $a + 1;
			}
			if ($Exception === true)
				$Page [$c] = $New;
			else
				$Page [$c] = $Answer [$a];
			$a = $a + 6;
			$c++;
		}
		$b=0;
		$d=0;
		if (isset ($Page) === false)
			return -1;
		$Ret = serialize ($Page);
		return $Ret;
	}	
	protected function WriteSPPEntry ($Username)
	{
		$Content = $this->readPage ("Wikipedia:Sperrpr&uuml;fung");
		$Content = $Content . "\n\n\n== [[BD:" . $Username . "|" . $Username . "]] ==\n<small>Zur [[Benutzer Diskussion:" . $Username . "|Diskussionsseite]]</small>\n\n{{#lst:Benutzer Diskussion:" . $Username. "|SP}}\n<!-- Diskutiert wird nicht hier!!! Bitte folge dem Link in der Überschrift! -->";
		$this->savePage ("Wikipedia:Sperrpr&uuml;fung", $Content, "Bot: Trage neue Sperrpr&uuml;fung ein", false, false);
	}
	protected function setTags ($Site)
	{
		$Content = $this->readPage($Site);
		$Content = str_replace ("{{Entsperrung|", "\n== [[WP:SPP|Sperrpr{{subst:User:Luke081515Bot/u}}fung]]  ~~~~~ ==\n<section begin=\"SP\" />\n=== Antrag ===\n{{Entsperrung wird diskutiert|3={{subst:PAGENAMEE}}|", $Content);
		$Content = $Content . "\n=== Diskussion ===\n\n\n<section end=\"SP\"/>";
		$this->savePage ($Site, $Content, "Bot: Bereite Seite f&uuml;r eine [[WP:SPP|Sperrpr&uuml;fung]] vor", true, true);
	}
	
	public function findSection ($Page)
	{
		$a=1;
		while (strstr ($this->readSection($Page, $a), "=") !== false)
		{
			if (strstr ($this->readSection($Page, $a), "{{Entsperrung wird diskutiert|") !== false)
				return $a;
			else
				$a++;
		}	
		echo(" False ");
		return -1;
	}
	
	public function findBlockingAdmin ($Page, $Section)
	{
		$Content = $this->readSection($Page, $Section);
		$Admin = strstr ($Content, "{{Entsperrung");
		$Admin = strstr ($Admin, "|");
		$Admin = substr ($Admin, 1);
		$Admin = strstr ($Admin, "|");
		$Admin = substr ($Admin, 1);
		$Admin = strstr ($Admin, "|");
		$Admin = substr ($Admin, 1);
		if (strstr ($Admin, "2=") === false) {}
		else
			$Admin = substr ($Admin, 2);
		$Admin = strstr ($Admin, "}}", true);
		return $Admin;
	}
	
	public function sendMessage ($Page)
	{
		$Content = $this->readPage ($Page);
		$Content = $Content . "\n\n{{subst:Benutzer:Luke081515Bot/SPPMessage}}";
		$this->savePage($Page, $Content, "Bot: Benachrichtige den sperrenden Admin &uuml;ber eine neue Sperrpr&uuml;fung", true, false);
	}
}
?>