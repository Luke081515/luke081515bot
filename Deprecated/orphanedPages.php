#!/usr/bin/php
<?php
include 'BotCore.php';

#######################
# Status: Deprecated  #
#######################

class SLABot extends Core
{
    protected $host;
    protected $username;
    protected $password;
    protected $cookies;
 
    public $bot = 1; // set to 0 if this edit should not be marked as bot edit
 
    public function SLABot($host, $username, $password, $Namespace)
    {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
		$this->main ($Namespace);
	}
	public function mainEngine () {}
    public function main ($Namespace) 
	{
		$Continue = "";
		$data = "action=query&list=allpages&format=php&apcontinue=" . $Continue . "&apnamespace=" . $Namespace . "&aplimit=5000&apdir=ascending&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer = explode ("\"", $website);
		$Sites = "";
		$q = 0;
		while (strstr ($website, "apcontinue") !== false)
		{
			$b=19;
			$Continue = $Answer [7];
			echo ("\n Continue: " . $Continue);
			while (isset($Answer [$b]) === true)
			{
				$Exception = false;
				$x = $b + 1;
				while (strstr ($Answer [$x], ";}") === false)
				{
					if ($Exception === false)
						$New = $Answer [$b] . "\"" . $Answer [$x];
					else
						$New = $New . "\"" . $Answer [$x];
					$Exception = true;
					$b++;
					$x = $b + 1;
				}
				if ($Exception === true)
					$Sites [$q] = $New;
				else
					$Sites [$q] = $Answer [$b];
				$b = $b + 8;
				$q++;
			}
			$data = "action=query&list=allpages&format=php&apcontinue=" . $Continue . "&apnamespace=" . $Namespace . "&aplimit=5000&apdir=ascending&rawcontinue=";
			$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
			$website = trim(substr($website, strpos($website, "\n\r\n")));
			$Answer = explode ("\"", $website);
		}
		$b=11;
		$data = "action=query&list=allpages&format=php&apcontinue=" . $Continue . "&apnamespace=" . $Namespace . "&aplimit=5000&apdir=ascending&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer = explode ("\"", $website);
		while (isset($Answer [$b]) === true)
		{
			$Exception = false;
			$x = $b + 1;
			while (strstr ($Answer [$x], ";}") === false)
			{
				if ($Exception === false)
					$New = $Answer [$b] . "\"" . $Answer [$x];
				else
					$New = $New . "\"" . $Answer [$x];
				$Exception = true;
				$b++;
				$x = $b + 1;
			}
			if ($Exception === true)
				$Sites [$q] = $New;
			else
				$Sites [$q] = $Answer [$b];
			$b = $b + 8;
			$q++;
		}
		echo ("\nFertig mit auslesen!");
		$a=0;
		$b=0;
		$f=0;
		while (isset ($Sites [$a]) === true && $this->readPage("Benutzer:Luke081515Bot/AutoSLA") === "true" && $f < 14)
		{
			if (strstr ($Sites [$a], "/") === false)
			{
				if ($this->CheckFront ($Sites [$a]) === false)
				{
					if ($this->SLA ($Sites [$a], 1) === true)
					{
						echo ("\nSLA auf:" . $Sites [$a]);
						$Deleted [$f] = $Sites [$a];
						$f++;
					}
					else 
						echo ("\nFehler:" . $Sites [$a]);
					sleep (5);
				}
			}
			$a++;
			$b++;
			if ($b >= 5000)
			{
				echo ("\nStandort:" . $Sites [$a]);
				$b=0;
			}
		}
		$f=0;
		$Write = "\n== Lauf ~~~~~ ==";
		while (isset ($Deleted [$f]) === true)
		{
			$Write = $Write . "\n* [[" . $Deleted [$f] . "]] - Verwaiste Datei-Diskussionsseite oder Lokale Diskussionsseite einer Commons-Datei";
			$f++;
		}
		$Write = $Write . "\n-- ~~~~";
		if (isset ($Deleted [0]) === true)
			$this->savePage ("User:Luke081515Bot/Log", $this->readPage ("Benutzer:Luke081515Bot/Log") . $Write, "Bot: Schreibe Log &uuml;ber letzten Botlauf");
	}
	
	public function SLA ($Page)
	{
		$Content = $this->readPage($Page);
		$SLAText = "Verwaiste Datei-Diskussionsseite oder Lokale Diskussionsseite einer Commons-Datei";
		$Standardtext = "<span style=\"text-shadow:#0000FF 1px 1px 2px; class=texhtml\">[[Benutzer:Luke081515/Bot|Luke081515]][[User:Luke081515Bot|Bot]]</span> - [[Benutzer:Luke081515Bot/SLABot|False positive?]] ~~~~~.";
		$Content = "{{SLA|1=" . $SLAText . " " . $Standardtext . "}}\n" . $Content;
		$Result = $this->savePage ($Page, $Content, "Bot: Stelle SLA auf verwaiste Diskussionsseite");
		if (strstr ($Result, "error") !== false)
			return false;
		return true;
	}
	
	public function CheckFront ($Disk)
	{
		$Page = strstr ($Disk, ":");
		$Page = substr ($Page, 1);
		$Page = "Datei:" . $Page;
		$data = "action=query&prop=categories&format=php&cllimit=1&cldir=ascending&titles=" . urlencode($Page);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		if (strstr ($website, "missing") !== false)
			return false;
		return true;
	}
}
$Bot = new SLABot ("de.wikipedia.org", "Luke081515Bot", "------", 7);
?>