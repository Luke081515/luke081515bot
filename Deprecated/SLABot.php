#!/usr/bin/php
<?php
include 'BotCore.php';

#######################
# Status: Deprecated  #
#######################

class SLABot extends Core
{
    protected $host;
    protected $username;
    protected $password;
    protected $cookies;
 
    public $bot = 1; // set to 0 if this edit should not be marked as bot edit
 
    public function SLABot($host, $username, $password, $Namespace)
    {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->login();
		$this->getArticles ($Namespace);
	}

    public function mainEngine () {}

	public function getArticles ($Namespace)
	{
		$f=0;
		$b=11;
		$q=0;
		$data = "action=query&list=allpages&format=php&apnamespace=" . $Namespace . "&apfilterredir=redirects&aplimit=5000&apdir=ascending&rawcontinue=";
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer = explode ("\"", $website);
		while (isset($Answer [$b]) === true)
		{
			$Sites [$q] = $Answer [$b];
			$b = $b + 8;
			$q++;
		}
		$a=0;
		while (isset ($Sites [$a]) === true && $this->readPage("Benutzer:Luke081515Bot/AutoSLA") === "true")
		{
			if ($this->GetPageCats ($Sites [$a], "Kategorie:Wikipedia:Erhaltenswerte Weiterleitung") === false)
			{
				if ($this->CheckWL ($Sites [$a]) === true)
				{
					if ($this->getLinks ($Sites [$a], 0, $Namespace) === false)
					{
						if ($this->SLA ($Sites [$a], 1) === true)
						{
							echo ("\nSLA auf:" . $Sites [$a]);
							$Deleted [$f] = $Sites [$a];
							$f++;
						}
						else 
							echo ("\nFehler:" . $Sites [$a]);
						sleep (5);
					}
				}
			}
			$a++;
		}
		$f=0;
		$Write = "\n== Lauf ~~~~~ ==";
		while (isset ($Deleted [$f]) === true)
		{
			$Write = $Write . "\n* [[" . $Deleted [$f] . "]] - Diskussionsseitenweiterleitung ohne Links aus dem selben Namensraum";
			$f++;
		}
		$Write = $Write . "\n-- ~~~~";
		if (isset ($Deleted [0]) === true)
			$this->savePage ("User:Luke081515Bot/Log", $this->readPage ("Benutzer:Luke081515Bot/Log") . $Write, "Bot: Schreibe Log &uuml;ber letzten Botlauf");
	}
	public function SLA ($Page, $Reason)
	{
		$Content = $this->readPage($Page);
		if ($Reason = 1)
			$SLAText = "Unerwüschte Diskussionsseitenweiterleitung, im Diskussionsnamensraum unverlinkt";
		$Standardtext = "<span style=\"text-shadow:#0000FF 1px 1px 2px; class=texhtml\">[[Benutzer:Luke081515/Bot|Luke081515]][[User:Luke081515Bot|Bot]]</span> - [[Benutzer:Luke081515Bot/SLABot|False positive?]] ~~~~~.";
		$Content = "{{SLA|1=" . $SLAText . " " . $Standardtext . "}}\n" . $Content;
		$Result = $this->savePage ($Page, $Content, "Bot: Stelle SLA auf nicht erwünschte oder verwaiste Weiterleitung");
		if (strstr ($Result, "error") !== false)
			return false;
		return true;
	}
	
	public function GetPageCats ($Site, $Cat)
	{
		$data = "action=query&prop=categories&format=php&cllimit=5000&cldir=ascending&rawcontinue=&titles=" . urlencode($Site);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Result = explode ("\"", $website);
		$a=19;
		$b=0;
		while (isset ($Result [$a]) === true)
		{
			$Kats [$b] = $Result [$a];
			$a = $a +  6;
			$b++;
		}
		$a=0;
		$b=0;
		while (isset ($Cat [$b]) === true)
		{
			while (isset ($Kats [$a]) === true)
			{
				if ($Cat [$b] === $Kats [$a])
					return true;
				$a++;			
			}
			$b++;
			$a=0;
		}
		return false;
	}
	public function CheckWL ($Disk)
	{
		$Page = strstr ($Disk, ":");
		$Page = substr ($Page, 1);
		$Content = $this->readPage ($Page);
		$TL = strtolower ($Content);
		if (strstr ($TL, "#redirect") !== false)
			return true;
		if (strstr ($TL, "#weiterleitung") !== false)
			return true;
		return false;
	}
	public function getLinks ($Site, $Limit, $Namespace)
	{
		$data = "action=query&prop=linkshere&format=php&lhprop=title&lhnamespace=" . $Namespace . "&lhlimit=5000&rawcontinue=&titles=" . urlencode ($Site);
		$website = $this->PostToHost($this->host, "/w/api.php", $data, $this->cookies);
		$website = trim(substr($website, strpos($website, "\n\r\n")));
		$Answer = explode ("\"", $website);
		$a=19;
		$b=0;
		while (isset ($Answer [$a]) === true)
		{
			$x = $a + 1;
			$Exception = false;
			while (strstr ($Answer [$x], ";}") === false)
			{
				if ($Exception === false)
					$New = $Answer [$a] . "\"" . $Answer [$x];
				else
					$New = $New . "\"" . $Answer [$x];
				$Exception = true;
				$a++;
				$x = $a + 1;
			}
			if ($Exception === true)
				$Result [$b] = $New;
			else
				$Result [$b] = $Answer [$a];
			$a = $a +  6;
			$b++;
		}
		$a=0;
		while (isset ($Result [$a]) === true)
			$a++;
		if ($a > $Limit)
			return true;
		return false;
	}
}
$Bot = new SLABot ("de.wikipedia.org", "Luke081515Bot", "------", 1);
?>