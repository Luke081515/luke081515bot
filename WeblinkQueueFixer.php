#!/usr/bin/php
<?php
include './BotCore.php';
###################
# Status: Stable  #
###################

class WeblinkBot extends Core
{
	public function WeblinkBot ($Account, $Job, $pUseHTTPS = true) {
		$this->initcurl($Account, $Job, $pUseHTTPS = true);
		$this->mainEngine();
	} 
	public function mainEngine() {
		$Liste = "Wikipedia:WikiProjekt Weblinkwartung/Botliste";
		if ($this->readPage("Benutzer:Luke081515Bot/WeblinkQueueFixer") === "true") {
			$Number = 0;
			$c=0;
			$d=0;
			$Sites = $this->readPage($Liste);
			if (strstr ($Sites, "Bot:Arbeite") !== false || strstr ($Sites, "{{Benutzer:Luke081515Bot/Bearbeite Auftrag}}") !== false) {
				$NewSite = str_replace ("Bot:Arbeite", "{{Benutzer:Luke081515Bot/Bearbeite_Auftrag}}", $Sites);
				$this->editpage ($Liste, $NewSite, "Bot: Auftrag wird bearbeitet");
				$List = explode ("|-", $Sites);
				$a=2;
				while (isset ($List [$a]) === true) {
					if (strstr ($List [$a], "|}") === false) {	
						$Result = explode (" || ", $List [$a]);
						$b=0;
						if (isset ($Result [2]) === true) {
							$Result [0] = trim ($Result [0], "]]");
							$Result [0] = substr ($Result [0], 5);
							$Site [$d] = $Result [0];
							if (strstr ($Site [$d], "[") !== false)
								$Site [$d] = substr ($Site [$d], 1);
							if (strstr ($Site [$d], "[") !== false)
								$Site [$d] = substr ($Site [$d], 1);
							echo ("\n" . $Site [$d]);
							$ToReplace [$d] = $Result [1];
							$Target [$d] = $Result [2];
							$Target [$d] = trim ($Target [$d]);
							$d++;
						}
						unset ($Result);
					}
					$a++;
				}
				$a=0;
				$b=0;
				$d=0;
				$q=0;
				$f=0;
				$g=0;
				$Edit = true;
				$Time = 5;
				while (isset ($Site [$a]) === true) {
					if ($this->readPage("Benutzer:Luke081515Bot/WeblinkQueueFixer") !== "true")
						$Enable  = false;
					else {
						$Enable = true;
						$c=0;
						$Edit = true;
						while (isset ($EditedPages [$c]) === true) {
							if ($EditedPages [$c] === $Site [$a])
								$Edit = false;
							$c++;
						}
						if ($Edit === true) {
							$b=0;
							while (isset ($Site [$b]) === true) {
								if ($Site [$a] === $Site [$b]) {
									$GiveToReplace [$d] = $ToReplace [$b];
									$GiveTarget [$d] = $Target [$b];
									$d++;
								}
								$b++;
							}
							if (isset ($GiveToReplace [0]) === true && isset ($GiveTarget [0]) === true) {
								$Sets = serialize ($GiveToReplace);
								$Targets = serialize ($GiveTarget);
								$EditedPages [$q] = $Site [$a];
								$q++;
								$Result = $this->replace ($Site [$a], $Sets, $Targets, $Time);
								if ($Result === -1) {
									$Fehler [$f] = $Site [$a];
									$f++;
								} else if ($Result === -2) {
									$NullEdit [$g] = $Site [$a];
									$g++;
								} else  {
									$LogNumbers [$Number] = $Result;
									$LogSites [$Number] = $Site [$a];
									$Number++;
								}
							}
							unset ($GiveToReplace);
							unset ($GiveTarget);
							unset ($Targets);
							unset ($Sets);
							$d=0;
						}
					}
					$a++;
				}
				if (isset ($Fehler [0]) === false)
					$Fehler = false;
				if (isset ($NullEdit [0]) === false)
					$NullEdit = false;
				$this->WriteLog($LogSites, $LogNumbers, $Fehler, $NullEdit);
				$NewSite = "{{Benutzer:Luke081515Bot/Auftrag Erledigt}}";
				$this->editPage ($Liste, $NewSite, "Bot: Auftrag abgeschlossen");
			}
		}
		else
			echo ("Bot gesperrt!");
	}
	public function replace ($Site, $Sets, $Targets, $Time) {
		//$Auftrag = "[[Spezial:PermaLink/|Auftrag]]";
		echo ("\n Schreibe");
		$Set = unserialize ($Sets);
		$Target = unserialize ($Targets);
		$Replaced = $this->readPage ($Site);
		$a=0;
		while (isset ($Set [$a]) === true && isset ($Target [$a]) === true) {
			$Set [$a] = str_replace ("{{pipe}}", "|", $Set [$a]);
			$Target [$a] = str_replace ("{{pipe}}", "|", $Target [$a]);
			
			$Replaced = str_replace ($Set[$a], $Target[$a] . "", $Replaced);
			echo ("\nSite:" . $Site);
			echo ("\nSet:" . $Set[$a]);
			echo ("\nTarget:" . $Target[$a]);
			$a++;
		}
		if ($a === 1)
			$Result = $this->editPage($Site, $Replaced, "Bot: Ein Weblink wurde korrigiert"/*, siehe " . $Auftrag*/);
		else
			$Result = $this->editPage($Site, $Replaced, "Bot: " . $a . " Weblinks wurden korrigiert"/*, siehe " . $Auftrag*/);
		$Ret = $this->removeLink ($Set, $Site);
		if (strstr ($Result, "error") !== false)
			return -1;
		if (strstr ($Result, "nochange") !== false)
			return -2;
		if ($Ret === false)
			sleep (3);
		else
			sleep ($Time);
		return $a;
	}
	public function WriteLog ($LS, $LN, $Fehler, $NullEdit) {
		$ToWrite = $this->readPage ("User:Luke081515Bot/Log");
		$ToWrite = $ToWrite . "\n\n== Log des ~~~, Botlauf von ~~~~~ (Weblinkkorrektur) ==\n;Auf Folgenden Seite wurden Links ersetzt:\n{| class=\"wikitable\"\n!Seite\n!Anzahl der ersetzten Links\n|-";
		$a=0;
		while (isset ($LS [$a]) === true && isset ($LN [$a]) === true) {
			$ToWrite = $ToWrite . "\n|[[" . $LS [$a] . "]]" . "\n" . "|" . $LN [$a] . "\n|-";
			$a++;
		}
		$a=0;
		$ToWrite = $ToWrite . "\n|}";
		if ($Fehler !== false) {
			$ToWrite = $ToWrite . "\n=== Bei den folgenden Seiten gab es Fehler ===";
			while (isset ($Fehler [$a]) === true) {
				$ToWrite = $ToWrite . "\n* [[" . $Fehler [$a] . "]]";
				$a++;
			}
		}
		$a=0;
		if ($NullEdit !== false) {
			$ToWrite = $ToWrite . "\n=== Bei den folgenden Seiten wurde kein Edit durchgeführt (NullEdit) ===";
			while (isset ($NullEdit [$a]) === true) {
				$ToWrite = $ToWrite . "\n* [[" . $NullEdit [$a] . "]]";
				$a++;
			}
		}
		$ToWrite = $ToWrite . "\n\n--~~~~";
		$this->editPage("User:Luke081515Bot/Log", $ToWrite, "Bot: Schreibe Log über letzten Auftrag");
	}
	private function getSectionNumber ($Disk) {
		$a=1;
		$Content = $this->readPage($Disk);
		if (strstr ($Content, "== {{Anker|deadurl_2015-10}} Defekte") === false)
			return false;
		$Content = $this->readSection($Disk, $a);
		while ($Content !== "") {
			if (strstr ($Content, "== {{Anker|deadurl_2015-10}} Defekte") !== false)
				return $a;
			$a++;
			$Content = $this->readSection($Disk, $a);
		}
		return false;
	}
	protected function removeLink ($Link, $Page)
	{
		$a=0;
		$b=0;
		$Disk = "Diskussion:" . $Page;
		$content = $this->readPage ($Disk);
		$Found = false;
		while (isset ($Link [$a]) === true) {
			if (strstr ($content, $Link [$a]) !== false)
				$Found = true;
			$a++;
		}
		if ($Found === false)
			return false;
		$Res = $Section = $this->getSectionNumber ($Disk);
		if ($Res === false)
			return false;
		$content = $this->readSection($Disk, $Section);
		$a=0;
		while (isset ($Link [$a]) === true) {
			$Param = $this->getParamContent ($Link [$a], $content);
			if ($Param !== false) {
				$b++;
				$content = str_replace ($Param . "\n", "", $content);
			}
			$a++;
		}
		if ($b === 1)
			$this->editSection ($Disk, $content, "Bot: Ein Weblink wurde korrigiert", $Section);
		else
			$this->editSection ($Disk, $content, "Bot: " . $b . " Weblinks wurden korrigiert", $Section);
		return true;
	}
	private function getParamContent ($ParamName, $Content)
	{
		if (strstr ($Content, "{{Defekter Weblink") === false)
			return false;
		$TempContent = strstr ($Content, "{{Defekter Weblink");
		$TempContent = substr ($TempContent, 2);
		$TemplateContent = strstr ($TempContent, "}}", true);
		if (strstr ($TemplateContent, "{{") !== false) {
			$TempContent = strstr ($TempContent, "}}");
			$AddedContent = substr ($TempContent, 2);
			$AddedContent = strstr ($AddedContent, "}}", true);
			$TemplateContent = $TemplateContent . "}}" . $AddedContent;
		}
		while (strstr ($TempContent, "{{") !== false) {
			$TempContent = strstr ($TempContent, "}}");
			$AddedContent = substr ($TempContent, 2);
			$AddedContent = strstr ($AddedContent, "}}", true);
			$TemplateContent = $TemplateContent . "}}" . $AddedContent;
		}
		if (strstr ($TemplateContent, $ParamName) === false)
			return false;
		$ParamContentII = strstr ($TemplateContent, $ParamName, true);
		$b=1;
		$ParamContentII = strrev ($ParamContentII);
		$ParamContentII = strstr ($ParamContentII, "|", true);
		$ParamContentII = strrev ($ParamContentII);
		$ParamContent = strstr ($TemplateContent, $ParamContentII);
		if (strstr ($ParamContent, "|") !== false)
			$ParamContent = strstr ($ParamContent, "|", true);
		$ParamContent = "|" . $ParamContent;
		$ParamContent = str_replace ("\n", "", $ParamContent);
		return $ParamContent;
	}
}
$Bot = new WeblinkBot('Luke081515Bot@dewiki', 'WQF');
?>
