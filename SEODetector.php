#!/usr/bin/php
<?php
include './BotCore.php';
/** SEODector.php
* Durchsucht eine Tabelle mit Links nach aktuellen existierenden Einbindungen
* @Author Luke081515Bot
* @Version 1.0
* @Status beta
*/
class SEODetector extends Core {
	public function SEODetector ($Account, $Job, $pUseHTTPS = true) {
		$this->initcurl($Account, $Job, $pUseHTTPS = true);
		$this->main ();
	}
	/** main
	* Liest Tabelle aus, triggert das durchsuchen nach aktuellen Links, und schreibt das Ergebnis nieder
	*/
	public function main () {
		if ($this->readPage("Benutzer:Luke081515Bot/SEODetector") === "true") {
			$Number = 0;
			$c=0;
			$d=0;
			$Sites = $this->readPage("Benutzer:Alnilam/Pr&uuml;ftabelle");
			$List = explode ("|-", $Sites);
			$a=2;
			while (isset ($List [$a]) === true) {
				if (strstr ($List [$a], "|}") === false) {	
					$Result = explode (" || ", $List [$a]);
					$b=0;
					if (isset ($Result [2]) === true) {
						$Result [0] = trim ($Result [0], "]]");
						if ($c === 0) {
							$Result [0] = substr ($Result [0], 3);
							$c++;
						} else
							$Result [0] = substr ($Result [0], 3);
						$URL [$d] = $Result [0];
						$Notice [$d] = $Result [2];
						$d++;
					}
					unset ($Result);
				}
				$a++;
			}
			$a=0;
			# Checke Anzahl Einbindungen der URLs #
			while (isset ($URL [$a]) === true) {
				$Temp = $this->searchSites ($URL [$a]);
				if ($Temp !== false) {
					$Temptogether = unserialize ($Temp);
					$Result [$a] = $Temptogether [0];
					$URLres [$a] = $Temptogether [1];
				}
				$a++;
			}
			$Results = false;
			$a=0;
			while (isset ($URL [$a]) === true) {
				if (isset ($Result [$a]) === true)
					$Results = true;
				$a++;
			}
			$b=0;
			$c=0;
			$d=0;
			if ($Results === true) {
				$Write = "\n== Neue Vorkommnisse (~~~~~) ==";
				while ($c <= $a) {
					$b=0;
					while (isset ($Result [$c] [$b]) === true) {
						if (strstr ($Result [$c] [$b], "Diskussion") === false)
							$Write = $Write . "\n* {{Artikel|1=:" . $Result [$c] [$b] . "}} - <code><nowiki>" . $URLres [$c] [$b] . "</nowiki></code> - [{{fullurl:" . $Result [$c] [$b] . "|action=edit&summary=Offensichtliche+%5B%5BSuchmaschinenoptimierung%5D%5Dsma%C3%9Fnahme+entfernt&minor=1}} bearbeiten]";
						else
							$Write = $Write . "\n* {{Artikel|1=:" . $Result [$c] [$b] . "}} - <code><nowiki>" . $URLres [$c] [$b] . "</nowiki></code> - [{{fullurl:" . $Result [$c] [$b] . "|action=edit&summary=Link+entsch%C3%A4rft&minor=1}} bearbeiten]";
						$b++;
						$d++;
					}
					$c++;
				}
				if (isset ($Write) === false)
					$Write = "";
				else
					$Write = $Write . "\n\n--~~~~";
				$Content = $this->readPage ("Benutzer:Alnilam/Pr&uuml;ftabelle/Ergebnisse");
				$Write = $Content . $Write;
				$this->editPage ("Benutzer:Alnilam/Pr&uuml;ftabelle/Ergebnisse", $Write, "Bot: Melde " . $d . " neue Treffer");
			}
		}
	}
	/** searchSites
	* Sucht seiten mit Vorkommen des Links
	* @Param: Zu suchende URL, Format *.url
	* @returns Array, [0] beinhaltet die gefundenen Seiten, [1] die gefunden URLs
	*/
	private function searchSites ($URL) {
		# http überprüfen #
		$b=0;
		$c=0;
		$data = "action=query&list=exturlusage&format=php&euprop=title%7Curl&euprotocol=http&euquery=" . urlencode($URL) . "&eunamespace=0%7C1%7C6%7C10%7C12%7C14%7C100&eulimit=5000&rawcontinue=";
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$answer = unserialize($website);  
		$a=0;
		while (isset ($answer["query"]['exturlusage'][$a]['title']) === true) {
			$Site [$b] = $answer["query"]['exturlusage'][$a]['title'];	
			$URLErg [$b] = $answer["query"]['exturlusage'][$a]['url'];
			$b++;
			$a++;
		}
		# https überprüfen #
		$data = "action=query&list=exturlusage&format=php&euprop=title%7Curl&euprotocol=https&euquery=" . urlencode($URL) . "&eunamespace=0%7C1%7C6%7C10%7C12%7C14%7C100&eulimit=5000&rawcontinue=";
		try {
			$website = $this->httpRequest($data, $this->job, 'GET');
		} catch (Exception $e) {
			throw $e;
		}
		$answer = unserialize($website);  
		$a=0;
		while (isset ($answer["query"]['exturlusage'][$a]['title']) === true) {
			$Site [$b] = $answer["query"]['exturlusage'][$a]['title'];
			$URLErg [$b] = $answer["query"]['exturlusage'][$a]['url'];
			$b++;
			$a++;
		}
		if (isset ($Site) === false)
			return false;
		$Sites = $this->readPage("Benutzer:Alnilam/Pr&uuml;ftabelle/Blacklist");
		$List = explode ("|-", $Sites);
		$a=2;
		$c=0;
		$d=0;
		while (isset ($List [$a]) === true) {
			if (strstr ($List [$a], "|}") === false) {	
				$Result = explode (" || ", $List [$a]);
				$b=0;
				if (isset ($Result [2]) === true) {
					$Result [0] = trim ($Result [0], "]]");
					$Result [0] = substr ($Result [0], 3);
					$WPage [$d] = $Result [0];
					$Link [$d] = $Result [1];
					$d++;
				}
				unset ($Result);
			}
			$a++;
		}
		$a=0;
		$b=0;
		$c=0;
		while (isset ($Site [$a]) === true) {
			$b=0;
			$Blacklisted = false; // Standard ist nicht schwarzgelistet
			while (isset ($Link [$b]) === true && $Blacklisted === false) {
				if ($Site [$a] === $WPage [$b]) {
					if ($URL === $Link [$b])
						$Blacklisted = true; // Seite ausgeschlossen
				}
				$b++;
			}
			if (strstr ($Site [$a], "/Archiv") !== false)
				$Blacklisted = true;
			if (strstr ($Site [$a], "/archiv") !== false)
				$Blacklisted = true;
			if ($Blacklisted === false) {
				$Result [$c] = $Site [$a];
				$Urlres [$c] = $URLErg [$a];
				$c++;
			}
			$a++;
		}
		if (isset ($Result [0]) === false)
			return false;
		$ret [0] = $Result;
		$ret [1] = $Urlres;
		return (serialize ($ret));
	}
}
$Bot = new SEODetector("Luke081515Bot@dewiki", 'SEODetector');
?>